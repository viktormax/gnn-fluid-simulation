#include "array_d.h"

#include <gtest/gtest.h>

#include "array_h.h"

TEST(ArrayDTest, ConstructorTest) {
    array_d<float> a;
    EXPECT_EQ(a.count, 0);
    EXPECT_EQ(a.bytes, 0);
    EXPECT_EQ(a(), nullptr);

    array_d<float> b(8);
    EXPECT_EQ(b.count, 8);
    EXPECT_EQ(b.bytes, 8 * sizeof(float));
    EXPECT_NE(b(), nullptr);
}

TEST(ArrayDTest, MoveConstructorTest) {
    array_d<float> a({1, 2, 3, 4});
    array_d<float> b(std::move(a));
    EXPECT_EQ(b.count, 4);
    EXPECT_EQ(b.bytes, 4 * sizeof(float));
    EXPECT_NE(b(), nullptr);

    EXPECT_EQ(a.count, 0);
    EXPECT_EQ(a.bytes, 0);
    EXPECT_EQ(a(), nullptr);
}

TEST(ArrayDTest, CopyConstructorTest) {
    array_d<float> a({1, 2, 3, 4});
    array_d<float> b(a);
    EXPECT_EQ(b.count, 4);
    EXPECT_EQ(b.bytes, 4 * sizeof(float));
    EXPECT_NE(b(), nullptr);

    EXPECT_EQ(a.count, 4);
    EXPECT_EQ(a.bytes, 4 * sizeof(float));
    EXPECT_NE(b(), nullptr);

    EXPECT_NE(b(), a());
}

TEST(ArrayDTest, InitializerListConstructorTest) {
    array_d<int> dev = {1, 2, 3, 4, 5};
    array_h arr = dev;

    EXPECT_EQ(dev.count, 5);
    EXPECT_EQ(arr[0], 1);
    EXPECT_EQ(arr[1], 2);
    EXPECT_EQ(arr[2], 3);
    EXPECT_EQ(arr[3], 4);
    EXPECT_EQ(arr[4], 5);
}

TEST(ArrayDTest, InitTest) {
    array_d<int> dev(5);
    dev.init(-1);
    array_h arr = dev;

    EXPECT_EQ(arr[0], -1);
    EXPECT_EQ(arr[1], -1);
    EXPECT_EQ(arr[2], -1);
    EXPECT_EQ(arr[3], -1);
    EXPECT_EQ(arr[4], -1);
}

TEST(ArrayDTest, FreeTest) {
    array_d<float> arr(5);
    arr.init(1.0f);
    EXPECT_TRUE(arr() != nullptr);
    arr.free();

    // Check if the array has been freed and all members are 0
    EXPECT_EQ(arr(), nullptr);
    EXPECT_EQ(arr.count, 0);
    EXPECT_EQ(arr.bytes, 0);
}

TEST(ArrayDTest, ResizeTest) {
    array_d<float> a;
    a.resize(5);
    ASSERT_EQ(5, a.count);
    a.resize(10);
    ASSERT_EQ(10, a.count);
    a.resize(0);
    ASSERT_EQ(0, a.count);

    array_d<float> arr = {1, 2, 3};
    arr.resize(5, -1);
    array_h h = arr;
    EXPECT_EQ(arr.count, 5);
    EXPECT_EQ(h[0], 1);
    EXPECT_EQ(h[1], 2);
    EXPECT_EQ(h[2], 3);
    EXPECT_EQ(h[3], -1);
    EXPECT_EQ(h[4], -1);
}

TEST(ArrayDTest, InsertTest) {
    array_d<float> a = {1, 2, 3};
    array_d<float> b = {4, 5, 6, 7};
    a.insert(b);

    array_h h = a;
    ASSERT_EQ(a.count, 4);
    ASSERT_EQ(h[0], 4);
    ASSERT_EQ(h[1], 5);
    ASSERT_EQ(h[2], 6);
    ASSERT_EQ(h[3], 7);

    a = {1, 2, 3};
    a.insert(b, 2);
    h = a;
    ASSERT_EQ(h[0], 1);
    ASSERT_EQ(h[1], 2);
    ASSERT_EQ(h[2], 4);
    ASSERT_EQ(h[3], 5);
    ASSERT_EQ(h[4], 6);
    ASSERT_EQ(h[5], 7);
}

TEST(ArrayDTest, CopyTest) {
    array_d<float> a = {1, 2, 3, 4, 5};
    array_h b = a.copy();
    ASSERT_EQ(b.count, 5);
    for (size_t i = 0; i < a.count; ++i) {
        ASSERT_EQ(b[i], i + 1);
    }

    b = a.copy(2, 3);
    ASSERT_EQ(b.count, 3);
    ASSERT_EQ(b[0], 3);
    ASSERT_EQ(b[1], 4);
    ASSERT_EQ(b[2], 5);

    b = a.copy(1, 100);
    ASSERT_EQ(b.count, 4);
}

TEST(ArrayDTest, AppendTest) {
    array_d<float> a = {1, 2, 3};
    array_d<float> b = {4, 5, 6};
    a.append(b);

    EXPECT_EQ(a.count, 6);
    array_h h = a;
    for (int i = 0; i < a.count; i++) {
        EXPECT_FLOAT_EQ(h[i], i + 1);
    }
}

TEST(ArrayDTest, ReorderTest) {
    array_d<float> arr({1, 2, 3, 4, 5});

    // shuffle the order of elements
    array_d<int> indexes({0, 2, 4, 3, 1});
    arr.reorder(indexes);

    // check if the elements are reordered correctly
    float expected_arr[] = {1, 3, 5, 4, 2};
    array_h h = arr;
    for (int i = 0; i < 5; i++) {
        EXPECT_FLOAT_EQ(expected_arr[i], h[i]);
    }

    float expected_arr2[] = {1, 2, 3, 4, 5};
    h = arr.reorder(indexes, true);
    for (int i = 0; i < 5; i++) {
        EXPECT_FLOAT_EQ(expected_arr2[i], h[i]);
    }
}

TEST(ArrayDTest, EqualityOperatorTest) {
    array_d<float> a({1.0, 2.0, 3.0, 4.0});
    array_d<float> b({1.0, 2.0, 3.0, 4.0});
    array_d<float> c({1.0, 2.0, 3.0, 4.0, 5.0, 6.0});
    array_d<float> d({1.0, 2.0, 3.1, 4.0});

    EXPECT_TRUE(a == b);
    EXPECT_FALSE(a == c);
    EXPECT_FALSE(b == d);
}
