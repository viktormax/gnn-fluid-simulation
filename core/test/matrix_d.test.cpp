#include "matrix_d.h"

#include <gtest/gtest.h>

#include "array_h.h"
#include "matrix_h.h"

TEST(MatrixDTest, ConstructorTest) {
    // Test default constructor
    matrix_d m1;
    EXPECT_EQ(m1.rows, 0);
    EXPECT_EQ(m1.cols, 0);
    EXPECT_EQ(m1.elements, 0);
    EXPECT_EQ(m1.bytes, 0);
    EXPECT_EQ(m1(), nullptr);

    // Test constructor with size parameters
    matrix_d m2(3, 5);
    EXPECT_EQ(m2.rows, 3);
    EXPECT_EQ(m2.cols, 5);
    EXPECT_EQ(m2.elements, 3 * 5);
    EXPECT_EQ(m2.bytes, 3 * 5 * sizeof(float));
    EXPECT_NE(m2(), nullptr);
}

TEST(MatrixDTest, InitializerListConstructorTest) {
    matrix_d m({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
    EXPECT_EQ(3, m.rows);
    EXPECT_EQ(3, m.cols);

    matrix_h h = m;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            EXPECT_EQ(i * 3 + j + 1, h(i, j));
        }
    }
}

TEST(MatrixDTest, MoveConstructorTest) {
    // Initialize a matrix with values
    matrix_d m1({{1, 2, 3}, {4, 5, 6}});

    // Move construct a new matrix from the first one
    matrix_d m2(std::move(m1));

    // Verify the values in the new matrix
    EXPECT_EQ(m2.rows, 2);
    EXPECT_EQ(m2.cols, 3);
    EXPECT_EQ(m2.elements, 2 * 3);
    EXPECT_EQ(m2.bytes, 2 * 3 * sizeof(float));
    matrix_h h = m2;
    for (size_t i = 0; i < h.elements; i++) {
        EXPECT_EQ(h[i], i + 1);
    }

    // Verify that the original matrix has been moved
    EXPECT_EQ(m1.rows, 0);
    EXPECT_EQ(m1.cols, 0);
    EXPECT_EQ(m1.elements, 0);
    EXPECT_EQ(m1.bytes, 0);
    EXPECT_EQ(m1(), nullptr);
}

TEST(MatrixDTest, CopyConstructorTest) {
    // Initialize a matrix with values
    matrix_d m1({{1, 2, 3}, {4, 5, 6}});

    // Copy construct a new matrix from the first one
    matrix_d m2(m1);

    // Verify the values in the new matrix
    EXPECT_EQ(m2.rows, 2);
    EXPECT_EQ(m2.cols, 3);
    EXPECT_EQ(m2.elements, 2 * 3);
    EXPECT_EQ(m2.bytes, 2 * 3 * sizeof(float));
    matrix_h h = m2;
    for (size_t i = 0; i < m2.elements; i++) {
        EXPECT_EQ(h[i], i + 1);
    }

    // Verify that the original matrix has not been modified
    EXPECT_EQ(m1.rows, 2);
    EXPECT_EQ(m1.cols, 3);
    EXPECT_EQ(m1.elements, 2 * 3);
    EXPECT_EQ(m1.bytes, 2 * 3 * sizeof(float));
    h = m1;
    for (size_t i = 0; i < m1.elements; i++) {
        EXPECT_EQ(h[i], i + 1);
    }
}

TEST(MatrixDTest, AssignmentOperatorTest) {
    // Initialize a matrix with values
    matrix_d m1({{1, 2, 3}, {4, 5, 6}});

    // Assign the values from the first matrix to a new matrix
    matrix_d m2;
    m2 = m1;

    // Verify the values in the new matrix
    EXPECT_EQ(m2.rows, 2);
    EXPECT_EQ(m2.cols, 3);
    EXPECT_EQ(m2.elements, 2 * 3);
    EXPECT_EQ(m2.bytes, 2 * 3 * sizeof(float));
    matrix_h h = m2;
    for (size_t i = 0; i < m2.elements; i++) {
        EXPECT_EQ(h[i], i + 1);
    }

    // Verify that the original matrix has not been modified
    EXPECT_EQ(m1.rows, 2);
    EXPECT_EQ(m1.cols, 3);
    EXPECT_EQ(m1.elements, 2 * 3);
    EXPECT_EQ(m1.bytes, 2 * 3 * sizeof(float));
    h = m1;
    for (size_t i = 0; i < m1.elements; i++) {
        EXPECT_EQ(h[i], i + 1);
    }
}

TEST(MatrixDTest, ResizeTest) {
    matrix_d matrix;
    matrix.resize(3, 4);
    EXPECT_EQ(matrix.rows, 3);
    EXPECT_EQ(matrix.cols, 4);
    EXPECT_EQ(matrix.elements, 12);
    EXPECT_EQ(matrix.bytes, 48);

    matrix.resize(2, 5);
    EXPECT_EQ(matrix.rows, 2);
    EXPECT_EQ(matrix.cols, 5);
    EXPECT_EQ(matrix.elements, 10);
    EXPECT_EQ(matrix.bytes, 40);
}

TEST(MatrixDTest, InitTest) {
    matrix_d matrix(3, 4);
    matrix.init(2.0f);
    matrix_h host = matrix;
    for (size_t i = 0; i < matrix.rows; i++) {
        for (size_t j = 0; j < matrix.cols; j++) {
            EXPECT_EQ(host(i, j), 2.0f);
        }
    }
}

TEST(MatrixDTest, FreeTest) {
    matrix_d mat(3, 3);
    mat.init(1.0f);
    EXPECT_TRUE(mat() != nullptr);
    mat.free();

    // Check if the matrix has been freed and all members are 0
    EXPECT_EQ(mat(), nullptr);
    EXPECT_EQ(mat.rows, 0);
    EXPECT_EQ(mat.cols, 0);
    EXPECT_EQ(mat.elements, 0);
    EXPECT_EQ(mat.bytes, 0);
}

TEST(MatrixDTest, TransposeTest) {
    size_t rows = 3;
    size_t cols = 4;
    matrix_h h(rows, cols);

    // Initialize the matrix with some values
    for (size_t i = 0; i < h.rows; i++) {
        for (size_t j = 0; j < h.cols; j++) {
            h(i, j) = i + j;
        }
    }

    h = matrix_d(h).transpose();

    // Check if the transpose of the matrix is correct
    for (size_t i = 0; i < h.rows; i++) {
        for (size_t j = 0; j < h.cols; j++) {
            EXPECT_EQ(h(i, j), j + i);
        }
    }
}

TEST(MatrixDTest, SumRowsTest) {
    size_t rows = 3;
    size_t cols = 4;
    matrix_h mat(rows, cols);

    // Initialize the matrix with some values
    for (size_t i = 0; i < mat.rows; i++) {
        for (size_t j = 0; j < mat.cols; j++) {
            mat(i, j) = i + j;
        }
    }

    matrix_d dev = mat;
    array_h sum = dev.sumRows();

    // Check if the sum of rows of the matrix is correct
    for (size_t i = 0; i < mat.rows; i++) {
        float expected_sum = 0.0;
        for (size_t j = 0; j < mat.cols; j++) {
            expected_sum += i + j;
        }
        EXPECT_EQ(sum[i], expected_sum);
    }
}

TEST(MatrixDTest, SumColsTest) {
    size_t rows = 3;
    size_t cols = 4;
    matrix_h mat(rows, cols);

    // Initialize the matrix with some values
    for (size_t i = 0; i < mat.rows; i++) {
        for (size_t j = 0; j < mat.cols; j++) {
            mat(i, j) = i + j;
        }
    }

    matrix_d dev = mat;
    array_h sum = dev.sumCols();

    // Check if the sum of cols of the matrix is correct
    for (size_t i = 0; i < mat.cols; i++) {
        float expected_sum = 0.0;
        for (size_t j = 0; j < mat.rows; j++) {
            expected_sum += i + j;
        }
        EXPECT_EQ(sum[i], expected_sum);
    }
}

TEST(MatrixDTest, PowTest) {
    size_t rows = 3;
    size_t cols = 4;
    matrix_h mat(rows, cols);

    // Initialize the matrix with some values
    for (size_t i = 0; i < mat.rows; i++) {
        for (size_t j = 0; j < mat.cols; j++) {
            mat(i, j) = i + j;
        }
    }

    float p = 2.0;
    matrix_d dev = mat;
    matrix_h result = dev.pow(p);

    // Check if the elements of the matrix are correctly raised to power p
    for (size_t i = 0; i < mat.rows; i++) {
        for (size_t j = 0; j < mat.cols; j++) {
            EXPECT_FLOAT_EQ(result(i, j), pow(i + j, p));
        }
    }
}

TEST(MatrixDTest, MathOperationsTest) {
    size_t rows = 5;
    size_t cols = 6;
    matrix_d a(rows, cols);
    matrix_d b(rows, cols);
    a.init(4);
    b.init(2);
    matrix_h sub = a - b;
    matrix_h add = a + b;
    matrix_h mul = a * b;
    for (size_t i = 0; i < rows; ++i) {
        for (size_t j = 0; j < cols; ++j) {
            EXPECT_EQ(sub(i, j), 2);
            EXPECT_EQ(add(i, j), 6);
            EXPECT_EQ(mul(i, j), 8);
        }
    }
}

TEST(MatrixDTest, ReorderColsTest) {
    // Initialize matrix
    matrix_d m({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});

    // Reorder columns using the indexes {2, 0, 1}
    array_d<int> indexes({2, 0, 1});
    m.reorderCols(indexes);

    // Check if reordering was successful
    float expected_data[][3] = {{3, 1, 2}, {6, 4, 5}, {9, 7, 8}};
    matrix_h h = m;
    for (size_t i = 0; i < m.rows; ++i) {
        for (size_t j = 0; j < m.cols; ++j) {
            EXPECT_EQ(h(i, j), expected_data[i][j]);
        }
    }

    // Reorder columns back to their original order
    m.reorderCols(indexes, true);
    h = m;
    for (size_t i = 0, c = 1; i < m.rows; ++i) {
        for (size_t j = 0; j < m.cols; ++j, ++c) {
            EXPECT_EQ(h(i, j), c);
        }
    }
}

TEST(MatrixDTest, EqualityOperatorTest) {
    matrix_d a({{1.0, 2.0}, {3.0, 4.0}});
    matrix_d b({{1.0, 2.0}, {3.0, 4.0}});
    matrix_d c({{1.0, 2.0, 3.0}, {4.0, 5.0, 6.0}});
    matrix_d d({{1.0, 2.0}, {3.1, 4.0}});

    EXPECT_TRUE(a == b);
    EXPECT_FALSE(a == c);
    EXPECT_FALSE(b == d);
}

TEST(MatrixDTest, InsertTest) {
    matrix_d a({{0.95651897, 0.06126302, 0.64664344, 0.87117506, 0.18546972},
                {0.76848231, 0.45295841, 0.38766712, 0.09197795, 0.9280677},
                {0.1624182, 0.42227549, 0.76913692, 0.20243999, 0.86165211},
                {0.21570975, 0.03330759, 0.86446493, 0.63744516, 0.53686283}});
    matrix_d b({{0.70250496, 0.55237196, 0.4981207, 0.7601739, 0.71109085}, {0.16821588, 0.81599355, 0.4678512, 0.42020189, 0.1097581}});
    matrix_d ex1({{0.70250496, 0.55237196, 0.4981207, 0.7601739, 0.71109085},
                  {0.16821588, 0.81599355, 0.4678512, 0.42020189, 0.1097581},
                  {0.1624182, 0.42227549, 0.76913692, 0.20243999, 0.86165211},
                  {0.21570975, 0.03330759, 0.86446493, 0.63744516, 0.53686283}});
    matrix_d ex2({{0.95651897, 0.06126302, 0.64664344, 0.87117506, 0.18546972},
                  {0.76848231, 0.45295841, 0.38766712, 0.70250496, 0.55237196},
                  {0.1624182, 0.42227549, 0.76913692, 0.16821588, 0.81599355},
                  {0.21570975, 0.03330759, 0.86446493, 0.63744516, 0.53686283}});

    EXPECT_TRUE(matrix_d(a).insert(b) == ex1);
    EXPECT_TRUE(matrix_d(a).insert(b, 1, 3) == ex2);
}

TEST(MatrixDTest, CopyTest) {
    matrix_d a({{1, 2, 3, 4, 5},
                {6, 7, 8, 9, 10},
                {11, 12, 13, 14, 15}});
    matrix_d ex1({{6, 7, 8, 9, 10},
                  {11, 12, 13, 14, 15}});
    matrix_d ex2({{9, 10},
                  {14, 15}});

    EXPECT_TRUE(a.copy(1) == ex1);
    EXPECT_TRUE(a.copy(1, 3, 100, 100) == ex2);
}