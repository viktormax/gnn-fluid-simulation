﻿#include "array_h.h"

#include <stdio.h>
#include "array_d.h"
#include "operators.h"

template <typename T>
array_h<T>::array_h() : count(_count), bytes(_bytes) {
    _count = 0;
    _bytes = 0;
    _allocated_bytes = 0;
    _ptr = nullptr;
}

template <typename T>
array_h<T>::array_h(const std::initializer_list<T>& data) : array_h(data.size()) {
    size_t index = 0;
    for (const auto& value : data) {
        this->operator[](index++) = value;
    }
}

template <typename T>
array_h<T>::array_h(const size_t count) : array_h() {
    resize(count);
}

template <typename T>
array_h<T>& array_h<T>::push(const T value) {
    resize(count + 1);
    _ptr[count - 1] = value;
    return *this;
}

template <typename T>
array_h<T>& array_h<T>::resize(const size_t count, std::optional<T> fill_empty) {
    size_t original_bytes = _bytes;
    size_t original_count = _count;
    _bytes = count * sizeof(T);
    _count = count;

    if (bytes <= _allocated_bytes || bytes == 0) {
        if (fill_empty.has_value()) {
            for (size_t i = original_count; i < count; i++) {
                _ptr[i] = fill_empty.value();
            }
        }

        return *this;
    }

    long extra = _allocated_bytes + 100 * sizeof(T);
    _allocated_bytes = bytes > extra ? bytes : extra;
    T* tmp_ptr = new T[_allocated_bytes / sizeof(T)];

    if (_ptr != nullptr) {
        cudaMemcpy(tmp_ptr, _ptr, original_bytes, cudaMemcpyHostToHost);
        delete[] _ptr;
    }

    _ptr = tmp_ptr;

    if (fill_empty.has_value()) {
        for (size_t i = original_count; i < count; i++) {
            _ptr[i] = fill_empty.value();
        }
    }

    return *this;
}

template <>
void array_h<float>::print(const int max_col) const {
    printf("array<float> (%lld)\n", count);
    for (int j = 0; j < count; j++) {
        if (j >= max_col) {
            printf("...");
            break;
        }
        auto v = _ptr[j];
        printf("%s%3f ", v < 0 ? "" : " ", v);
    }
    printf("\n\n");
}

template <typename T>
array_h<T>& array_h<T>::append(const array_h<T>& that) {
    long offset = count;
    resize(count + that.count);
    cudaMemcpy(_ptr + offset, that._ptr, that.bytes, cudaMemcpyHostToHost);
    return *this;
}

template<typename T>
array_h<T>& array_h<T>::fromStream(std::ifstream& rf) {
    size_t c;

    rf.read((char*)&c, sizeof(c));
    resize(c);
    rf.read((char*)_ptr, bytes);

    return *this;
}

template<typename T>
const array_h<T>& array_h<T>::toStream(std::ofstream& wf) const {
    wf.write((char*)&_count, sizeof(_count));
    wf.write((char*)_ptr, _bytes);
    return *this;
}

template <typename T>
array_h<T>::~array_h() {
    if (_ptr == nullptr) {
        return;
    }

    delete[] _ptr;

    _count = 0;
    _bytes = 0;
    _allocated_bytes = 0;
    _ptr = nullptr;
}

template <typename T>
array_h<T>::array_h(const array_h<T>& that) : array_h(that.count) {
    cudaMemcpy(_ptr, that._ptr, that.bytes, cudaMemcpyHostToHost);
}

template <typename T>
array_h<T>::array_h(const array_d<T>& that) : array_h(that.count) {
    cudaMemcpy(_ptr, that(), that.bytes, cudaMemcpyDeviceToHost);
}

template <typename T>
array_h<T>::array_h(array_h<T>&& that) : array_h() {
    this->_count = that._count;
    this->_bytes = that._bytes;
    this->_allocated_bytes = that._allocated_bytes;
    this->_ptr = that._ptr;
    that._ptr = nullptr;
}

template <typename T>
array_h<T>& array_h<T>::operator=(array_h<T>&& that) {
    if (_ptr != nullptr) {
        delete[] _ptr;
    }

    this->_count = that._count;
    this->_bytes = that._bytes;
    this->_allocated_bytes = that._allocated_bytes;
    this->_ptr = that._ptr;
    that._ptr = nullptr;
    return *this;
}

template <typename T>
array_h<T>& array_h<T>::operator=(const array_h<T>& that) {
    resize(that.count);
    cudaMemcpy(_ptr, that._ptr, that.bytes, cudaMemcpyHostToHost);
    return *this;
}

template <typename T>
array_h<T>& array_h<T>::init(const T val) {
    for (int i = 0; i < count; i++) {
        _ptr[i] = val;
    }

    return *this;
}

std::ostream& operator<<(std::ostream& ss, const array_h<float>& a) {
    ss << std::fixed << std::setprecision(6) << "[";
    for (size_t i = 0; i < a.count; i++) {
        ss << a[i];
        if (i < a.count - 1) {
            ss << ",";
        }
    }
    ss << "]";
    return ss;
}


template <typename T>
array_h<T>& array_h<T>::operator=(const array_d<T>& that) {
    resize(that.count);
    cudaMemcpy(_ptr, that(), that.bytes, cudaMemcpyDeviceToHost);
    return *this;
}

template <typename T>
bool array_h<T>::operator==(const array_h<T>& that) const {
    if (count != that.count) {
        return false;
    }

    return std::memcmp(_ptr, that._ptr, bytes) == 0;
}
