#include "MemoryManager.hpp"

#include <cuda_runtime.h>

std::vector<std::tuple<size_t, void*, int>> MemoryManager::pointers;

MemoryManager memory_manager;

MemoryManager::MemoryManager() {
    std::atexit(MemoryManager::clear);
}

void* MemoryManager::restore(const size_t bytes) {
    void* pointer = nullptr;
    for (auto& [allocated, ptr, idle] : pointers) {
        if (allocated == bytes) {
            pointer = ptr;
            idle = 0;
            ptr = nullptr;
            allocated = 0;
            return pointer;
        }
    }

    cudaMalloc(&pointer, bytes);

    if (pointer == nullptr) {
        throw "Failed to allocate device memory.";
    }

    return pointer;
}

void MemoryManager::cache(void* pointer, const size_t bytes) {
    // cudaFree(pointer); return;
    for (auto& [allocated, ptr, idle] : pointers) {
        if (allocated == 0) {
            idle = 0;
            ptr = pointer;
            allocated = bytes;
            return;
        }
    }
    
    pointers.push_back(std::tuple(bytes, pointer, 0));
}

void MemoryManager::runGarbitchCollector(int max_idle, const size_t max_bytes) {
    for (auto& [allocated, ptr, idle] : pointers) {
        if (allocated != 0) {
            if (idle >= max_idle || (max_bytes != 0 && allocated > max_bytes)) {
                cudaFree(ptr);
                ptr = nullptr;
                allocated = 0;
                idle = 0;
            } else {
                idle++;
            }
        }
    }
}

std::tuple<size_t, size_t> MemoryManager::getStats() {
    size_t allocations = 0, usedMemory = 0;
    for (auto& [allocated, ptr, idle] : pointers) {
        if (allocated != 0) {
            allocations++;
            usedMemory += allocated;
        }
    }
    return  { allocations, usedMemory };
}

void MemoryManager::clear() {
    printf("Clear cache.\n");
    for (auto& [s, p, h] : pointers) {
        if (s != 0) {
            cudaFree(p);
            s = 0;
            p = nullptr;
            h = 0;
        }
    }
}
