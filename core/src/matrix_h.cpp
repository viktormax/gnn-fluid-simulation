#include "matrix_h.h"

#include <cuda_runtime.h>
#include <stdio.h>

#include "matrix_d.h"

matrix_h::matrix_h() : bytes(_bytes), rows(_rows), cols(_cols), elements(_elements) {
    _ptr = nullptr;
    _rows = 0;
    _cols = 0;
    _elements = 0;
    _bytes = 0;
    _allocated_bytes = 0;
}

matrix_h::matrix_h(const std::initializer_list<std::initializer_list<float>>& data) : matrix_h(data.size(), data.begin()->size()) {
    size_t row = 0;
    for (const auto& row_data : data) {
        size_t col = 0;
        for (const auto& value : row_data) {
            this->operator()(row, col) = value;
            col++;
        }
        row++;
    }
}

matrix_h::~matrix_h() {
    this->free();
}

matrix_h::matrix_h(const size_t rows, const size_t cols) : matrix_h() {
    resize(rows, cols);
}

matrix_h::matrix_h(matrix_h&& that) noexcept : matrix_h() {
    _ptr = that._ptr;
    _rows = that._rows;
    _cols = that._cols;
    _elements = that._elements;
    _bytes = that._bytes;
    _allocated_bytes = that._allocated_bytes;
    that._ptr = nullptr;
}

matrix_h::matrix_h(const matrix_h& that) : matrix_h(that.rows, that.cols) {
    cudaMemcpy(_ptr, that._ptr, that._bytes, cudaMemcpyHostToHost);
}

matrix_h::matrix_h(const matrix_d& that) : matrix_h(that.rows, that.cols) {
    cudaMemcpy(_ptr, that(), that.bytes, cudaMemcpyDeviceToHost);
}

matrix_h& matrix_h::operator=(matrix_h&& that) {
    if (_ptr != nullptr) {
        delete[] _ptr;
    }

    _ptr = that._ptr;
    _rows = that._rows;
    _cols = that._cols;
    _elements = that._elements;
    _bytes = that._bytes;
    _allocated_bytes = that._allocated_bytes;
    that._ptr = nullptr;
    return *this;
}

matrix_h& matrix_h::operator=(const matrix_h& that) {
    resize(that._rows, that._cols);
    cudaMemcpy(_ptr, that._ptr, that._bytes, cudaMemcpyHostToHost);
    return *this;
}

matrix_h& matrix_h::operator=(const matrix_d& that) {
    resize(that.rows, that.cols);
    cudaMemcpy(_ptr, that(), that.bytes, cudaMemcpyDeviceToHost);
    return *this;
}

bool matrix_h::operator==(const matrix_h& that) const {
    if (rows != that.rows || cols != that.cols) {
        return false;
    }

    return std::memcmp(_ptr, that._ptr, bytes) == 0;
}

matrix_h& matrix_h::fromStream(std::ifstream& rf) {
    size_t r, c;

    rf.read((char*)&r, sizeof(r));
    rf.read((char*)&c, sizeof(c));

    resize(r, c);

    rf.read((char*)_ptr, bytes);

    return *this;
}

const matrix_h& matrix_h::toStream(std::ofstream& wf) const {
    wf.write((char*)&rows, sizeof(rows));
    wf.write((char*)&cols, sizeof(cols));
    wf.write((char*)_ptr, bytes);

    return *this;
}

std::ostream& operator<<(std::ostream& ss, const matrix_h& m) {
    ss << std::fixed << std::setprecision(6) << "[";
    for (size_t i = 0; i < m.rows; i++) {
        ss << "[";
        for (size_t j = 0; j < m.cols; j++) {
            ss << m(i, j);
            if (j < m.cols - 1) {
                ss << ",";
            }
        }
        ss << "]";
        if (i < m.rows - 1) {
            ss << ",";
        }
    }
    ss << "]";
    return ss;
}

matrix_h& matrix_h::resize(const size_t rows, const size_t cols) {
    _rows = rows;
    _cols = cols;
    _elements = _rows * _cols;
    _bytes = _elements * sizeof(float);

    if (_bytes <= _allocated_bytes) {
        return *this;
    }

    if (_ptr != nullptr) {
        delete[] _ptr;
    }

    _allocated_bytes = _bytes;
    _ptr = (float*)new char[_allocated_bytes];

    return *this;
}

matrix_h& matrix_h::init(const float value) {
    for (size_t i = 0; i < elements; i++) {
        _ptr[i] = value;
    }

    return *this;
}

matrix_h& matrix_h::free() {
    if (_ptr == nullptr) {
        return *this;
    }

    delete[] _ptr;

    _ptr = nullptr;
    _rows = 0;
    _cols = 0;
    _elements = 0;
    _bytes = 0;
    _allocated_bytes = 0;

    return *this;
}

matrix_h& matrix_h::transpose() {
    float* new_ptr = (float*)new char[_allocated_bytes];

    for (size_t i = 0; i < _rows; i++) {
        for (size_t j = 0; j < _cols; j++) {
            new_ptr[j * _rows + i] = _ptr[i * _cols + j];
        }
    }

    size_t tmp = _rows;
    _rows = _cols;
    _cols = tmp;

    delete _ptr;
    _ptr = new_ptr;

    return *this;
}

const matrix_h& matrix_h::print(const size_t max_row, const size_t max_col) const {
    printf("matrix<float> (%lld, %lld)\n", rows, cols);

    for (size_t i = 0; i < rows; i++) {
        if (i >= max_row) {
            printf("...\n");
            break;
        }

        for (size_t j = 0; j < cols; j++) {
            if (j >= max_col) {
                printf("...");
                break;
            }

            float v = (_ptr + cols * i)[j];
            printf("%s%3.8f ", v < 0 ? "" : " ", v);
        }

        printf("\n");
    }

    printf("\n");

    return *this;
}
