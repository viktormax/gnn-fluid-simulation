#include "mappers.h"
#include "kernels.h"

matrix_d toMatrix(const array_d<float3>& arr) {
    if (arr.count == 0) {
        return matrix_d();
    }

    matrix_d out(arr.count, 3);
    map_from_float3(out.str(), arr.str());
    return out;
}

array_d<float3> toArray(const matrix_d& mat) {
    if (mat.elements == 0) {
        return array_d<float3>();
    }

    array_d<float3> out(mat.rows);
    map_to_float3(mat.str(), out.str());
    return out;
}

matrix_d toMatrix1d(const array_d<float>& arr) {
    if (arr.count == 0) {
        return matrix_d();
    }

    matrix_d out(arr.count, 1);
    cudaMemcpy(out(), arr(), arr.bytes, cudaMemcpyHostToHost);
    return out;
}

array_d<float> toArray1d(const matrix_d& mat) {
    if (mat.elements == 0) {
        return array_d<float>();
    }

    array_d<float> out(mat.rows);
    cudaMemcpy(out(), mat(), mat.bytes, cudaMemcpyHostToHost);
    return out;
}

matrix_h toMatrix(const array_h<float3>& arr) {
    if (arr.count == 0) {
        return matrix_h();
    }

    matrix_h out(arr.count, 3);
    cudaMemcpy(out(), arr(), arr.bytes, cudaMemcpyHostToHost);
    return out;
}

array_h<float3> toArray(const matrix_h& mat) {
    if (mat.elements == 0) {
        return array_h<float3>();
    }

    array_h<float3> out(mat.rows);
    cudaMemcpy(out(), mat(), mat.bytes, cudaMemcpyHostToHost);
    return out;
}

