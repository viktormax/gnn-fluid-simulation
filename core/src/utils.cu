#include "utils.h"

template <typename T>
__global__ void kernelInitArray(T* array, const T val, const size_t count) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (; index < count; index += stride) {
        array[index] = val;
    }
}

template <typename T>
void initArray(T* data, const long count,  const T val) {
    kernelInitArray<<<KERNEL_PROPS(count)>>>(data, val, count);
}

template void initArray(int*, long, int);
template void initArray(float*, long, float);
template void initArray(float3*, long, float3);
template void initArray(uint32_t*, long, uint32_t);
template void initArray(char*, long, char);

array_stats getStats(const array_h<float>& arr) {
    double sum = 0;
    double variance = 0;
    array_stats stats;
    stats.min = arr[0];
    stats.max = arr[0];
    stats.mean = 0;
    stats.count = arr.count;

    for (size_t i = 0; i < arr.count; i++) {
        sum += arr[i];
        if (arr[i] > stats.max) {
            stats.max = arr[i];
        }
        if (arr[i] < stats.min) {
            stats.min = arr[i];
        }
    }

    stats.mean = sum / stats.count;

    for (size_t i = 0; i < arr.count; i++) {
        variance += (arr[i] - stats.mean) * (arr[i] - stats.mean);
    }

    stats.stddev = sqrt(variance / stats.count);

    return stats;
}
