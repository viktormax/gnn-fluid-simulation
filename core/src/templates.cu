#include "kernels.cu"
#include "array_d.cpp"
#include "array_h.cpp"
#include "structs.h"

template class array_d<int>;
template class array_d<float>;
template class array_d<float2>;
template class array_d<float3>;
template class array_d<uint32_t>;
template class array_d<char>;
template class array_d<plane_s>;

template class array_h<plane_s>;
template class array_h<int>;
template class array_h<float>;
template class array_h<float2>;
template class array_h<float3>;
template class array_h<float4>;
template class array_h<uint32_t>;
template class array_h<char>;

std::ostream& operator<<(std::ostream& ss, const float3& p) {
    return ss << "[" << p.x << ", " << p.y << ", " << p.z << "]";
}

template void sub_vector(const float*, const float, float*, const size_t);

template <>
void array_d<float>::print(const int max_count) const {
    array_h(*this).print(max_count);
}
