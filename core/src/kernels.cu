#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>
#include <math.h>

#include "constants.h"
#include "kernels.h"
#include "operators.h"

namespace {

__global__ void kernel_sub_vector(const float* a, const float* b, const size_t b_size, float* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < size) {
        out[index] = a[index] - b[index % b_size];
    }
}

template <typename T>
__global__ void kernel_sub_vector(const T* a, const T b, T* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < size) {
        out[index] = a[index] - b;
    }
}

__global__ void kernel_add_vector(const float* a, const float* b, const size_t b_size, float* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < size) {
        out[index] = a[index] + b[index % b_size];
    }
}

__global__ void kernel_add_vector(const float* a, const float b, const size_t b_size, float* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < size) {
        out[index] = a[index] + b;
    }
}

__global__ void kernel_mul_vector(const float* a, const float* b, const size_t b_size, float* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < size) {
        out[index] = a[index] * b[index % b_size];
    }
}

__global__ void kernel_mul_vector(const float* a, const float b, float* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < size) {
        out[index] = a[index] * b;
    }
}

__global__ void kernel_div_vector(const float* a, const float* b, const size_t b_size, float* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < size) {
        out[index] = a[index] / b[index % b_size];
    }
}

__global__ void kernel_div_vector(const float* a, const float b, float* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < size) {
        out[index] = a[index] / b;
    }
}

__global__ void kernel_pow_vector(const float* a, const float p, float* out, const int size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < size) {
        out[index] = pow(a[index], p);
    }
}

__global__ void kernel_sqrt_vector(const float* a, float* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < size) {
        out[index] = sqrt(a[index]);
    }
}

__global__ void kernel_max_vector(const float* in, float* out, const float max, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < size) {
        out[index] = fmaxf(max, in[index]);
    }
}

template <typename T>
__global__ void kernel_set_vector(T* a, const T v, const int size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < size) {
        a[index] = v;
    }
}

template <typename T>
__global__ void kernel_reorder_matrix(const T* data, const int* indexes, const size_t cols, const size_t rows, const bool reverse, T* out) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int row = index / cols;
    int col = index % cols;

    if (row < rows && col < cols) {
        int to_idx = reverse ? indexes[col] : col;
        int from_idx = reverse ? col : indexes[col];

        out[to_idx + row * cols] = data[from_idx + row * cols];
    }
}

__global__ void kernel_insert_matrix(matrix_s dest, const matrix_s src, size_t start_row, size_t start_col) {
    auto index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < src.elements) {
        size_t row = index / src.cols;
        size_t col = index % src.cols;
        size_t dest_row = start_row + row;
        size_t dest_col = start_col + col;

        if (dest_row < dest.rows && dest_col < dest.cols) {
            dest(dest_row, dest_col) = src[index];
        }
    }
}

__global__ void kernel_matrix_copy(const matrix_s src, matrix_s dst, size_t start_row, size_t start_col) {
    auto index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < dst.elements) {
        auto dst_row = index / dst.cols;
        auto dst_col = index % dst.cols;

        dst(dst_row, dst_col) = src(dst_row + start_row, dst_col + start_col);
    }
}

__global__ void kernel_map_to_float3(const matrix_s matrix, array_s<float3> vector) {
    auto index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < matrix.rows) {
        auto sample = matrix.row(index);

        vector[index].x = sample[0];
        vector[index].y = sample[1];
        vector[index].z = sample[2];
    }
}

__global__ void kernel_map_from_float3(matrix_s matrix, const array_s<float3> vector) {
    auto index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < matrix.rows) {
        auto sample = matrix.row(index);

        sample[0] = vector[index].x;
        sample[1] = vector[index].y;
        sample[2] = vector[index].z;
    }
}

}  // namespace

void sub_vector(const float* a, const float* b, const size_t b_size, float* out, const size_t size) {
    kernel_sub_vector<<<KERNEL_PROPS(size)>>>(a, b, b_size, out, size);
}

template <typename T>
void sub_vector(const T* a, const T b, T* out, const size_t size) {
    kernel_sub_vector<<<KERNEL_PROPS(size)>>>(a, b, out, size);
}

void add_vector(const float* a, const float* b, const size_t b_size, float* out, const size_t size) {
    kernel_add_vector<<<KERNEL_PROPS(size)>>>(a, b, b_size, out, size);
}

void add_vector(const float* a, const float b, const size_t b_size, float* out, const size_t size) {
    kernel_add_vector<<<KERNEL_PROPS(size)>>>(a, b, b_size, out, size);
}

void mul_vector(const float* a, const float* b, const size_t b_size, float* out, const size_t size) {
    kernel_mul_vector<<<KERNEL_PROPS(size)>>>(a, b, b_size, out, size);
}

void div_vector(const float* a, const float* b, const size_t b_size, float* out, const size_t size) {
    kernel_div_vector<<<KERNEL_PROPS(size)>>>(a, b, b_size, out, size);
}

void mul_vector(const float* a, const float b, float* out, const size_t size) {
    kernel_mul_vector<<<KERNEL_PROPS(size)>>>(a, b, out, size);
}

void div_vector(const float* a, const float b, float* out, const size_t size) {
    kernel_div_vector<<<KERNEL_PROPS(size)>>>(a, b, out, size);
}

void pow_vector(const float* a, const float p, float* out, const size_t size) {
    kernel_pow_vector<<<KERNEL_PROPS(size)>>>(a, p, out, size);
}

void sqrt_vector(const float* a, float* out, const size_t size) {
    kernel_sqrt_vector<<<KERNEL_PROPS(size)>>>(a, out, size);
}

void max_vector(const float* in, float* out, const float max, const size_t size) {
    kernel_max_vector<<<KERNEL_PROPS(size)>>>(in, out, max, size);
}

template <typename T>
void set_vector(T* a, const T v, const size_t size) {
    kernel_set_vector<<<KERNEL_PROPS(size)>>>(a, v, size);
}

template <typename T>
void reorder_matrix(const T* data, const int* indexes, const size_t cols, const size_t rows, const bool reverse, T* out) {
    kernel_reorder_matrix<<<KERNEL_PROPS(cols * rows)>>>(data, indexes, cols, rows, reverse, out);
}

void insert_matrix(matrix_s& dest, const matrix_s& src, size_t start_row, size_t start_col) {
    kernel_insert_matrix<<<KERNEL_PROPS(src.elements)>>>(dest, src, start_row, start_col);
}

void matrix_copy(const matrix_s& src, matrix_s& dst, size_t start_row, size_t start_col) {
    kernel_matrix_copy<<<KERNEL_PROPS(dst.elements)>>>(src, dst, start_row, start_col);
}

void map_to_float3(const matrix_s& matrix, array_s<float3>& vector) {
    kernel_map_to_float3<<<KERNEL_PROPS(matrix.rows)>>>(matrix, vector);
}

void map_from_float3(matrix_s& matrix, const array_s<float3>& vector) {
    kernel_map_from_float3<<<KERNEL_PROPS(matrix.rows)>>>(matrix, vector);
}
