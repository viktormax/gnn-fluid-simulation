#pragma once

#include "array_d.h"
#include "utils.h"
#include <stdexcept>
#include "kernels.h"

// Power ^

template <typename T>
__global__ void kernelPowArray(const T* a, const double v, T* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (; index < size; index += stride) {
        out[index] = pow(a[index], v);
    }
}

// Add +

template <typename T>
__global__ void kernelAddArray(const T* a, const T* b, T* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (; index < size; index += stride) {
        out[index] = a[index] + b[index];
    }
}

template <typename T>
__global__ void kernelAddArray(const T* a, const T v, T* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (; index < size; index += stride) {
        out[index] = a[index] + v;
    }
}

__global__ void kernelAddArray(const float* a, const double v, float* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (; index < size; index += stride) {
        out[index] = a[index] + v;
    }
}

// Sub -

template <typename T>
__global__ void kernelSubArray(const T* a, const T* b, T* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (; index < size; index += stride) {
        out[index] = a[index] - b[index];
    }
}

template <typename T>
__global__ void kernelSubArray(const T* a, const T v, T* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (; index < size; index += stride) {
        out[index] = a[index] - v;
    }
}

__global__ void kernelSubArray(const float a, const point* b, point* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (; index < size; index += stride) {
        out[index] = a - b[index];
    }
}

// Mul *

template <typename T>
__global__ void kernelMulArray(const T* a, const T* b, T* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (; index < size; index += stride) {
        out[index] = a[index] * b[index];
    }
}

__global__ void kernelMulArray(const point* a, const float* b, point* out, const size_t size) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (; index < size; index += stride) {
        out[index] = a[index] * b[index];
    }
}

template <typename T>
__global__ void kernelMulArray(const T* a, const T v, T* out, const size_t count) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (; index < count; index += stride) {
        out[index] = a[index] * v;
    }
}

__global__ void kernelMulArray(const float* a, const float* b, float* out, const size_t count) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (; index < count; index += stride) {
        out[index] = a[index] * b[index];
    }
}

__global__ void kernelMulArray(const point* a, const float v, point* out, const size_t count) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (; index < count; index += stride) {
        out[index] = a[index] * v;
    }
}

// Div /

__global__ void kernelDivArray(const point* a, const float v, point* out, const size_t count) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (; index < count; index += stride) {
        out[index] = a[index] / v;
    }
}

__global__ void kernelDivArray(const point* a, const float* b, point* out, const size_t count) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (; index < count; index += stride) {
        out[index] = a[index] / b[index];
    }
}

template <typename T>
__global__ void kernelDivArray(const T* a, const T v, T* out, const size_t count) {
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (; index < count; index += stride) {
        out[index] = a[index] / v;
    }
}


array_d<point> operator+(const array_d<point>& a, const point& b) {
    array_d<point> out(a.count);
    kernelAddArray<<<KERNEL_PROPS(a.count)>>>(a(), b, out(), a.count);
    return out;
}

array_d<point> operator+(const array_d<point>& a, const array_d<point>& b) {
    if (a.count != b.count) {
        throw std::invalid_argument("Invalid array size");
    }

    array_d<point> out(a.count);
    kernelAddArray<<<KERNEL_PROPS(a.count)>>>(a(), b(), out(), a.count);
    return out;
}

array_d<float> operator+(const array_d<float>& a, const array_d<float>& b) {
    if (a.count != b.count) {
        throw std::invalid_argument("Invalid array size");
    }

    array_d<float> out(a.count);
    kernelAddArray<<<KERNEL_PROPS(a.count)>>>(a(), b(), out(), a.count);
    return out;
}

array_d<float> operator+(const array_d<float>& a, const double b) {
    array_d<float> out(a.count);
    kernelAddArray<<<KERNEL_PROPS(a.count)>>>(a(), b, out(), a.count);
    return out;
}

array_d<point> operator-(const array_d<point>& b) {
    array_d<point> out(b.count);
    kernelSubArray<<<KERNEL_PROPS(b.count)>>>(0, b(), out(), b.count);
    return out;
}

array_d<point> operator-(const array_d<point>& a, const array_d<point>& b) {
    if (a.count != b.count) {
        throw std::invalid_argument("Invalid array size");
    }

    array_d<point> out(a.count);
    kernelSubArray<<<KERNEL_PROPS(a.count)>>>(a(), b(), out(), a.count);
    return out;
}

array_d<float> operator-(const array_d<float>& a, const array_d<float>& b) {
    if (a.count != b.count) {
        throw std::invalid_argument("Invalid array size");
    }

    array_d<float> out(a.count);
    kernelSubArray<<<KERNEL_PROPS(a.count)>>>(a(), b(), out(), a.count);
    return out;
}

array_d<float> operator-(const array_d<float>& a, const float& b) {
    array_d<float> out(a.count);
    kernelSubArray<<<KERNEL_PROPS(a.count)>>>(a(), b, out(), a.count);
    return out;
}

array_d<point> operator*(const array_d<point>& a, const float& b) {
    array_d<point> out(a.count);
    kernelMulArray<<<KERNEL_PROPS(a.count)>>>(a(), b, out(), a.count);
    return out;
}

array_d<point> operator*(const array_d<point>& a, const array_d<float>& b) {
    if (a.count != b.count) {
        throw std::invalid_argument("Invalid array size");
    }

    array_d<point> out(a.count);
    kernelMulArray<<<KERNEL_PROPS(a.count)>>>(a(), b(), out(), a.count);
    return out;
}

array_d<float> operator*(const array_d<float>& a, const array_d<float>& b) {
    if (a.count != b.count) {
        throw std::invalid_argument("Invalid array size");
    }

    array_d<float> out(a.count);
    kernelMulArray<<<KERNEL_PROPS(a.count)>>>(a(), b(), out(), a.count);
    return out;
}

array_d<float> operator*(const array_d<float>& a, const float& b) {
    array_d<float> out(a.count);
    kernelMulArray<<<KERNEL_PROPS(a.count)>>>(a(), b, out(), a.count);
    return out;
}

array_d<point> operator/(const array_d<point>& a, const float& b) {
    array_d<point> out(a.count);
    kernelDivArray<<<KERNEL_PROPS(a.count)>>>(a(), b, out(), a.count);
    return out;
}

array_d<point> operator/(const array_d<point>& a, const array_d<float>& b) {
    if (a.count != b.count) {
        throw std::invalid_argument("Invalid array size");
    }

    array_d<point> out(a.count);
    kernelDivArray<<<KERNEL_PROPS(a.count)>>>(a(), b(), out(), a.count);
    return out;
}

array_d<float> operator/(const array_d<float>& a, const float& b) {
    array_d<float> out(a.count);
    kernelDivArray<<<KERNEL_PROPS(a.count)>>>(a(), b, out(), a.count);
    return out;
}

array_d<float> operator/(const array_d<float>& a, const array_d<float>& b) {
    array_d<float> out(a.count);
    div_vector(a(), b(), 1, out(), a.count);
    return out;
}

array_d<float> pow(const array_d<float>& a, const double p) {
    array_d<float> out(a.count);
    kernelPowArray<<<KERNEL_PROPS(a.count)>>>(a(), p, out(), a.count);
    return out;
}

array_d<float> sqrt(const array_d<float>& a) {
    array_d<float> out(a.count);
    sqrt_vector(a(), out(), a.count);
    return out;
}

array_d<float> max(const array_d<float> &in, const float max) {
    array_d<float> out(in.count);
    max_vector(in(), out(), max, out.count);
    return out;
}
