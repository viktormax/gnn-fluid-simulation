#include "matrix_d.h"

#include <cuda_runtime.h>

#include <stdexcept>

#include "MemoryManager.hpp"
#include "kernels.h"
#include "matrix_h.h"

cublasHandle_t matrix_d::_cublas_handle = nullptr;

array_d<float> matrix_d::_ones;

matrix_d::matrix_d() : bytes(_bytes), rows(_rows), cols(_cols), elements(_elements) {
    _ptr = nullptr;
    _rows = 0;
    _cols = 0;
    _elements = 0;
    _bytes = 0;
    _allocated_bytes = 0;
}

matrix_d::matrix_d(const std::initializer_list<std::initializer_list<float>>& data): matrix_d(data.size(), data.begin()->size()) {
    *this = matrix_h(data);
}

matrix_d::~matrix_d() {
    this->free();
}

matrix_d::matrix_d(const size_t rows, const size_t cols) : matrix_d() {
    resize(rows, cols);
}

matrix_d::matrix_d(mat_size size) : matrix_d(size.rows, size.cols) {
}

matrix_d::matrix_d(matrix_d&& that) noexcept : matrix_d() {
    _ptr = that._ptr;
    _rows = that._rows;
    _cols = that._cols;
    _elements = that._elements;
    _bytes = that._bytes;
    _allocated_bytes = that._allocated_bytes;
    that._ptr = nullptr;
    that._rows = 0;
    that._cols = 0;
    that._elements = 0;
    that._bytes = 0;
    that._allocated_bytes = 0;
}

matrix_d::matrix_d(const matrix_d& that) : matrix_d(that.rows, that.cols) {
    cudaMemcpy(_ptr, that._ptr, that.bytes, cudaMemcpyDeviceToDevice);
}

matrix_d::matrix_d(const matrix_h& that) : matrix_d(that.rows, that.cols) {
    cudaMemcpy(_ptr, that(), that.bytes, cudaMemcpyHostToDevice);
}

matrix_d& matrix_d::operator=(matrix_d&& that) {
    if (_ptr != nullptr) {
        memory_manager.cache(_ptr, _allocated_bytes);
    }

    _ptr = that._ptr;
    _rows = that._rows;
    _cols = that._cols;
    _elements = that._elements;
    _bytes = that._bytes;
    _allocated_bytes = that._allocated_bytes;
    that._ptr = nullptr;
    return *this;
}

matrix_d& matrix_d::operator=(const matrix_d& that) {
    resize(that.rows, that.cols);
    cudaMemcpy(_ptr, that._ptr, that.bytes, cudaMemcpyDeviceToDevice);
    return *this;
}

matrix_d& matrix_d::operator=(const matrix_h& that) {
    resize(that.rows, that.cols);
    cudaMemcpy(_ptr, that(), that.bytes, cudaMemcpyHostToDevice);
    return *this;
}

matrix_d& matrix_d::resize(const size_t rows, const size_t cols) {
    _rows = rows;
    _cols = cols;
    _elements = _rows * _cols;
    _bytes = _elements * sizeof(float);

    if (_bytes <= _allocated_bytes) {
        return *this;
    }

    if (_ptr != nullptr) {
        memory_manager.cache(_ptr, _allocated_bytes);
    }

    _allocated_bytes = _bytes;
    _ptr = (float*)memory_manager.restore(_allocated_bytes);

    return *this;
}

matrix_d& matrix_d::init(const float value) {
    if (value == 0) {
        cudaMemset(_ptr, 0, bytes);
    } else {
        set_vector(_ptr, value, elements);
    }

    return *this;
}

matrix_d& matrix_d::free() {
    if (_ptr == nullptr) {
        return *this;
    }

    memory_manager.cache(_ptr, _allocated_bytes);

    _ptr = nullptr;
    _rows = 0;
    _cols = 0;
    _elements = 0;
    _bytes = 0;
    _allocated_bytes = 0;

    return *this;
}

matrix_s matrix_d::str() {
    return matrix_s(_ptr, _rows, _cols);
}

const matrix_s matrix_d::str() const {
    return matrix_s(_ptr, _rows, _cols);
}

matrix_d matrix_d::transpose() const {
    matrix_d out(cols, rows);
    float alpha = 1.0f;
    float beta = 0.0f;

    cublasSgeam(matrix_d::getCublasHandler(), CUBLAS_OP_T, CUBLAS_OP_N,
                _rows, _cols, &alpha, _ptr, _cols, &beta, _ptr, _rows, out(), _rows);

    return out;
}

matrix_d& matrix_d::insert(const matrix_d& that, size_t start_row, size_t start_col) {
    insert_matrix(this->str(), that.str(), start_row, start_col);
    return *this;
}

matrix_d matrix_d::copy(size_t start_row, size_t start_col, size_t row_count, size_t col_count) const {
    row_count = row_count == 0 ? rows : row_count;
    col_count = col_count == 0 ? cols : col_count;
    row_count = std::min(row_count, rows - start_row);
    col_count = std::min(col_count, cols - start_col);
    matrix_d dst(row_count, col_count);
    matrix_copy(str(), dst.str(), start_row, start_col);
    return dst;
}

array_d<float> matrix_d::sumRows() const {
    const float alpha = 1.0f;
    const float beta = 0.0f;

    array_d<float> row_sum(rows);
    if (_ones.count < cols) {
        _ones.resize(cols, 1);
    }

    cublasSgemv(matrix_d::getCublasHandler(), CUBLAS_OP_T, cols, rows,
                &alpha, _ptr, cols, _ones(), 1, &beta, row_sum(), 1);

    return row_sum;
}

array_d<float> matrix_d::sumCols() const {
    const float alpha = 1.0f;
    const float beta = 0.0f;

    array_d<float> col_sum(cols);
    if (_ones.count < rows) {
        _ones.resize(rows, 1);
    }

    cublasSgemv(matrix_d::getCublasHandler(), CUBLAS_OP_N, cols, rows,
        &alpha, _ptr, cols, _ones(), 1, &beta, col_sum(), 1);

    return col_sum;
}

matrix_d matrix_d::pow(const float p) const {
    matrix_d result(rows, cols);
    pow_vector(_ptr, p, result._ptr, elements);
    return result;
}

matrix_d matrix_d::operator-(const matrix_d& b) const {
    if (cols != b.cols || rows != b.rows) throw std::invalid_argument("Invalid matrix size");

    matrix_d result(rows, cols);
    sub_vector(_ptr, b._ptr, b.elements, result(), elements);
    return result;
}

matrix_d matrix_d::operator-(const array_d<float>& b) const {
    if (cols != b.count) throw std::invalid_argument("Invalid array size");

    matrix_d result(rows, cols);
    sub_vector(_ptr, b(), cols, result._ptr, elements);
    return result;
}

matrix_d matrix_d::operator-(const float& b) const {
    matrix_d result(rows, cols);
    sub_vector(_ptr, b, result._ptr, elements);
    return result;
}

matrix_d matrix_d::operator+(const matrix_d& b) const {
    if (cols != b.cols || rows != b.rows) throw std::invalid_argument("Invalid matrix size");

    matrix_d result(rows, cols);
    add_vector(_ptr, b._ptr, b.elements, result(), elements);
    return result;
}

matrix_d matrix_d::operator+(const array_d<float>& b) const {
    if (cols != b.count) throw std::invalid_argument("Invalid array size");

    matrix_d result(rows, cols);
    add_vector(_ptr, b(), cols, result._ptr, elements);
    return result;
}

matrix_d matrix_d::operator+(const float& b) const {
    matrix_d result(rows, cols);
    add_vector(_ptr, b, cols, result._ptr, elements);
    return result;
}

matrix_d matrix_d::operator*(const matrix_d& b) const {
    if (rows != b.rows || cols != b.cols) throw std::invalid_argument("Invalid matrix size");

    matrix_d result(rows, cols);
    mul_vector(_ptr, b._ptr, b.elements, result._ptr, elements);
    return result;
}

matrix_d matrix_d::operator*(const array_d<float>& b) const {
    if (cols != b.count) throw std::invalid_argument("Invalid matrix size");

    matrix_d result(rows, cols);
    mul_vector(_ptr, b(), cols, result._ptr, elements);
    return result;
}

matrix_d matrix_d::operator*(const float b) const {
    matrix_d result(rows, cols);
    mul_vector(_ptr, b, result._ptr, elements);
    return result;
}

matrix_d matrix_d::operator/(const matrix_d& b) const {
    if (rows != b.rows || cols != b.cols) throw std::invalid_argument("Invalid matrix size");

    matrix_d result(rows, cols);
    div_vector(_ptr, b._ptr, b.elements, result._ptr, elements);
    return result;
}

matrix_d matrix_d::operator/(const float b) const {
    matrix_d result(rows, cols);
    div_vector(_ptr, b, result._ptr, elements);
    return result;
}

bool matrix_d::operator==(const matrix_d& other) const {
    return matrix_h(*this) == matrix_h(other);
}

matrix_d& matrix_d::fromStream(std::ifstream& rf) {
    matrix_h h;
    *this = h.fromStream(rf);
    return *this;
}

const matrix_d& matrix_d::toStream(std::ofstream& wf) const {
    matrix_h h = *this;
    h.toStream(wf);
    return *this;
}

std::ostream& operator<<(std::ostream& ss, const matrix_d& m) {
    return ss << matrix_h(m);
}

// matrix_d matrix_d::dot(const matrix_d& b) const {
//    float alpha = 1.0f;
//    float beta = 0.0f;

//    if (cols != b.rows) {
//        throw std::invalid_argument("Invalid matrix size");
//    }

//    matrix_d c(rows, b.cols);
//    cublasSgemm(matrix_d::getCublasHandler(), CUBLAS_OP_N, CUBLAS_OP_N, b.cols, rows,
//                cols, &alpha, b(), b.cols, _ptr, cols, &beta, c(), b.cols);

//    return c;
// }

// matrix_d matrix_d::dotAT(const matrix_d& b) const {
//    float alpha = 1.0f;
//    float beta = 0.0f;

//    if (rows != b.rows) {
//        throw std::invalid_argument("Invalid matrix size");
//    }

//    matrix_d c(cols, b.cols);
//    cublasSgemm(matrix_d::getCublasHandler(), CUBLAS_OP_N, CUBLAS_OP_T, b.cols, cols,
//                rows, &alpha, b(), b.cols, _ptr, cols, &beta, c(), b.cols);

//    return c;
// }

void matrix_d::print(const size_t max_row, const size_t max_col) const {
    matrix_h h = *this;
    h.print(max_row, max_col);
}

matrix_d& matrix_d::reorderCols(const array_d<int>& indexes, const bool reverse) {
    if (cols != indexes.count) throw std::invalid_argument("Invalid indexes size");
    float* tmp_ptr = (float*)memory_manager.restore(_allocated_bytes);

    reorder_matrix(_ptr, indexes(), cols, rows, reverse, tmp_ptr);
    memory_manager.cache(_ptr, _allocated_bytes);
    _ptr = tmp_ptr;

    return *this;
}

const cublasHandle_t& matrix_d::getCublasHandler() {
    if (_cublas_handle == nullptr) {
        cublasCreate(&_cublas_handle);
    }
    return _cublas_handle;
}

const array_d<float>& matrix_d::ones(size_t size) {
    if (_ones.count < size) {
        _ones.resize(size, 1);
    }
    return _ones;
}

// TODO: remove

void dotMatrix(const matrix_d& a, const matrix_d& b, matrix_d& c, float alpha, float beta) {
    if (a.cols != b.rows) throw std::invalid_argument("Invalid matrix size");

    c.resize(a.rows, b.cols);
    cublasSgemm(matrix_d::getCublasHandler(), CUBLAS_OP_N, CUBLAS_OP_N,
                b.cols, a.rows, a.cols, &alpha, b(), b.cols, a(),
                a.cols, &beta, c(), b.cols);
}

void dotMatrixAT(const matrix_d& a, const matrix_d& b, matrix_d& c, float alpha, float beta) {
    if (a.rows != b.rows) throw std::invalid_argument("Invalid matrix size");

    c.resize(a.cols, b.cols);
    cublasSgemm(matrix_d::getCublasHandler(), CUBLAS_OP_N, CUBLAS_OP_T,
                b.cols, a.cols, a.rows, &alpha, b(), b.cols, a(),
                a.cols, &beta, c(), b.cols);
}

void dotMatrixBT(const matrix_d& a, const matrix_d& b, matrix_d& c, float alpha, float beta) {
    if (a.cols != b.cols) throw std::invalid_argument("Invalid matrix size");

    c.resize(a.rows, b.rows);
    cublasSgemm(matrix_d::getCublasHandler(), CUBLAS_OP_T, CUBLAS_OP_N,
                b.rows, a.rows, a.cols, &alpha, b(), b.cols, a(),
                a.cols, &beta, c(), b.rows);
}
