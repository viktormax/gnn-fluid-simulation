#include "array_d.h"

#include <cuda_runtime_api.h>

#include <stdexcept>

#include "MemoryManager.hpp"
#include "array_h.h"
#include "kernels.h"

template <typename T>
array_d<T>::array_d() : count(_count), bytes(_bytes) {
    _count = 0;
    _bytes = 0;
    _allocated_bytes = 0;
    _ptr = nullptr;
}

template<typename T>
array_d<T>& array_d<T>::fromStream(std::ifstream& rf) {
    array_h<T> h;
    *this = h.fromStream(rf);
    return *this;
}

template<typename T>
const array_d<T>& array_d<T>::toStream(std::ofstream& wf) const {
    array_h<T> h = *this;
    h.toStream(wf);
    return *this;
}

template <typename T>
array_d<T>::~array_d() {
    free();
}

template <typename T>
array_d<T>::array_d(const std::initializer_list<T>& data) : array_d(data.size()) {
    *this = array_h(data);
}

template <typename T>
array_d<T>::array_d(const size_t count) : array_d() {
    resize(count);
}

template <typename T>
array_d<T>::array_d(const array_d<T>& that) : array_d(that.count) {
    cudaMemcpy(_ptr, that._ptr, that.bytes, cudaMemcpyDeviceToDevice);
}

template <typename T>
array_d<T>::array_d(const array_h<T>& that) : array_d(that.count) {
    cudaMemcpy(_ptr, that(), that.bytes, cudaMemcpyHostToDevice);
}

template <typename T>
array_d<T>::array_d(array_d<T>&& that) : array_d() {
    this->_count = that._count;
    this->_bytes = that._bytes;
    this->_allocated_bytes = that._allocated_bytes;
    this->_ptr = that._ptr;
    that._ptr = nullptr;
    that._bytes = 0;
    that._count = 0;
    that._allocated_bytes = 0;
}

template <typename T>
array_d<T>& array_d<T>::operator=(const array_d<T>& that) {
    resize(that.count);
    cudaMemcpy(_ptr, that._ptr, that.bytes, cudaMemcpyDeviceToDevice);
    return *this;
}

template <typename T>
array_d<T>& array_d<T>::operator=(const array_h<T>& that) {
    resize(that.count);
    cudaMemcpy(_ptr, that(), that.bytes, cudaMemcpyHostToDevice);
    return *this;
}

template <typename T>
array_d<T>& array_d<T>::operator=(array_d<T>&& that) {
    if (_ptr != nullptr) {
        memory_manager.cache(_ptr, _allocated_bytes);
    }

    this->_count = that._count;
    this->_bytes = that._bytes;
    this->_allocated_bytes = that._allocated_bytes;
    this->_ptr = that._ptr;
    that._ptr = nullptr;
    return *this;
}

template <typename T>
bool array_d<T>::operator==(const array_d<T>& that) const {
    return array_h(*this) == array_h(that);
}

template <typename T>
array_d<T>& array_d<T>::init(const T val) {
    char zero[sizeof(T)] = { 0 };
    if (memcmp((char*)&val, zero, sizeof(T)) == 0) {
        cudaMemset(_ptr, 0, bytes);
        return *this;
    }

    set_vector(_ptr, val, count);
    return *this;
}

template <typename T>
array_d<T>& array_d<T>::resize(const size_t count, std::optional<T> fill_empty) {
    size_t original_bytes = _bytes;
    size_t original_count = _count;
    _bytes = count * sizeof(T);
    _count = count;

    if (bytes <= _allocated_bytes || bytes == 0) {
        if (fill_empty.has_value()) {
            set_vector(_ptr + original_count, fill_empty.value(), count - original_count);
        }

        return *this;
    }

    long extra = _allocated_bytes + 20 * sizeof(T);
    _allocated_bytes = bytes > extra ? bytes : extra;
    T* tmp_ptr = (T*)memory_manager.restore(_allocated_bytes);

    if (_ptr != nullptr) {
        cudaMemcpy(tmp_ptr, _ptr, original_bytes, cudaMemcpyDeviceToDevice);
        memory_manager.cache(_ptr, original_bytes);
    }

    _ptr = tmp_ptr;

    if (fill_empty.has_value()) {
        set_vector(_ptr + original_count, fill_empty.value(), count - original_count);
    }

    return *this;
}

template<typename T>
array_d<T>& array_d<T>::free() {
    if (_ptr == nullptr) {
        return *this;
    }

    memory_manager.cache(_ptr, _allocated_bytes);

    _count = 0;
    _bytes = 0;
    _allocated_bytes = 0;
    _ptr = nullptr;

    return *this;
}

template<typename T>
array_s<T> array_d<T>::str() {
    return array_s(_ptr, count);
}

template<typename T>
const array_s<T> array_d<T>::str() const {
    return array_s(_ptr, count);
}

template <typename T>
array_d<T>& array_d<T>::insert(const array_d<T>& that, const size_t start_index) {
    if (start_index + that.count > _count) {
        resize(start_index + that.count);
    }

    cudaMemcpy(_ptr + start_index, that._ptr, that.bytes, cudaMemcpyDeviceToDevice);

    return *this;
}

template <typename T>
array_d<T> array_d<T>::copy(const size_t start_index, size_t count) const {
    if (count == 0) {
        count = _count - start_index;
    }

    count = min(_count - start_index, count);

    array_d<T> cpy(count);
    cudaMemcpy(cpy._ptr, _ptr + start_index, cpy.bytes, cudaMemcpyDeviceToDevice);
    return cpy;
}

template <typename T>
array_d<T>& array_d<T>::append(const array_d<T>& that) {
    long offset = count;
    resize(count + that.count);
    cudaMemcpy(_ptr + offset, that._ptr, that.bytes, cudaMemcpyDeviceToDevice);
    return *this;
}

template <typename T>
array_d<T>& array_d<T>::reorder(const array_d<int>& indexes, const bool reverse) {
    if (count != indexes.count) throw std::invalid_argument("Invalid indexes size");
    T* tmp_ptr = (T*)memory_manager.restore(_allocated_bytes);

    reorder_matrix(_ptr, indexes(), count, 1, reverse, tmp_ptr);
    memory_manager.cache(_ptr, _allocated_bytes);
    _ptr = tmp_ptr;

    return *this;
}
