#pragma once

#include <deque>

#include "array_d.h"
#include "MemoryManager.hpp"

template <typename T>
class array_ptr {
public:
    const size_t& count;

    array_ptr() = delete;

    array_ptr(const array_ptr&) = delete;

    array_ptr(std::deque<array_d<T>> queue) : count(_count), _count(queue.size()), _ptr(nullptr) {
        T** host = new T*[count];
        for (int i = 0; i < count; i++) {
            host[i] = queue[i]();
        }

        init(host);    

        delete[] host;
    }

    ~array_ptr() {
        if (_ptr != nullptr) {
            memory_manager.cache(_ptr, _allocated_bytes);
        }
    }

    inline T** operator()() {
        return _ptr;
    }

    inline const T* const* operator()() const {
        return _ptr;
    }

private:
    T** _ptr;
    size_t _count;
    size_t _allocated_bytes;

    void init(T** host) {
        if (count == 0) {
            return;
        }

        _allocated_bytes = sizeof(T*) * count;
        _ptr = (T**)memory_manager.restore(_allocated_bytes);
        cudaMemcpy(_ptr, host, _allocated_bytes, cudaMemcpyHostToDevice);
    }
};