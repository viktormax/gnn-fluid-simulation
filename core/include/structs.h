#pragma once

#include <cuda_runtime_api.h>
#include "constants.h"

struct array_stats {
    float min = 0;
    float max = 0;
    float mean = 0;
    float stddev = 0;
    float count = 0;
};

struct mat_size {
    size_t rows;
    size_t cols;
};

struct physics_s {
    float radius = CONECTIVITY_RADIUS;
    int maxNeighbours = MAX_NEIGHBOURS;
    int maxGrixDims = MAX_GRID_DIMS;
    point gravity = make_point(0, GRAVITY_Y, 0);
    float restDistanceRatio = REST_DISTANCE_RATIO;
    float maxSpeed = MAX_SPEED;
    float cohesion = COHESION;
    float surfaceTension = SURFACE_TENSION;
    float relaxationFactor = RELAXATION_FACTOR;
    float viscosity = VISCOSITY;
    float damp = DAMP;
};

struct plane_s {
    float3 normal;
    float d;
    float3 p[4];
};
