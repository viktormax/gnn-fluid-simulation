#pragma once

template <typename T>
struct array_s {
    T* data;
    const size_t count;

    array_s(T* data, size_t count) : data(data), count(count) {
    }

    __device__ __inline__ T& operator[](const size_t index) {
        CHECK_BOUNDS(count, index);
        return data[index];
    };

    __device__ __inline__ T const& operator[](const size_t index) const {
        CHECK_BOUNDS(count, index);
        return data[index];
    };
};
