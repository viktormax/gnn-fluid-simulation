#include "array_d.h"

array_d<point> operator+(const array_d<point>& a, const point& b);

array_d<float> operator+(const array_d<float>& a, const double b);

array_d<point> operator+(const array_d<point>& a, const array_d<point>& b);

array_d<float> operator+(const array_d<float>& a, const array_d<float>& b);

array_d<point> operator-(const array_d<point>& b);

array_d<point> operator-(const array_d<point>& a, const array_d<point>& b);

array_d<float> operator-(const array_d<float>& a, const array_d<float>& b);

array_d<float> operator-(const array_d<float>& a, const float& b);

array_d<point> operator*(const array_d<point>& a, const float& b);

array_d<point> operator*(const array_d<point>& a, const array_d<float>& b);

array_d<float> operator*(const array_d<float>& a, const array_d<float>& b);

array_d<float> operator*(const array_d<float>& a, const float& b);

array_d<point> operator/(const array_d<point>& a, const float& b);

array_d<point> operator/(const array_d<point>& a, const array_d<float>& b);

array_d<float> operator/(const array_d<float>& a, const float& b);

array_d<float> operator/(const array_d<float>& a, const array_d<float>& b);

array_d<float> sqrt(const array_d<float>& a);

array_d<float> pow(const array_d<float>& a, const double p);

array_d<float> sqrt(const array_d<float>& a);

array_d<float> max(const array_d<float>& in, const float max);
