#pragma once

#include <cublas_v2.h>

#include <fstream>

#include "array_d.h"
#include "structs.h"
#include "matrix_s.h"

class matrix_h;

class matrix_d {
   private:
    float* _ptr;
    size_t _rows;
    size_t _cols;
    size_t _elements;
    size_t _bytes;
    size_t _allocated_bytes;

    static cublasHandle_t _cublas_handle;
    static array_d<float> _ones;

   public:
    const size_t& rows;
    const size_t& cols;
    const size_t& elements;
    const size_t& bytes;

    matrix_d();

    matrix_d(const std::initializer_list<std::initializer_list<float>>& data);

    ~matrix_d();

    matrix_d(const size_t rows, const size_t cols);

    matrix_d(mat_size size);

    matrix_d(matrix_d&& that) noexcept;

    matrix_d(const matrix_d& that);

    matrix_d(const matrix_h& that);

    matrix_d& operator=(matrix_d&& that);

    matrix_d& operator=(const matrix_d& that);

    matrix_d& operator=(const matrix_h& that);

    matrix_d& resize(const size_t rows, const size_t cols);

    matrix_d& init(const float value);

    matrix_d& free();

    matrix_s str();

    const matrix_s str() const;

    matrix_d transpose() const;

    matrix_d& insert(const matrix_d& that, size_t start_row = 0, size_t start_col = 0);

    matrix_d copy(size_t start_row = 0, size_t start_col = 0, size_t row_count = 0, size_t col_count = 0) const;

    array_d<float> sumRows() const;

    array_d<float> sumCols() const;

    matrix_d pow(const float p) const;

    matrix_d operator-(const matrix_d& b) const;

    matrix_d operator-(const array_d<float>& b) const;

    matrix_d operator-(const float& b) const;

    matrix_d operator+(const matrix_d& b) const;

    matrix_d operator+(const array_d<float>& b) const;

    matrix_d operator+(const float& b) const;

    matrix_d operator*(const matrix_d& b) const;

    matrix_d operator*(const array_d<float>& b) const;

    matrix_d operator*(const float b) const;

    matrix_d operator/(const matrix_d& b) const;

    matrix_d operator/(const float b) const;

    bool operator==(const matrix_d& other) const;

    matrix_d& fromStream(std::ifstream& rf);

    const matrix_d& toStream(std::ofstream& wf) const;

    friend std::ostream& operator<<(std::ostream& ss, const matrix_d& m);

    // TODO:
    // matrix_d dot(const matrix_d& b) const;
    // TODO:
    // matrix_d dotAT(const matrix_d& b) const;
    // TODO:
    // matrix_d dotBT(const matrix_d& b) const;

    void print(const size_t max_row = 10, const size_t max_col = 10) const;

    matrix_d& reorderCols(const array_d<int>& indexes, const bool reverse = false);

    static const cublasHandle_t& getCublasHandler();

    static const array_d<float>& ones(size_t size);

    inline float const* operator()() const {
        return _ptr;
    }

    inline float* operator()() {
        return _ptr;
    }

    mat_size size() const {
        return { rows, cols };
    }
};

// TODO: remove
void dotMatrixAT(const matrix_d& a, const matrix_d& b, matrix_d& c, float alpha = 1.0f, float beta = 0.0f);
// TODO: remove
void dotMatrix(const matrix_d& a, const matrix_d& b, matrix_d& c, float alpha = 1.0f, float beta = 0.0f);
// TODO: remove
void dotMatrixBT(const matrix_d& a, const matrix_d& b, matrix_d& c, float alpha = 1.0f, float beta = 0.0f);
