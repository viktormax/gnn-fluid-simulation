#pragma once

#include <fstream>
#include <iomanip>
#include <sstream>

#include "constants.h"

class matrix_d;

class matrix_h {
   private:
    float* _ptr;
    size_t _rows;
    size_t _cols;
    size_t _elements;
    size_t _bytes;
    size_t _allocated_bytes;

   public:
    const size_t& rows;
    const size_t& cols;
    const size_t& elements;
    const size_t& bytes;

    matrix_h();

    matrix_h(const std::initializer_list<std::initializer_list<float>>& data);

    ~matrix_h();

    matrix_h(const size_t rows, const size_t cols);

    matrix_h(matrix_h&& that) noexcept;

    matrix_h(const matrix_h& that);

    matrix_h(const matrix_d& that);

    matrix_h& operator=(matrix_h&& that);

    matrix_h& operator=(const matrix_h& that);

    matrix_h& operator=(const matrix_d& that);

    bool operator==(const matrix_h& that) const;

    matrix_h& fromStream(std::ifstream& rf);

    const matrix_h& toStream(std::ofstream& wf) const;

    friend std::ostream& operator<<(std::ostream& ss, const matrix_h& m);

    matrix_h& resize(const size_t rows, const size_t cols);

    matrix_h& init(const float value);

    matrix_h& free();

    matrix_h& transpose();

    const matrix_h& print(const size_t max_row = 10, const size_t max_col = 10) const;

    inline float const* operator()() const {
        return _ptr;
    }

    inline float* operator()() {
        return _ptr;
    }

    inline float const& operator[](const size_t idx) const {
        CHECK_BOUNDS(elements, idx);
        return _ptr[idx];
    }

    inline float& operator[](const size_t idx) {
        CHECK_BOUNDS(elements, idx);
        return _ptr[idx];
    }

    inline float const& operator()(const size_t row, const size_t col) const {
        CHECK_BOUNDS2D(rows, cols, row, col);
        return (_ptr + row * cols)[col];
    }

    inline float& operator()(const size_t row, const size_t col) {
        CHECK_BOUNDS2D(rows, cols, row, col);
        return (_ptr + row * cols)[col];
    }
};
