﻿#pragma once

#include <cuda_runtime_api.h>

#include <fstream>
#include <iomanip>
#include <optional>
#include <sstream>

template <typename T>
class array_d;

template <typename T>
class array_h;

std::ostream& operator<<(std::ostream& ss, const float3& p);

template <typename T>
std::ostream& operator<<(std::ostream& ss, const array_h<T>& arr) {
    ss << "[";
    for (size_t i = 0, end = arr.count - 1; i < arr.count; i++) {
        ss << arr[i];
        if (i != end) {
            ss << ", ";
        }
    }
    return ss << "]";
}

template <typename T>
class array_h {
   private:
    T* _ptr;        // Pointer to data.
    size_t _count;  // Number of items.
    size_t _bytes;  // Size of array in bytes.
    size_t _allocated_bytes;

   public:
    const size_t& bytes;
    const size_t& count;

    array_h();

    ~array_h();

    array_h(const std::initializer_list<T>& data);

    array_h(const size_t count);

    array_h(array_h<T>&& that);

    array_h(const array_h<T>& that);

    array_h(const array_d<T>& that);

    array_h<T>& operator=(array_h<T>&& that);

    array_h<T>& operator=(const array_h<T>& that);

    array_h<T>& operator=(const array_d<T>& that);

    bool operator==(const array_h<T>& that) const;

    array_h& init(const T val);

    array_h& push(const T value);

    array_h& resize(const size_t count, std::optional<T> fill_empty = std::nullopt);

    void print(const int max_col = 100) const;

    array_h& append(const array_h<T>& other);

    array_h& fromStream(std::ifstream& rf);

    const array_h& toStream(std::ofstream& wf) const;

    inline T* operator()() {
        return _ptr;
    }

    inline T const* operator()() const {
        return _ptr;
    }

    inline T& operator[](const size_t index) {
        return _ptr[index];
    }

    inline const T operator[](const size_t index) const {
        return _ptr[index];
    }

    friend std::ostream& operator<<<>(std::ostream& ss, const array_h<T>& arr);
};
