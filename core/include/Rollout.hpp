
#pragma once

#include "core.h"

struct rollout_meta {
    size_t particlesCount = 0;
    size_t staticCount = 0;
    size_t stepsCount = 0;
    size_t boundariesCount = 0;
    physics_s physics;
};

class Rollout {
public:
    physics_s physics;
    array_h<plane_s> boundaries;
    array_h<point> statics;
    std::vector<array_h<point>> locs;

    void fromStream(std::ifstream& file) {
        rollout_meta meta;
        file.read((char*)&meta, sizeof(meta));
        physics = meta.physics;

        if (meta.boundariesCount > 0) {
            boundaries.fromStream(file);
        }

        if (meta.staticCount > 0) {
            statics.fromStream(file);
        }

        locs.resize(meta.stepsCount);
        for (int i = 0; i < meta.stepsCount; i++) {
            locs[i].fromStream(file);
        }
    }

    bool load(const std::string& fileName) {
        std::ifstream file(fileName, std::ios::in | std::ios::binary);

        if (!file) {
            printf("Cannot load rollout!\n");
            return false;
        }

        fromStream(file);
        return true;
    }

    static rollout_meta loadMeta(const std::string& fileName) {
        std::ifstream file(fileName, std::ios::in | std::ios::binary);

        if (!file) {
            printf("Cannot open file!\n");
            return rollout_meta();
        }

        rollout_meta meta;
        file.read((char*)&meta, sizeof(meta));
        
        return meta;
    }

    void toStream(std::ofstream& file) {
        rollout_meta meta;
        meta.boundariesCount = boundaries.count;
        meta.particlesCount = locs[0].count;
        meta.staticCount = statics.count;
        meta.stepsCount = locs.size();
        meta.physics = physics;
        file.write((char*)&meta, sizeof(meta));

        if (meta.boundariesCount > 0) {
            boundaries.toStream(file);
        }

        if (meta.staticCount > 0) {
            statics.toStream(file);
        }

        for (int i = 0; i < meta.stepsCount; i++) {
            locs[i].toStream(file);
        }
    }

    void save(const std::string& fileName) {
        std::ofstream file(fileName, std::ios::out | std::ios::binary);

        if (!file) {
            printf("Cannot open file!\n");
            return;
        }

        toStream(file);
    }

    void setStatics(const array_h<point>& statics) {
        this->statics = statics;
    }

    void setBoundaries(const array_h<plane_s>& boundaries) {
        this->boundaries = boundaries;
    }

    void setPhysics(const physics_s& physics) {
        this->physics = physics;
    }

    void setLocs(const array_h<point>& locs) {
        this->locs.clear();
        pushLocs(locs);
    }

    void pushLocs(const array_h<point>& locs) {
        this->locs.push_back(locs);
    }

    void free() {
        locs.clear();
        statics = array_h<point>();
    }

    array_h<point> getStep(int step) const {
        if (step >= locs.size()) {
            return array_h<point>();
        }

        return locs[step];
    }
};
