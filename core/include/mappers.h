#pragma once

#include "matrix_d.h"
#include "array_d.h"
#include "matrix_h.h"
#include "array_h.h"

matrix_d toMatrix(const array_d<float3>& arr);

array_d<float3> toArray(const matrix_d& mat);

matrix_h toMatrix(const array_h<float3>& arr);

array_h<float3> toArray(const matrix_h& mat);

matrix_d toMatrix1d(const array_d<float>& arr);

array_d<float> toArray1d(const matrix_d& mat);
