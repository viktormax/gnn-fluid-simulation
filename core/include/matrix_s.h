#pragma once

struct matrix_s {
    float* data;
    const size_t rows;
    const size_t cols;
    const size_t elements;

    matrix_s(float* data, const size_t rows, const size_t cols) : data(data), rows(rows), cols(cols), elements(rows* cols) {
    }

    __device__ __inline__ float& operator[](const size_t index) {
        CHECK_BOUNDS(elements, index);
        return data[index];
    };

    __device__ __inline__ float operator[](const size_t index) const {
        CHECK_BOUNDS(elements, index);
        return data[index];
    };

    __device__ __inline__ float* row(const size_t row) {
        CHECK_BOUNDS2D(rows, cols, row, 0);
        return data + row * cols;
    };

    __device__ __inline__ const float* row(const size_t row) const {
        CHECK_BOUNDS2D(rows, cols, row, 0);
        return data + row * cols;
    };

    __device__ __inline__ float& operator()(const size_t row, const size_t col) {
        CHECK_BOUNDS2D(rows, cols, row, col);
        return (data + row * cols)[col];
    };

    __device__ __inline__ float const& operator()(const size_t row, const size_t col) const {
        CHECK_BOUNDS2D(rows, cols, row, col);
        return (data + row * cols)[col];
    };
};
