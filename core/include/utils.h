#pragma once

#include "constants.h"
#include "operators.h"
#include "structs.h"
#include "array_h.h"

inline __device__ __host__ float squared_distance(const float3 a, const float3 b) {
    float3 tmp = pow(a - b, 2);
    return tmp.x + tmp.y + tmp.z;
}

inline __device__ __host__ float squared_distance(const float2 a, const float2 b) {
    float2 tmp = pow(a - b, 2);
    return tmp.x + tmp.y;
}

inline __device__ __host__ float length(const float3 a) {
    return sqrtf(a.x * a.x + a.y * a.y + a.z * a.z);
}

inline __device__ __host__ float length(const float2 a) {
    return sqrtf(a.x * a.x + a.y * a.y);
}

inline __device__ __host__ float dot(const float3 a, const float3 b) {
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline __device__ __host__ int3 to_int3(const float3 v) {
    return make_int3(v.x, v.y, v.z);
}

inline __device__ __host__ int2 to_int2(const float2 v) {
    return make_int2(v.x, v.y);
}

inline __device__ __host__ bool is_in_bounds(const int3 p, const float3 grid_dims) {
    return p.x >= 0 && p.y >= 0 && p.z >= 0 && p.x < grid_dims.x&& p.y < grid_dims.y&& p.z < grid_dims.z;
}

inline __device__ __host__ bool is_in_bounds(const int2 p, const float2 grid_dims) {
    return p.x >= 0 && p.y >= 0 && p.x < grid_dims.x&& p.y < grid_dims.y;
}

inline __device__ __host__ int3 location_to_grid_cell(const float3 coord, const float3 lower_bounds, const float radius) {
    return to_int3((coord - lower_bounds) / radius);
}

inline __device__ __host__ int2 location_to_grid_cell(const float2 coord, const float2 lower_bounds, const float radius) {
    return to_int2((coord - lower_bounds) / radius);
}

#if DATA_DIM == 3
inline __device__ __host__ int3 index_to_offset(const int i) {
    return make_int3(-1 + (i % 3), -1 + ((i / 3) % 3), -1 + ((i / 9) % 3));
}
#elif DATA_DIM == 2
inline __device__ __host__ int2 index_to_offset(const int i) {
    return make_int2(-1 + (i % 3), -1 + ((i / 3) % 3));
}
#endif

inline __device__ __host__ uint32_t grid_cell_to_hash(const int3 grid_coord, const float3 grid_dims) {
    auto coord = clamp(grid_coord, 0.0f, grid_dims - 1);

    uint32_t hash = coord.x * grid_dims.y * grid_dims.z;
    hash += coord.y * grid_dims.z;
    hash += coord.z;

    return hash;
}

inline __device__ __host__ uint32_t grid_cell_to_hash(const int2 grid_coord, const float2 grid_dims) {
    auto coord = clamp(grid_coord, 0.0f, grid_dims - 1);

    uint32_t hash = coord.x * grid_dims.y;
    hash += coord.y;

    return hash;
}

inline float3 interpolate(float3 startValue, float3 endValue, float stepNumber, float lastStepNumber) {
    return (endValue - startValue) * stepNumber / lastStepNumber + startValue;
}

inline float3 normalize(const float3 v) {
    return v / length(v);
}

array_stats getStats(const array_h<float>& arr);

array_d<point> capMagnitude(const array_d<point>& a, const float max);

