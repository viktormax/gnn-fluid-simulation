#pragma once

#include <vector>
#include <tuple>

class MemoryManager {
   public:

    MemoryManager();

    void* restore(const size_t bytes);

    void cache(void* pointer, const size_t bytes);

    void runGarbitchCollector(int max_idle, const size_t max_bytes = 0);

    std::tuple<size_t, size_t> getStats();

   private:
    static std::vector<std::tuple<size_t, void*, int>> pointers;

    static void clear();
};

extern MemoryManager memory_manager;
