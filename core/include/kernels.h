#pragma once

#include "structs.h"
#include "array_s.h"
#include "matrix_s.h"

void sub_vector(const float* a, const float* b, const size_t b_size, float* out, const size_t size);

template <typename T>
void sub_vector(const T* a, const T b, T* out, const size_t size);

void add_vector(const float* a, const float* b, const size_t b_size, float* out, const size_t size);

void add_vector(const float* a, const float b, const size_t b_size, float* out, const size_t size);

void mul_vector(const float* a, const float* b, const size_t b_size, float* out, const size_t size);

void mul_vector(const float* a, const float b, float* out, const size_t size);

void div_vector(const float* a, const float* b, const size_t b_size, float* out, const size_t size);

void div_vector(const float* a, const float b, float* out, const size_t size);

void pow_vector(const float* a, const float p, float* out, const size_t size);

void sqrt_vector(const float* a, float* out, const size_t size);

void max_vector(const float *in, float *out, const float max, const size_t size);

template <typename T>
void set_vector(T* a, const T v, const size_t size);

template <typename T>
void reorder_matrix(const T* data, const int* indexes, const size_t cols, const size_t rows, const bool reverse, T* out);

void insert_matrix(matrix_s& dest, const matrix_s& src, size_t start_row, size_t start_col);

void matrix_copy(const matrix_s& src, matrix_s& dst, size_t start_row, size_t start_col);

void map_to_float3(const matrix_s& matrix, array_s<float3>& vector);

void map_from_float3(matrix_s& matrix, const array_s<float3>& vector);
