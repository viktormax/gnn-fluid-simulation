#pragma once

#include <vector_types.h>

#define THREADS_COUNT 512

#define KERNEL_PROPS(opsCount) (int)ceil(opsCount / (float)THREADS_COUNT), THREADS_COUNT

#define SQRT_0_75 0.86602540378
#define SQRT_3 1.73205080757

#define MAX_NEIGHBOURS 128
#define CONECTIVITY_RADIUS 0.1
#define MAX_GRID_DIMS 128
#define DATA_DIM 3
#define MAX_SPEED 5
#define GRAVITY_Y 0
#define REST_DISTANCE_RATIO 0.55
#define COHESION 0.1
#define SURFACE_TENSION 20
#define RELAXATION_FACTOR 1
#define VISCOSITY 60
#define DAMP 1

#ifdef DEBUG
#include "stdio.h"
#define CHECK_BOUNDS(m, i) \
    if (i >= m) printf("Out of bounds (%d of %d)\n", (int)i, (int)m)
#define CHECK_BOUNDS2D(m_r, m_c, r, c) \
    if (r >= m_r || c >= m_c) printf("Out of bounds (%d, %d of %d, %d)\n", (int)r, (int)c, (int)m_r, (int)m_c)
#else
#define CHECK_BOUNDS(m, i)
#define CHECK_BOUNDS2D(m_r, m_c, r, c)
#endif

#if DATA_DIM == 3
using point = float3;
inline float3 make_point(float x, float y, float z) {
    return {x, y, z};
}
#elif DATA_DIM == 2
using point = float2;
inline float2 make_point(float x, float y, float z) {
    return {x, y};
}
#endif