#pragma once

#include <fstream>
#include <optional>

#include "constants.h"
#include "array_s.h"

template <typename T>
class array_h;

template <typename T>
struct array_d;

template <typename T>
std::ostream& operator<<(std::ostream& ss, const array_d<T>& arr) {
    return ss << array_h(arr);
}

template <typename T>
class array_d {
   public:
    const size_t& bytes;
    const size_t& count;

    array_d();

    ~array_d();

    array_d(const std::initializer_list<T>& data);

    array_d(const size_t count);

    array_d(const array_d<T>& that);

    array_d(const array_h<T>& that);

    array_d(array_d<T>&& that);

    array_d& operator=(const array_d<T>& that);

    array_d& operator=(const array_h<T>& that);

    array_d& operator=(array_d<T>&& that);

    bool operator==(const array_d<T>& that) const;

    array_d& init(const T val);

    array_d& free();

    array_s<T> str();

    const array_s<T> str() const;

    array_d& resize(const size_t count, std::optional<T> fill_empty = std::nullopt);

    void print(const int max_count = 100) const;

    array_d& insert(const array_d<T>& that, const size_t start_index = 0);

    array_d copy(const size_t start_index = 0, size_t count = 0) const;

    array_d& append(const array_d<T>& that);

    array_d& reorder(const array_d<int>& indexes, const bool reverse = false);

    array_d& fromStream(std::ifstream& rf);

    const array_d& toStream(std::ofstream& wf) const;

    inline T* operator()() {
        return _ptr;
    }

    inline const T* operator()() const {
        return _ptr;
    }

    friend std::ostream& operator<<<>(std::ostream& ss, const array_d<T>& arr);

   private:
    T* _ptr;        // Pointer to data.
    size_t _count;  // Number of items.
    size_t _bytes;  // Size of array in bytes.
    size_t _allocated_bytes;
};
