#pragma once

#include <cuda_runtime.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

inline __device__ __host__ float3 operator-(const float3 &a, const float3 &b) {
    return make_float3(a.x - b.x, a.y - b.y, a.z - b.z);
}

inline __device__ __host__ float2 operator-(const float2& a, const float2& b) {
    return make_float2(a.x - b.x, a.y - b.y);
}

inline __device__ __host__ float3 operator-(const float3 &a, const float &b) {
    return make_float3(a.x - b, a.y - b, a.z - b);
}

inline __device__ __host__ float2 operator-(const float2& a, const float& b) {
    return make_float2(a.x - b, a.y - b);
}

inline __device__ __host__ float3 operator+(const float3& a, const float& b) {
    return make_float3(a.x + b, a.y + b, a.z + b);
}

inline __device__ __host__ float2 operator+(const float2& a, const float& b) {
    return make_float2(a.x + b, a.y + b);
}

inline __device__ __host__ float3 operator-(const float &a, const float3 &b) {
    return make_float3(a - b.x, a - b.y, a - b.z);
}

inline __device__ __host__ float2 operator-(const float &a, const float2 &b) {
    return make_float2(a - b.x, a - b.y);
}

inline __device__ __host__ int3 operator-(const int3 &a, const int3 &b) {
    return make_int3(a.x - b.x, a.y - b.y, a.z - b.z);
}

inline __device__ __host__ int2 operator-(const int2 &a, const int2 &b) {
    return make_int2(a.x - b.x, a.y - b.y);
}

inline __device__ __host__ float3 operator+(const float3 &a, const float3 &b) {
    return make_float3(a.x + b.x, a.y + b.y, a.z + b.z);
}

inline __device__ __host__ float2 operator+(const float2 &a, const float2 &b) {
    return make_float2(a.x + b.x, a.y + b.y);
}

inline __device__ __host__ int3 operator+(const int3 &a, const int3 &b) {
    return make_int3(a.x + b.x, a.y + b.y, a.z + b.z);
}

inline __device__ __host__ int2 operator+(const int2& a, const int2& b) {
    return make_int2(a.x + b.x, a.y + b.y);
}

inline __device__ __host__ float3 operator/(const float3 &a, const float3 &b) {
    return make_float3(a.x / b.x, a.y / b.y, a.z / b.z);
}

inline __device__ __host__ float2 operator/(const float2 &a, const float2 &b) {
    return make_float2(a.x / b.x, a.y / b.y);
}

inline __device__ __host__ float3 operator/(const float3 &a, const float &b) {
    return make_float3(a.x / b, a.y / b, a.z / b);
}

inline __device__ __host__ float2 operator/(const float2& a, const float& b) {
    return make_float2(a.x / b, a.y / b);
}

inline __device__ __host__ int3 operator/(const int3 &a, const int &b) {
    return make_int3(a.x / b, a.y / b, a.z / b);
}

inline __device__ __host__ int2 operator/(const int2 &a, const int &b) {
    return make_int2(a.x / b, a.y / b);
}

inline __device__ __host__ __host__ float3 operator*(const float3 &a, const float &b) {
    return make_float3(a.x * b, a.y * b, a.z * b);
}

inline __device__ __host__ __host__ float2 operator*(const float2 &a, const float &b) {
    return make_float2(a.x * b, a.y * b);
}

inline __device__ __host__ float3 operator*(const float &b, const float3 &a) {
    return make_float3(a.x * b, a.y * b, a.z * b);
}

inline __device__ __host__ float2 operator*(const float &b, const float2 &a) {
    return make_float2(a.x * b, a.y * b);
}

inline __device__ __host__ float3 operator*(const float3 &a, const int3 &b) {
    return make_float3(a.x * b.x, a.y * b.y, a.z * b.z);
}

inline __device__ __host__ float3 operator*(const float3 &a, const float3 &b) {
    return make_float3(a.x * b.x, a.y * b.y, a.z * b.z);
}

inline __device__ __host__ float clampMin(const float v, const float min) {
    return fmaxf(v, min);
}

inline __device__ __host__ float3 clampMin(const float3 v, const float min) {
    return make_float3(clampMin(v.x, min), clampMin(v.y, min), clampMin(v.z, min));
}

inline __device__ __host__ float clamp(const float v, const float min, const float max) {
    return fmaxf(fminf(v, max), min);
}

inline __device__ __host__ float3 clamp(const float3 v, const float min, const float max) {
    return make_float3(clamp(v.x, min, max), clamp(v.y, min, max), clamp(v.z, min, max));
}

inline __device__ __host__ float2 clamp(const float2 v, const float min, const float max) {
    return make_float2(clamp(v.x, min, max), clamp(v.y, min, max));
}

inline __device__ __host__ int3 clamp(const int3 v, const float min, const float3 max) {
    return make_int3(clamp(v.x, min, max.x), clamp(v.y, min, max.y), clamp(v.z, min, max.z));
}

inline __device__ __host__ int2 clamp(const int2 v, const float min, const float2 max) {
    return make_int2(clamp(v.x, min, max.x), clamp(v.y, min, max.y));
}

inline __device__ __host__ float max(const float3 v) {
    return v.x > v.y ? (v.x > v.z ? v.x : v.z) : (v.y > v.z ? v.y : v.z);
}

inline __device__ __host__ float3 abs(const float3 v) {
    return make_float3(abs(v.x), abs(v.y), abs(v.z));
}

inline __device__ __host__ float3 ceil(const float3 v) {
    return make_float3(ceilf(v.x), ceilf(v.y), ceilf(v.z));
}

inline __device__ __host__ float2 ceil(const float2 v) {
    return make_float2(ceilf(v.x), ceilf(v.y));
}

inline __device__ __host__ float3 pow(const float3 a, const float p) {
    return make_float3(pow(a.x, p), pow(a.y, p), pow(a.z, p));
}

inline __device__ __host__ float2 pow(const float2 a, const float p) {
    return make_float2(pow(a.x, p), pow(a.y, p));
}
