cmake_minimum_required(VERSION 3.18 FATAL_ERROR)
set(CMAKE_TOOLCHAIN_FILE ${CMAKE_CURRENT_SOURCE_DIR}/vcpkg/scripts/buildsystems/vcpkg.cmake)

project(main LANGUAGES CXX CUDA)

include(CheckLanguage)
check_language(CUDA)
enable_language(CUDA)

# BUILD OPTIONS
set(MAIN_SOURCES sph_runner.cpp validate_model.cpp iris_demo.cpp train_model.cpp)
set(MODULES visualization core sph network)
set(CUDA_ARCHITECTURES 86)
set(COMPILE_FEATURES cxx_std_17)
# END BUILD OPTIONS

set(CMAKE_CUDA_FLAGS_DEBUG "${CMAKE_CUDA_FLAGS_DEBUG} -g -G -src-in-ptx -DDEBUG")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DDEBUG")

foreach(module ${MODULES})
    add_subdirectory(${module})
    get_property(targets DIRECTORY ${module} PROPERTY BUILDSYSTEM_TARGETS)

    foreach(target IN LISTS targets)
        target_compile_features(${target} PRIVATE ${COMPILE_FEATURES})
        set_property(TARGET ${target} PROPERTY CUDA_ARCHITECTURES ${CUDA_ARCHITECTURES})
    endforeach()
endforeach()

foreach(MAIN_SOURCE ${MAIN_SOURCES})
    get_filename_component(MAIN_NAME ${MAIN_SOURCE} NAME_WE)
    add_executable(${MAIN_NAME} ${MAIN_SOURCE})

    target_link_libraries(${MAIN_NAME} PRIVATE visualization core sph network)

    add_custom_command(TARGET ${MAIN_NAME} PRE_BUILD COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/resources ${CMAKE_BINARY_DIR}/Debug/resources)
    add_custom_command(TARGET ${MAIN_NAME} PRE_BUILD COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/resources ${CMAKE_BINARY_DIR}/Release/resources)
    add_custom_command(TARGET ${MAIN_NAME} PRE_BUILD COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/resources ${CMAKE_BINARY_DIR}/resources)

    set_property(TARGET ${MAIN_NAME} PROPERTY CUDA_ARCHITECTURES ${CUDA_ARCHITECTURES})
    target_compile_features(${MAIN_NAME} PRIVATE ${COMPILE_FEATURES})
endforeach()

set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT sph_runner)
