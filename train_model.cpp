#include <MemoryManager.hpp>

#include "core.h"
#include "network.h"
#include "sph.h"
#include "visualization.h"
#include "Dataset.hpp"
#include <chrono>
#include <filesystem>

#include <Windows.h>
#include <unordered_set>
bool isPressed(char key);

void saveMeta(const std::string& metaName, const array_stats& lossStats, int epoch);
json loadMeta(const std::string& metaName);
void saveVersionedModel(const std::string& modelDir, const std::string& logName, const NetworkModel& model, int currentVersion, const array_stats& lossStats);
void saveCurrentModel(const std::string& modelDir, const NetworkModel& model, const array_stats& lossStats);
void saveMinModel(const std::string& modelDir, const NetworkModel& model, const array_stats& lossStats);
array_stats log(std::chrono::milliseconds duration, std::string logName, const array_h<float>& rollputMeanLoss, float prevLoss);
void parseArgs(int argc, char** argv, int& messagePassingSteps, int& neuronCount, int& scale, ActivationType& activation);

#define LOG(logName, text) \
    do { \
        std::ofstream wf(logName, std::ios::app);\
        auto now = std::chrono::system_clock::now();\
        std::time_t now_t = std::chrono::system_clock::to_time_t(now);\
        std::tm local_tm = *std::localtime(&now_t);\
        wf << std::put_time(&local_tm, "%Y-%m-%d %H:%M:%S ");\
        wf << text << "\n";\
    } while (0)

int main(int argc, char** argv) {
    // Default Hyperparmeters
    int messagePassingSteps = 5;
    int neuronCount = 128;
    int scale = 10000;
    ActivationType activation = ActivationType::LReLU;

    parseArgs(argc, argv, messagePassingSteps, neuronCount, scale, activation);

    std::string modelDir = std::to_string(messagePassingSteps) + "mps-" + std::to_string(neuronCount) + "nc-" + std::to_string(scale) + "sc-" + to_string(activation);

    std::filesystem::create_directories(modelDir);

    std::string logName = modelDir + "/training.log";

    TrainingSession ts;
    ts.load(modelDir);

    LOG(logName, "Set learging rate to " << ts.learningRate);

    NetworkModel model;
    if (!model.load(modelDir + "/model.bin")) {
        model.init(3, messagePassingSteps, neuronCount, activation);
        saveCurrentModel(modelDir, model, array_stats());
        LOG(logName, "Creating model in " << modelDir << "/model.bin");
    }

    Dataset data;
    int steps = 200;
    int iters = 20;
    printf("Generating trainig data...\n");
    data.init(iters, steps, scale);
    array_h<float> rollputMeanLoss(iters * steps);

    while (true) {        
        auto start = std::chrono::high_resolution_clock::now();
        for (int i = 0; i < data.indices.size(); i++) {
            auto predicted = model.train(data.getGraph(i), data.getTarget(i), ts.learningRate);

            rollputMeanLoss[i] = model.cost();

            memory_manager.runGarbitchCollector(1);

            printf("step %d, epoch %d, cost: %f\n", i, (int)model.epoch(), model.cost());

            if (isPressed('Q')) {
                printf("\nExitind...\n");
                exit(EXIT_SUCCESS);
            }

            if (isPressed('S')) {
                printf("\nSaving model...\n");
                saveCurrentModel(modelDir, model, array_stats());
            }

            if (isPressed('P')) {
                printf("\nGradients: ");
                model.getGrads()->print(5);
                printf("Predicred: ");
                predicted.print(5);
                printf("Target: ");
                data.getTarget(i).print(5);
            }            
        }
        auto end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

        auto lossStats = log(duration, logName, rollputMeanLoss, ts.prevMeanLoss);

        saveCurrentModel(modelDir, model, lossStats);

        if (ts.versionIteration == ts.saveIteration) {
            saveVersionedModel(modelDir, logName, model, ts.currentVersion, lossStats);

            printf("Regenerate training data...\n");
            LOG(logName, "Regenerate training data.");
            data.init(iters, steps, scale);

            if (ts.currentVersion == 4) {
                ts.learningRate = 1e-5;
                LOG(logName, "Set learging rate to " << ts.learningRate);
            } else if (ts.currentVersion == 9) {
                ts.learningRate = 1e-6;
                LOG(logName, "Set learging rate to " << ts.learningRate);
            } else if (ts.currentVersion == 14) {
                ts.learningRate = 1e-7;
                LOG(logName, "Set learging rate to " << ts.learningRate);
            } else if (ts.currentVersion == 19) {
                LOG(logName, "Training is finished.");
                return 0;
            }
          
            ts.currentVersion++;
            ts.versionIteration = 0;
        }

        if (lossStats.mean < ts.minMeanLoss) {
            saveMinModel(modelDir, model, lossStats);
            ts.minMeanLoss = lossStats.mean;
        }

        ts.prevMeanLoss = lossStats.mean;
        ts.versionIteration++;

        ts.save(modelDir);

        data.shuffle();
    }
}

bool isPressed(char key) {
    static std::unordered_set<char> keys;
    auto keyState = GetKeyState(key);
    bool isToggled = keyState & 1;
    bool isDown = keyState & 0x8000;

    if (isDown) {
        if (keys.count(key) == 0) {
            keys.insert(key);
        }
    }
    else {
        if (keys.count(key) != 0) {
            keys.erase(key);
            return true;
        }
    }

    return false;
}

void saveMeta(const std::string& metaName, const array_stats& lossStats, int epoch) {
    std::ifstream metaFile(metaName);
    json meta = metaFile ? json::parse(metaFile) : json();
    meta["epoch"] = epoch;
    meta["rolloutStats"]["count"] = lossStats.count;
    meta["rolloutStats"]["mean"] = lossStats.mean;
    meta["rolloutStats"]["min"] = lossStats.min;
    meta["rolloutStats"]["max"] = lossStats.max;
    meta["rolloutStats"]["stddev"] = lossStats.stddev;
    std::ofstream ff(metaName);
    ff << std::setw(4) << meta << std::endl;
}

json loadMeta(const std::string& metaName) {
    std::ifstream metaFile(metaName);
    if (metaFile) {
        return json::parse(metaFile);
    }
    else {
        json meta;
        meta["epoch"] = 0;
        meta["rolloutStats"]["count"] = 0;
        meta["rolloutStats"]["mean"] = 0;
        meta["rolloutStats"]["min"] = 0;
        meta["rolloutStats"]["max"] = 0;
        meta["rolloutStats"]["stddev"] = 0;
        return meta;
    }
}

struct training_session {
    float learningRate;
    int currentVersion;
    int versionIteration;
    int saveIteration;
    float minMeanLoss;
    float prevMeanLoss;

    void load(const std::string& modelDir) {
        std::ifstream file(modelDir + "/trainingSession.json");
        if (file) {
            json j = json::parse(file);
            j["learningRate"].get_to(learningRate);
            j["currentVersion"].get_to(currentVersion);
            j["versionIteration"].get_to(versionIteration);
            j["saveIteration"].get_to(saveIteration);
            j["minMeanLoss"].get_to(minMeanLoss);
            j["prevMeanLoss"].get_to(prevMeanLoss);
        }
        else {
            learningRate = 1e-4;
            currentVersion = 0;
            versionIteration = 0;
            saveIteration = 25;
            minMeanLoss = FLT_MAX;
            prevMeanLoss = 0;
            save(modelDir);
        }
    }

    void save(const std::string& modelDir) {
        json j;
        j["learningRate"] = learningRate;
        j["currentVersion"] = currentVersion;
        j["versionIteration"] = versionIteration;
        j["saveIteration"] = saveIteration;
        j["minMeanLoss"] = minMeanLoss;
        j["prevMeanLoss"] = prevMeanLoss;
        std::ofstream ff(modelDir + "/trainingSession.json");
        ff << std::setw(4) << j << std::endl;
    }
};

void saveVersionedModel(const std::string& modelDir, const std::string& logName, const NetworkModel& model, int currentVersion, const array_stats& lossStats) {
    printf("\nSaving versioned model...\n");
    LOG(logName, "Saving model version " << currentVersion);
    model.save(modelDir + "/v" + std::to_string(currentVersion) + "-model.bin");
    saveMeta(modelDir + "/v" + std::to_string(currentVersion) + "-meta.json", lossStats, model.epoch());
}

void saveCurrentModel(const std::string& modelDir, const NetworkModel& model, const array_stats& lossStats) {
    printf("\nSaving model...\n");
    model.save(modelDir + "/model.bin");
    saveMeta(modelDir + "/meta.json", lossStats, model.epoch());
}

void saveMinModel(const std::string& modelDir, const NetworkModel& model, const array_stats& lossStats) {
    printf("\nSaving min model...\n");
    model.save(modelDir + "/min-model.bin");
    saveMeta(modelDir + "/min-meta.json", lossStats, model.epoch());
}

array_stats log(std::chrono::milliseconds duration, std::string logName, const array_h<float>& rollputMeanLoss, float prevLoss) {
    std::ofstream wf(logName, std::ios::app);
    double seconds = duration.count() / 1000.0;
    auto lossStats = getStats(rollputMeanLoss);

    float lossPerSecond = (lossStats.mean - prevLoss) / seconds;

    auto now = std::chrono::system_clock::now();
    std::time_t now_t = std::chrono::system_clock::to_time_t(now);
    std::tm local_tm = *std::localtime(&now_t);
    wf << std::put_time(&local_tm, "%Y-%m-%d %H:%M:%S");

    wf << " loss/second: " << lossPerSecond;
    wf << ", time: " << seconds << "s, " << "rollout loss mean: " << lossStats.mean;
    wf << ", stddev: " << lossStats.stddev << ", min: " << lossStats.min << ", max: " << lossStats.max << "\n";

    return lossStats;
}

void parseArgs(int argc, char** argv, int& messagePassingSteps, int& neuronCount, int& scale, ActivationType& activation) {
    for (int i = 1; i < argc; i++) {
        std::string arg = argv[i];
        if (arg == "-mps") {
            messagePassingSteps = std::stoi(argv[++i]);
        } else if (arg == "-nc") {
            neuronCount = std::stoi(argv[++i]);
        } else if (arg == "-sc") {
            scale = std::stoi(argv[++i]);
        } else if (arg == "-act") {
            activation = from_string(argv[++i]);
        }
    }
}
