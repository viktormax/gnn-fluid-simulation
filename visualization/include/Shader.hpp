#pragma once

#include <GL/glew.h>

#include <map>
#include <string>

class Shader {
   public:
    void clear();

    void useShader() const;

    inline GLint getUniformId(const std::string& name) const {
        return _uniforms.count(name) > 0 ? _uniforms.at(name) : -1;
    }

    static Shader getPointShader() {
        return getShader("point", "shaders/point.vr.glsl", "shaders/point.fr.glsl");
    }

   private:
    GLuint _shaderID = 0;
    std::map<std::string, GLuint> _uniforms;
    static std::map<std::string, Shader> _shaders;

    void compileShader(const std::string& vertexCode, const std::string& fragmentCode);
    void addShader(GLuint program, const std::string& shaderCode, GLenum shaderType);

    void createFromString(const std::string& vertexCode, const std::string& fragmentCode);
    void createFromFile(const std::string& vertexPath, const std::string& fragmentPath);

    std::string readFile(const std::string& filePath) const;

    static Shader getShader(const std::string& name, const std::string& vertexCode, const std::string& fragmentCode);
};