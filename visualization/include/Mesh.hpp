#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <vector>

#include "Vertex.hpp"

class Mesh {
   public:
    void clear();

    void create(const std::vector<Vertex>& vertices, const std::vector<GLuint>& indices);

    void render() const;

   protected:
    GLuint _VAO = 0;
    GLuint _VBO = 0;
    GLuint _IBO = 0;
    std::vector<GLuint> _indices;
};