#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "core.h"
#include <map>

struct state_s {
    bool isResetSimulationRequested = false;
    bool isStartRolloutRequested = false;
    bool runSimulation = false;
    bool oneStepMode = false;
    bool renderDebugWindow = true;
    bool showBoundaryPoints = false;
    bool disableKeyControl = false;
    bool rolloutRecordInProgress = false;
    int playRoloutStep = -1;
};

template<int C>
struct sliding_array {
    const int count = C;
    float data[C] = { 0 };
    int currIdx = 0;

    inline void push(float v) {
        data[currIdx++] = v;
        if (currIdx == count) {
            currIdx = 0;
        }
    }

    inline float get(int index) {
        if (currIdx + index >= count) {
            return get(index - count);
        }
        return data[currIdx + index];
    }

    static float iterator(void* data, int idx) {
        return ((sliding_array<C>*)data)->get(idx);
    }
};

class DebugWindow {
public:
    ~DebugWindow();

    void init(GLFWwindow* window);
    void render(physics_s& physics, state_s& state, const array_d<float>& neighboursCount, size_t step);
    void keyControl(bool keys[1024], state_s& state) const;

    inline std::tuple<float3, float3> getColors() const {
        return { _colorSlow, _colorFast };
    }

    void startRollout(const array_h<point>& locs, const array_h<point>& statics, physics_s physics, const array_h<plane_s>& boundaries);
    void addRolloutRecord(const array_h<point>& locs);
    array_h<point> getRolloutStep(state_s& state);

   private:
    bool _initialized = false;
    bool _loadOnStart = false;
    bool _firstRender = true;
    Rollout _rollout;
    char _rolloutFileName[64] = "rollout.bin";
    std::map<std::string, rollout_meta> _rollouts;

    int _cudaVersion = 0;
    int _cudaDeciceNumber = 0;
    sliding_array<32> _mean;
    sliding_array<32> _max;

    float3 _colorSlow = make_float3(0.0859375, 0.49609375, 0.98437500);
    float3 _colorFast = make_float3(0.98828125, 0.1953125, 0.34765625);

    void save(const physics_s& physics, const state_s& state) const;
    void load(physics_s& physics, state_s& state);
    void loadRollouts();
};
