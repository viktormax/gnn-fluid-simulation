#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include "Shader.hpp"

class Light {
   public:
    static const std::string uAmbientLight;
    static const std::string uDiffuseLight;
    static const std::string uSpecularExponent;

    void init(glm::vec3 color, float ambientIntensity, glm::vec3 direction, float deffuseIntensity, float specularExponent);

    void useLight(const Shader& shader) const;

   private:
    glm::vec3 _color;
    float _ambientIntensity;

    glm::vec3 _direction;
    float _diffuseIntensity;
    float _specularExponent;
};
