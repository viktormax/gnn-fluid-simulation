#pragma once

#include "Points.hpp"
#include "core.h"

class Plane {
   public:
    void create(float3 p1, float3 p2, float3 p3, float3 p4) {
        _points.push(p1);
        _points.push(p2);
        _points.push(p3);
        _points.push(p4);

        _vertices.create(4);
        _vertices.updatePositions(_points);
        _vertices.updateColors(color::rgb(55, 168, 82));
        _vertices.setPointSize(50);
        computeNormalVector();
    }

    void computeNormalVector() {
        auto p12 = _points[1] - _points[0];
        auto p13 = _points[2] - _points[0];

        _normal = make_float3(
            p12.y * p13.z - p12.z * p13.y,
            p12.z * p13.x - p12.x * p13.z,
            p12.x * p13.y - p12.y * p13.x);

        _d = _normal.x * _points[0].x + _normal.y * _points[0].y + _normal.z * _points[0].z;

        // Normalize
        auto maxCoponent = max(abs(_normal));
        _normal = _normal / maxCoponent;
        _d = _d / maxCoponent;
    }

    void renderVertices(const Camera& camera, const Light& light) const {
        _vertices.render(camera, light);
    }

    plane_s toStruct() {
        plane_s plane;
        plane.d = _d;
        plane.normal = _normal;
        plane.p[0] = _points[0];
        plane.p[1] = _points[1];
        plane.p[2] = _points[2];
        plane.p[3] = _points[3];
        return plane;
    }

    static plane_s createStruct(float3 p1, float3 p2, float3 p3, float3 p4) {
        auto p12 = p2 - p1;
        auto p13 = p3 - p1;

        auto _normal = make_float3(
            p12.y * p13.z - p12.z * p13.y,
            p12.z * p13.x - p12.x * p13.z,
            p12.x * p13.y - p12.y * p13.x);

        auto _d = _normal.x * p1.x + _normal.y * p1.y + _normal.z * p1.z;

        // Normalize
        auto maxCoponent = max(abs(_normal));
        _normal = _normal / maxCoponent;
        _d = _d / maxCoponent;

        plane_s plane;
        plane.d = _d;
        plane.normal = _normal;
        plane.p[0] = p1;
        plane.p[1] = p2;
        plane.p[2] = p3;
        plane.p[3] = p4;
        return plane;
    }

   private:
    Points _vertices;
    array_h<float3> _points;
    float3 _normal;
    float _d;
};