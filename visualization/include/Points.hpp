#pragma once

#include "Camera.hpp"
#include "PointMesh.hpp"
#include "Shader.hpp"
#include "Light.hpp"
#include "core.h"

class Points {
   public:
    static const std::string uPointSize;

    void create(const size_t count);

    void render(const Camera& camera, const Light& light) const;

    void updatePositions(const array_h<float3>& locs);

    void updateColors(const array_h<float4>& colors);

    void updateColors(const float4 color);

    inline void setPointSize(float size) {
        _pointSize = size;
    }

   private:
    PointMesh _mesh;
    Shader _shader;
    float _pointSize = 20;
};