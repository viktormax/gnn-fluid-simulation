#pragma once

#include "core.h"

struct Vertex {
    float3 position;
    float2 texture;
    float3 normal;

    static Vertex create(float x, float y, float z) {
        return { x, y, z, 0, 0, 0, 0, 0 };
    }

    Vertex& setTexture(float u, float v) {
        texture.x = u;
        texture.y = v;
        return *this;
    }

    Vertex& setNormal(float x, float y, float z) {
        normal.x = x;
        normal.y = y;
        normal.z = z;
        return *this;
    }
};

struct PointVertex {
    float3 position;
    float4 color;

    static PointVertex create(float x, float y, float z) {
        return { x, y, z, 1, 1, 1, 1 };
    }

    PointVertex& setPosition(float x, float y, float z) {
        position.x = x;
        position.y = y;
        position.z = z;
        return *this;
    }

    PointVertex& setCoor(float r, float g, float b, float a) {
        color.x = r;
        color.y = g;
        color.z = b;
        color.w = a;
        return *this;
    }
};

struct color {
    static float4 rgb(float r, float g, float b, float a = 1) {
        return { r / 255, g / 255, b / 255, a };
    }
};