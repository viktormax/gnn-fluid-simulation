#pragma once

#include "Camera.hpp"
#include "Light.hpp"
#include "Window.hpp"
#include "DebugWindow.hpp"

class Scene {
   public:
    const Window& window;
    const Light& light;
    const Camera& camera;
    const DebugWindow& debug;
    const double& dt;

    Scene();

    void init();

    void createNewFrame();

    void swapBuffers();

    void renderDebugWindow(physics_s& physics, const array_d<float>& neighboursCount, size_t step);

    inline bool isResetSimulationRequested() {
        if (_state.isResetSimulationRequested) {
            _state.isResetSimulationRequested = false;
            return true;
        }
        return false;
    }

    inline bool runSimulation() {
        if (_state.runSimulation && _state.oneStepMode) {
            _state.runSimulation = false;
            return true;
        }
        return _state.runSimulation;
    }

    inline bool playRolloutInProgress() {
        return _state.playRoloutStep != -1;
    }

    inline void stopSimulation() {
        _state.runSimulation = false;
    }

    inline int getCurrentRoloutStep() {
        return _state.playRoloutStep;
    }

    inline array_h<point> getRolloutStep() {
        return _debug.getRolloutStep(_state);
    }

    inline void addRolloutRecord(const array_h<point>& locs) {
        _debug.addRolloutRecord(locs);
    }

    inline void startRollout(const array_h<point>& locs, const array_h<point>& statics, physics_s physics, const array_h<plane_s>& boundaries) {
        _debug.startRollout(locs, statics, physics, boundaries);
    }

    inline bool showBoundaryPoints() {
        return _state.showBoundaryPoints;
    }

    inline bool isStartRolloutRequested() {
        if (_state.isStartRolloutRequested) {
            _state.isStartRolloutRequested = false;
            return true;
        }
        return false;
    }

    inline bool rolloutRecordInProgress() {
        return _state.rolloutRecordInProgress;
    }

   private:
    Window _window;
    Light _light;
    Camera _camera;
    DebugWindow _debug;
    state_s _state;

    double _lastTime = 0;
    double _deltaTime = 0;
};