#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include "Shader.hpp"

class Camera {
   public:
    static const std::string uView;
    static const std::string uProjection;
    static const std::string uCameraPos;

    void init(const float* windowRatio, glm::vec3 position, glm::vec3 worldUp, float yaw, float pitch, float moveSpeed, float turnSpeed);

    void useCamera(const Shader& shader) const;

    void keyControl(const bool keys[1024], float deltaTime);
    void mouseControl(float xChange, float yChange);

    glm::mat4 getViewMatrix() const;
    glm::mat4 getProjectionMatrix() const;

   private:
    glm::vec3 _position;
    glm::vec3 _front;
    glm::vec3 _up;
    glm::vec3 _right;
    glm::vec3 _worldUp;

    const float* _windowRatio = nullptr;

    float _yaw;
    float _pitch;
    float _moveSpeed;
    float _turnSpeed;

    void update();
};
