#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

class Window {
   public:
    bool* const keys;

    Window();

    ~Window();

    void init();

    void clear();

    inline bool shouldClose() const {
        return glfwWindowShouldClose(_mainWindow);
    }

    inline void swapBuffers() const {
        glfwSwapBuffers(_mainWindow);
    }

    inline float getXChange() {
        float tmp = _xChange;
        _xChange = 0;
        return _cursorDisabled ? tmp : 0;
    }

    inline float getYChange() {
        float tmp = _yChange;
        _yChange = 0;
        return _cursorDisabled ? tmp : 0;
    }

    inline const float* getWindowRatio() const {
        return &_windowRatio;
    }

    inline GLFWwindow* getMainWindow() {
        return _mainWindow;
    }

    inline void disableKeyControl(bool disable) {
        _disableKeyControl = disable;
    }

   private:
    GLFWwindow* _mainWindow = nullptr;

    float _windowRatio = 1;

    bool _cursorDisabled = true;
    bool _fullScreen = false;
    bool _initialized = false;
    bool _firstMouseMove = true;
    bool _disableKeyControl = false;

    int _bufferHeight = 0;
    int _bufferWidth = 0;

    float _lastX = 0;
    float _lastY = 0;
    float _xChange = 0;
    float _yChange = 0;

    bool _keys[1024];

    void createCallbacks();

    static void handleKeys(GLFWwindow* window, int key, int code, int action, int mode);
    static void handleMouse(GLFWwindow* window, double posX, double posY);
    static void handleResize(GLFWwindow* window, int width, int height);
};
