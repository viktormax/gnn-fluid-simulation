#pragma once

#include "Mesh.hpp"

class PointMesh : public Mesh {
   public:

    void clear();

    void create(int size);

    void render() const;

    inline std::vector<PointVertex>& getVerticesReference() {
        return _vertices;
    }

   private:
    std::vector<PointVertex> _vertices;
};
