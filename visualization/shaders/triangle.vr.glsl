#version 410

layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 tex;
layout (location = 2) in vec3 norm;

out vec2 vTex;
out vec3 vNorm;

uniform mat4 uModel;
uniform mat4 uProjection;
uniform mat4 uView;

void main() {
   vTex = tex;
   vNorm = mat3(transpose(inverse(uModel))) * norm;
   gl_Position = uProjection * uView * uModel * vec4(pos, 1.0);
}