#version 410

in vec2 vTex;
in vec3 vNorm;

out vec4 color;

uniform sampler2D uTexture;
uniform vec4 uColor;
uniform vec4 uAmbientLight;
uniform vec4 uDiffuseLight;

void main() {
   vec4 ambient = uAmbientLight.w * vec4(uAmbientLight.xyz, 1);
   float diffuseFactor = max(dot(normalize(vNorm), normalize(uDiffuseLight.xyz)), 0);
   vec4 diffuse = vec4(uAmbientLight.xyz, 1) * diffuseFactor * uDiffuseLight.w;
   color = (ambient + diffuse) * uColor * texture(uTexture, vTex);
}