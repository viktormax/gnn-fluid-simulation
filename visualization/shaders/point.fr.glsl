#version 410

in vec4 vCol;

out vec4 color;

uniform vec4 uAmbientLight;
uniform vec4 uDiffuseLight;
uniform float uSpecularExponent;

const vec3 viewDirection = vec3 (0.0, 0.0, 1.0);

void main() {
    vec3 normal = vec3(gl_PointCoord * 2.0 - vec2(1.0, 1.0), 0);
    vec3 halfwayDirection = normalize(viewDirection + normalize(uDiffuseLight.xyz));

    float mag = dot(normal.xy, normal.xy);
    if (mag > 1.0) {
        discard;
    }

    normal.z = sqrt(1.0 - mag);

    float diff = max(0.0, dot(normalize(uDiffuseLight.xyz), normal));
    float spec = max(0.0, pow(dot(halfwayDirection, normal), uSpecularExponent));

    vec3 ambient = uAmbientLight.a * vCol.rgb * uAmbientLight.rgb;
    vec3 diffuse = uDiffuseLight.a * vCol.rgb * diff;
    vec3 specular = vCol.rgb * spec; 

    color = vec4(diffuse + specular + ambient, vCol.a);
}