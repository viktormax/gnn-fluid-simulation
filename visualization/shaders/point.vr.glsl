#version 410

layout (location = 0) in vec3 pos;
layout (location = 1) in vec4 col;

out vec4 vCol;

uniform mat4 uProjection;
uniform mat4 uView;
uniform vec3 uCameraPos;
uniform float uPointSize;

void main() {
   vec4 pos = uView * vec4(pos, 1.0);
   float dist = distance(pos.xyz, uCameraPos);

   gl_Position = uProjection * pos;
   gl_PointSize = dist > 0.0 ? uPointSize / dist : uPointSize * 1000.0;

   vCol = col;
}
