#pragma once

#include "Scene.hpp"

Scene::Scene() : window(_window), light(_light), camera(_camera), debug(_debug), dt(_deltaTime) {
}

void Scene::init() {
    _window.init();
    _light.init(glm::vec3(1, 1, 1), 0.2, glm::vec3(2, -1, -2), 1, 250);
    _camera.init(window.getWindowRatio(), glm::vec3(0, 1, 4), glm::vec3(0, 1, 0), -45, 25, 5, 0.1);
    _debug.init(_window.getMainWindow());
}

void Scene::createNewFrame() {
    _window.disableKeyControl(_state.disableKeyControl);
    glfwPollEvents();

    auto now = glfwGetTime();
    _deltaTime = now - _lastTime;
    _lastTime = now;

    _debug.keyControl(_window.keys, _state);
    _camera.keyControl(_window.keys, _deltaTime);
    _camera.mouseControl(_window.getXChange(), _window.getYChange());

    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Scene::swapBuffers() {
    _window.swapBuffers();
}

void Scene::renderDebugWindow(physics_s& physics, const array_d<float>& neighboursCount, size_t step) {
    if (_state.renderDebugWindow) {
        _debug.render(physics, _state, neighboursCount, step);
    }
}