#include "Window.hpp"

#include <stdio.h>
#include <stdlib.h>

Window::Window() : keys(_keys) {
}

Window::~Window() {
    clear();
}

void Window::clear() {
    if (!_initialized) {
        return;
    }

    glfwDestroyWindow(_mainWindow);
    glfwTerminate();
    _initialized = false;
}

void Window::init() {
    for (int i = 0; i < 1024; i++) {
        _keys[i] = false;
    }

    if (!glfwInit()) {
        printf("GLFW initialization failed!\n");
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    auto videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    _mainWindow = glfwCreateWindow(videoMode->width, videoMode->height, "SPH", NULL, NULL);

    if (!_mainWindow) {
        printf("GLFW window creation failed!\n");
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMaximizeWindow(_mainWindow);
    glfwGetFramebufferSize(_mainWindow, &_bufferWidth, &_bufferHeight);

    glfwMakeContextCurrent(_mainWindow);
    glfwSetInputMode(_mainWindow, GLFW_CURSOR, _cursorDisabled ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);

    createCallbacks();

    if (glewInit() != GLEW_OK) {
        printf("GLEW initialization failed!\n");
        clear();
        exit(EXIT_FAILURE);
    }

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
    glViewport(0, 0, _bufferWidth, _bufferHeight);

    glfwSetWindowUserPointer(_mainWindow, this);

    _windowRatio = (float)_bufferWidth / _bufferHeight;
    _initialized = true;
}

void Window::handleKeys(GLFWwindow* window, int key, int code, int action, int mode) {
    Window* thisWindow = static_cast<Window*>(glfwGetWindowUserPointer(window));

    if (thisWindow->_disableKeyControl) {
        return;
    }

    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
    if (key == GLFW_KEY_F && action == GLFW_PRESS) {
        auto monitor = glfwGetPrimaryMonitor();
        auto videoMode = glfwGetVideoMode(monitor);
        
        thisWindow->_fullScreen = !thisWindow->_fullScreen;
        if (thisWindow->_fullScreen) {
            glfwSetWindowMonitor(window, monitor, 0, 0, videoMode->width, videoMode->height, GLFW_DONT_CARE);
        } else {
            glfwSetWindowMonitor(window, NULL, 0, 0, videoMode->width, videoMode->height, GLFW_DONT_CARE);
            glfwMaximizeWindow(window);
        }
    }
    if (key == GLFW_KEY_P && action == GLFW_PRESS) {
        thisWindow->_cursorDisabled = !thisWindow->_cursorDisabled;
        glfwSetInputMode(window, GLFW_CURSOR, thisWindow->_cursorDisabled ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);
    }

    if (key >= 0 && key < 1024) {
        if (action == GLFW_PRESS) {
            thisWindow->keys[key] = true;
        } else if (action == GLFW_RELEASE) {
            thisWindow->keys[key] = false;
        }
    }
}

void Window::handleMouse(GLFWwindow* window, double posX, double posY) {
    Window* thisWindow = static_cast<Window*>(glfwGetWindowUserPointer(window));

    if (thisWindow->_firstMouseMove) {
        thisWindow->_lastX = posX;
        thisWindow->_lastY = posY;
        thisWindow->_firstMouseMove = false;
    }

    thisWindow->_xChange = posX - thisWindow->_lastX;
    thisWindow->_yChange = thisWindow->_lastY - posY;
    thisWindow->_lastX = posX;
    thisWindow->_lastY = posY;
}

void Window::handleResize(GLFWwindow* window, int width, int height) {
    Window* thisWindow = static_cast<Window*>(glfwGetWindowUserPointer(window));
    glfwGetFramebufferSize(window, &thisWindow->_bufferWidth, &thisWindow->_bufferHeight);
    thisWindow->_windowRatio = (float)thisWindow->_bufferWidth / thisWindow->_bufferHeight;
    glViewport(0, 0, width, height);
}

void Window::createCallbacks() {
    glfwSetKeyCallback(_mainWindow, handleKeys);
    glfwSetCursorPosCallback(_mainWindow, handleMouse);
    glfwSetFramebufferSizeCallback(_mainWindow, handleResize);
}
