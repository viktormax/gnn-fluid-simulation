#include "PointMesh.hpp"

void PointMesh::clear() {
    _vertices.clear();
    Mesh::clear();
}

void PointMesh::create(int count) {
    _vertices.resize(count);
    _indices.resize(count);
    for (int i = 0; i < count; i++) {
        _indices[i] = i;
        _vertices[i] = PointVertex::create(0, 0, 0);
    }

    glGenVertexArrays(1, &_VAO);
    glGenBuffers(1, &_VBO);
    glGenBuffers(1, &_IBO);

    glBindVertexArray(_VAO);
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _IBO);
        {
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * _indices.size(), _indices.data(), GL_DYNAMIC_DRAW);
        }

        glBindBuffer(GL_ARRAY_BUFFER, _VBO);
        {
            glBufferData(GL_ARRAY_BUFFER, sizeof(PointVertex) * _vertices.size(), _vertices.data(), GL_STATIC_DRAW);

            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(PointVertex), (void*)offsetof(PointVertex, position));
            glEnableVertexAttribArray(0);

            glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(PointVertex), (void*)offsetof(PointVertex, color));
            glEnableVertexAttribArray(1);
        }
    }
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void PointMesh::render() const {
    glBindVertexArray(_VAO);
    {
        glBindBuffer(GL_ARRAY_BUFFER, _VBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(PointVertex) * _vertices.size(), _vertices.data());

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _IBO);
        {
            glDrawElements(GL_POINTS, _indices.size(), GL_UNSIGNED_INT, 0);
        }
    }
    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
