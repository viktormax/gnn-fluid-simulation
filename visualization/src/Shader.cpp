#include "Shader.hpp"

#include <stdio.h>

#include <fstream>
#include <iostream>

std::map<std::string, Shader> Shader::_shaders;

void Shader::clear() {
    if (_shaderID != 0) {
        glDeleteProgram(_shaderID);
        _shaderID = 0;
    }
}

void Shader::createFromString(const std::string& vertexCode, const std::string& fragmentCode) {
    compileShader(vertexCode, fragmentCode);
}

void Shader::createFromFile(const std::string& vertexPath, const std::string& fragmentPath) {
    auto vertexCode = readFile(vertexPath);
    auto fragmentCode = readFile(fragmentPath);
    compileShader(vertexCode, fragmentCode);
}

std::string Shader::readFile(const std::string& filePath) const {
    std::string content;
    std::ifstream fileStream(filePath, std::ios::in);

    if (!fileStream.is_open()) {
        printf("Faild to read file %s.\n", filePath.c_str());
        return "";
    }

    std::string line = "";
    while (!fileStream.eof()) {
        std::getline(fileStream, line);
        content.append(line + "\n");
    }

    fileStream.close();
    return content;
}

void Shader::compileShader(const std::string& vertexCode, const std::string& fragmentCode) {
    _shaderID = glCreateProgram();

    if (!_shaderID) {
        printf("Error creating shader program\n");
        throw "Error creating shader program";
    }

    addShader(_shaderID, vertexCode, GL_VERTEX_SHADER);
    addShader(_shaderID, fragmentCode, GL_FRAGMENT_SHADER);

    int result = 0;
    char eLog[1024] = {0};

    glLinkProgram(_shaderID);
    glGetProgramiv(_shaderID, GL_LINK_STATUS, &result);
    if (!result) {
        glGetProgramInfoLog(_shaderID, sizeof(eLog), NULL, eLog);
        printf("Error linking program:'%s'\n", eLog);
    }

    glValidateProgram(_shaderID);
    glGetProgramiv(_shaderID, GL_VALIDATE_STATUS, &result);
    if (!result) {
        glGetProgramInfoLog(_shaderID, sizeof(eLog), NULL, eLog);
        printf("Error validating program:'%s'\n", eLog);
    }

    int count;
    glGetProgramiv(_shaderID, GL_ACTIVE_UNIFORMS, &count);
    for (int i = 0; i < count; i++) {
        int size, length;
        GLenum type;
        const int bufSize = 64;
        char name[bufSize];
        glGetActiveUniform(_shaderID, (GLuint)i, bufSize, &length, &size, &type, name);
        _uniforms.insert({ name, glGetUniformLocation(_shaderID, name) });
    }
}

void Shader::useShader() const {
    glUseProgram(_shaderID);
}

void Shader::addShader(GLuint program, const std::string& shaderCode, GLenum shaderType) {
    GLuint shader = glCreateShader(shaderType);

    const char* code[1];
    code[0] = shaderCode.c_str();

    int codeLength[1];
    codeLength[0] = shaderCode.length();

    glShaderSource(shader, 1, code, codeLength);
    glCompileShader(shader);

    int result = 0;
    char eLog[1024] = {0};

    glLinkProgram(shader);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
    if (!result) {
        glGetShaderInfoLog(shader, sizeof(eLog), NULL, eLog);
        printf("Error compiling the %d shader:'%s'\n", shaderType, eLog);
    }

    glAttachShader(program, shader);
}

Shader Shader::getShader(const std::string& name, const std::string& vertexCode, const std::string& fragmentCode) {
    if (_shaders.count(name) > 0) {
        return _shaders.at(name);
    }

    Shader shader;
    shader.createFromFile(vertexCode, fragmentCode);
    _shaders[name] = shader;
    return shader;
}