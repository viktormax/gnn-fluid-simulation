#include "Camera.hpp"

#include <glm/gtc/type_ptr.hpp>

const std::string Camera::uView = "uView";
const std::string Camera::uProjection = "uProjection";
const std::string Camera::uCameraPos = "uCameraPos";

void Camera::init(const float* windowRatio, glm::vec3 position, glm::vec3 worldUp, float yaw, float pitch, float moveSpeed, float turnSpeed) {
    _position = position;
    _worldUp = worldUp;
    _yaw = yaw;
    _pitch = pitch;
    _moveSpeed = moveSpeed;
    _turnSpeed = turnSpeed;
    _windowRatio = windowRatio;
    _front = glm::vec3(0, 0, -1);

    update();
}

void Camera::useCamera(const Shader& shader) const {
    if (shader.getUniformId(Camera::uView) != -1) {
        glUniformMatrix4fv(shader.getUniformId(Camera::uView), 1, GL_FALSE, glm::value_ptr(getViewMatrix()));
    }
    if (shader.getUniformId(Camera::uProjection) != -1) {
        glUniformMatrix4fv(shader.getUniformId(Camera::uProjection), 1, GL_FALSE, glm::value_ptr(getProjectionMatrix()));
    }
    if (shader.getUniformId(Camera::uCameraPos) != -1) {
        glUniform3d(shader.getUniformId(Camera::uCameraPos), _position.x, _position.y, _position.z);
    }
}

void Camera::keyControl(const bool keys[1024], float deltaTime) {
    if (keys[GLFW_KEY_W]) {
        _position += _front * _moveSpeed * deltaTime;
    }
    if (keys[GLFW_KEY_S]) {
        _position -= _front * _moveSpeed * deltaTime;
    }
    if (keys[GLFW_KEY_A]) {
        _position -= _right * _moveSpeed * deltaTime;
    }
    if (keys[GLFW_KEY_D]) {
        _position += _right * _moveSpeed * deltaTime;
    }
}

void Camera::mouseControl(float xChange, float yChange) {
    xChange *= _turnSpeed;
    yChange *= _turnSpeed;

    _yaw += xChange;
    _pitch += yChange;

    if (_pitch > 89) {
        _pitch = 89;
    } else if (_pitch < -89) {
        _pitch = -89;
    }

    update();
}

glm::mat4 Camera::getViewMatrix() const {
    return glm::lookAt(_position, _position + _front, _up);
}

glm::mat4 Camera::getProjectionMatrix() const {
    return glm::perspective(45.0f, _windowRatio == nullptr ? 1 : *_windowRatio, 0.1f, 100.0f);
}

void Camera::update() {
    _front.x = cos(glm::radians(_yaw)) * cos(glm::radians(_pitch));
    _front.y = sin(glm::radians(_pitch));
    _front.z = sin(glm::radians(_yaw)) * cos(glm::radians(_pitch));
    _front = glm::normalize(_front);

    _right = glm::normalize(glm::cross(_front, _worldUp));
    _up = glm::normalize(glm::cross(_right, _front));
}
