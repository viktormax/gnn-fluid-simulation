#include "Points.hpp"

const std::string Points::uPointSize = "uPointSize";

void Points::create(const size_t count) {
    _shader = Shader::getPointShader();
    _mesh.create(count);
}

void Points::render(const Camera& camera, const Light& light) const {
    _shader.useShader();
    camera.useCamera(_shader);
    light.useLight(_shader);

    if (_shader.getUniformId(Points::uPointSize) != -1) {
        glUniform1f(_shader.getUniformId(Points::uPointSize), _pointSize);
    }

    _mesh.render();
}

void Points::updatePositions(const array_h<float3>& locs) {
    auto& verteces = _mesh.getVerticesReference();
    if (locs.count != verteces.size()) {
        _mesh.clear();
        _mesh.create(locs.count);
    }

    for (size_t i = 0, l = verteces.size(); i < l; i++) {
        verteces[i].position = locs[i];
    }
}

void Points::updateColors(const array_h<float4>& colors) {
    auto& verteces = _mesh.getVerticesReference();
    for (size_t i = 0, l = verteces.size(); i < l; i++) {
        verteces[i].color = colors[i];
    }
}

void Points::updateColors(const float4 color) {
    auto& verteces = _mesh.getVerticesReference();
    for (size_t i = 0, l = verteces.size(); i < l; i++) {
        verteces[i].color = color;
    }
}
