#include "DebugWindow.hpp"

#include <imgui.h>

#include <filesystem>
#include <fstream>
#include <nlohmann/json.hpp>

#include "DebugWindow.hpp"
#include "MemoryManager.hpp"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"

using json = nlohmann::json;

void DebugWindow::init(GLFWwindow* window) {
    ImGui::CreateContext();
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 410");
    ImGui::StyleColorsDark();

    // Font settings
    // ImGuiIO& io = ImGui::GetIO();
    // ImFontConfig conf;
    // conf.SizePixels = 16;
    // io.Fonts->AddFontDefault(&conf);

    cudaRuntimeGetVersion(&_cudaVersion);
    cudaGetDeviceCount(&_cudaDeciceNumber);

    load(physics_s(), state_s());
    loadRollouts();
    _initialized = true;
}

void DebugWindow::render(physics_s& physics, state_s& state, const array_d<float>& neighboursCount, size_t step) {
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    ImGui::Begin("Debug Window");
    ImGui::SetWindowPos(ImVec2(5, 5), ImGuiCond_FirstUseEver);

    if (_firstRender) {
        _firstRender = false;
        if (_loadOnStart) {
            load(physics, state);
        }
    }

    if (_cudaVersion == 0 || _cudaDeciceNumber == 0) {
        if (_cudaDeciceNumber == 0) ImGui::Text("CUDA-capable device is not found.");
        if (_cudaVersion == 0) ImGui::Text("CUDA runtime is not installed.");
        ImGui::End();
        return;
    }

    if (ImGui::CollapsingHeader("Key Control")) {
        ImGui::BulletText("F - full screen mode");
        ImGui::BulletText("P - show/hide pointer");
        ImGui::BulletText("B - show/hide debug window");
        ImGui::BulletText("R - reset simulation");
        ImGui::BulletText("SPACE - run/stop simulation");
        ImGui::BulletText("ESCAPE - close program");
        ImGui::BulletText("A,W,S,D - navigation");
    }

    if (ImGui::CollapsingHeader("Color settings")) {
        ImGui::ColorEdit3("Fast Color", (float*)&_colorFast);
        ImGui::ColorEdit3("Slow Color", (float*)&_colorSlow);
    }

    if (ImGui::CollapsingHeader("Rollout manager", ImGuiTreeNodeFlags_DefaultOpen)) {
        for (const auto& [key, value] : _rollouts) {
            ImGui::BulletText(key.c_str());
            ImGui::SameLine();
            ImGui::Text(" steps: %d; particles: %d;", value.stepsCount, value.particlesCount);
            ImGui::SameLine();
            ImGui::PushID(key.c_str());
            if (ImGui::Button(state.playRoloutStep == -1 ? "Play" : "Stop")) {
                state.isResetSimulationRequested = true;
                if (state.playRoloutStep != -1) {
                    state.playRoloutStep = -1;
                } else {
                    state.playRoloutStep = 0;
                    state.runSimulation = false;
                    _rollout.load("rollouts/" + key);
                }
            }
            ImGui::PopID();
        }

        ImGui::BeginDisabled(state.rolloutRecordInProgress);
        ImGui::InputText("Rollout File Name", _rolloutFileName, 64);
        state.disableKeyControl = ImGui::IsItemActive();
        ImGui::EndDisabled();

        ImGui::BeginDisabled(_rolloutFileName == "" || state.playRoloutStep != -1);
        if (ImGui::Button(state.rolloutRecordInProgress ? "Finish Recording" : "Start Recording")) {
            state.rolloutRecordInProgress = !state.rolloutRecordInProgress;
            if (state.rolloutRecordInProgress) {
                state.isStartRolloutRequested = true;
            } else {
                _rollout.save("rollouts/" + std::string(_rolloutFileName));
                _rollout.free();
                loadRollouts();
            }
        }
        ImGui::EndDisabled();
        if (state.rolloutRecordInProgress) {
            ImGui::SameLine();
            ImGui::Text("Recorded steps: %d", _rollout.locs.size());
        }
    } else {
        state.disableKeyControl = false;
    }

    ImGui::BeginDisabled(state.playRoloutStep != -1);

    if (ImGui::CollapsingHeader("Fluid parmeters", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::InputFloat3("Gravity", (float*)&physics.gravity);
        ImGui::SliderFloat("Radius", &physics.radius, 0, 1, "%.3f", ImGuiSliderFlags_None);
        ImGui::SliderFloat("Cohasion", &physics.cohesion, 0, 1, "%.3f", ImGuiSliderFlags_None);
        ImGui::SliderFloat("Surface Tension", &physics.surfaceTension, 0, 100, "%.0f", ImGuiSliderFlags_None);
        ImGui::SliderFloat("Viscosity", &physics.viscosity, 0, 100, "%.0f", ImGuiSliderFlags_None);
        ImGui::SliderFloat("Rest Distance (%)", &physics.restDistanceRatio, 0, 1, "%.2f", ImGuiSliderFlags_None);
        ImGui::SliderFloat("Damping", &physics.damp, 0, 100, "%.0f", ImGuiSliderFlags_None);
    }

    if (ImGui::CollapsingHeader("Neighbour settings", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::InputInt("Max Neighbours", &physics.maxNeighbours);
        if (state.runSimulation) {
            auto stats = getStats(neighboursCount);
            _mean.push(stats.mean);
            _max.push(stats.max);
        }
        ImGui::PlotHistogram("Mean Neighbours", _mean.iterator, (void*)&_mean, _mean.count, 0, NULL, 0, physics.maxNeighbours, ImVec2(0, 40));
        ImGui::PlotHistogram("Max Neighbours", _max.iterator, (void*)&_max, _max.count, 0, NULL, 0, physics.maxNeighbours, ImVec2(0, 40));
    }

    if (ImGui::Button("Save settings")) {
        save(physics, state);
    }
    ImGui::SameLine();
    if (ImGui::Button("Load settings")) {
        load(physics, state);
    }
    ImGui::SameLine();
    if (ImGui::Button("Reset settings")) {
        physics = physics_s();
        state = state_s();
        _loadOnStart = false;
    }
    ImGui::SameLine();
    ImGui::Checkbox("Load on start", &_loadOnStart);

    if (state.oneStepMode) {
        if (ImGui::Button("Next Step")) {
            state.runSimulation = true;
        }
    } else {
        if (ImGui::Button(state.runSimulation ? "Stop Simulation" : "Run Simulation")) {
            state.runSimulation = !state.runSimulation;
        }
    }
    ImGui::SameLine();
    if (ImGui::Button("Reset Simulation")) {
        state.isResetSimulationRequested = true;
    }
    ImGui::SameLine();
    ImGui::Text("Current step: %d", step);

    ImGui::Checkbox("One Step Mode", &state.oneStepMode);
    ImGui::EndDisabled();
    ImGui::SameLine();
    ImGui::Checkbox("Show Boundary Points", &state.showBoundaryPoints);

    auto [allocations, usedMemory] = memory_manager.getStats();
    ImGui::Text("CUDA Cached Memory: %dMb", usedMemory >> 20);
    ImGui::SameLine();
    ImGui::Text("Particles count: %d", neighboursCount.count);
    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

    ImGui::End();
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void DebugWindow::keyControl(bool keys[1024], state_s& state) const {
    if (keys[GLFW_KEY_SPACE]) {
        keys[GLFW_KEY_SPACE] = false;
        state.runSimulation = !state.runSimulation;
    }
    if (keys[GLFW_KEY_R]) {
        keys[GLFW_KEY_R] = false;
        state.isResetSimulationRequested = true;
    }
    if (keys[GLFW_KEY_B]) {
        keys[GLFW_KEY_B] = false;
        state.renderDebugWindow = !state.renderDebugWindow;
    }
}

DebugWindow::~DebugWindow() {
    if (!_initialized) {
        return;
    }

    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
    _initialized = false;
}

void DebugWindow::save(const physics_s& physics, const state_s& state) const {
    std::ofstream f("settings.json");

    json j;
    j["radius"] = physics.radius;
    j["maxNeighbours"] = physics.maxNeighbours;
    j["maxGrixDims"] = physics.maxGrixDims;
    j["gravity"]["x"] = physics.gravity.x;
    j["gravity"]["y"] = physics.gravity.y;
    j["gravity"]["z"] = physics.gravity.z;
    j["restDistanceRatio"] = physics.restDistanceRatio;
    j["maxSpeed"] = physics.maxSpeed;
    j["cohesion"] = physics.cohesion;
    j["surfaceTension"] = physics.surfaceTension;
    j["relaxationFactor"] = physics.relaxationFactor;
    j["viscosity"] = physics.viscosity;
    j["damp"] = physics.damp;
    j["loadOnStart"] = _loadOnStart;
    j["showBoundaryPoints"] = state.showBoundaryPoints;
    j["oneStepMode"] = state.oneStepMode;

    f << std::setw(4) << j << std::endl;
}

void DebugWindow::load(physics_s& physics, state_s& state) {
    if (!std::filesystem::exists("settings.json")) {
        return;
    }

    std::ifstream f("settings.json");

    json j = json::parse(f);
    j["radius"].get_to(physics.radius);
    j["maxNeighbours"].get_to(physics.maxNeighbours);
    j["maxGrixDims"].get_to(physics.maxGrixDims);
    j["gravity"]["x"].get_to(physics.gravity.x);
    j["gravity"]["y"].get_to(physics.gravity.y);
    j["gravity"]["z"].get_to(physics.gravity.z);
    j["restDistanceRatio"].get_to(physics.restDistanceRatio);
    j["maxSpeed"].get_to(physics.maxSpeed);
    j["cohesion"].get_to(physics.cohesion);
    j["surfaceTension"].get_to(physics.surfaceTension);
    j["relaxationFactor"].get_to(physics.relaxationFactor);
    j["viscosity"].get_to(physics.viscosity);
    j["damp"].get_to(physics.damp);
    j["loadOnStart"].get_to(_loadOnStart);
    j["showBoundaryPoints"].get_to(state.showBoundaryPoints);
    j["oneStepMode"].get_to(state.oneStepMode);
}

void DebugWindow::loadRollouts() {
    std::filesystem::create_directories("rollouts");

    for (auto& p : std::filesystem::directory_iterator("rollouts")) {
        if (std::filesystem::is_regular_file(p.path())) {
            std::string name = p.path().filename().string();
            _rollouts[name] = Rollout::loadMeta("rollouts/" + name);
        }
    }
}

void DebugWindow::startRollout(const array_h<point>& locs, const array_h<point>& statics, physics_s physics, const array_h<plane_s>& boundaries) {
    _rollout.setStatics(statics);
    _rollout.setPhysics(physics);
    _rollout.setLocs(locs);
    _rollout.setBoundaries(boundaries);
}

void DebugWindow::addRolloutRecord(const array_h<point>& locs) {
    _rollout.pushLocs(locs);
}

array_h<point> DebugWindow::getRolloutStep(state_s& state) {
    auto locs = _rollout.getStep(state.playRoloutStep++);

    if (locs.count == 0) {
        state.playRoloutStep = -1;
        state.isResetSimulationRequested = true;
    }

    return locs;
}
