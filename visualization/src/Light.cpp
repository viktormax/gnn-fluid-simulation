#include "Light.hpp"

const std::string Light::uAmbientLight = "uAmbientLight";
const std::string Light::uDiffuseLight = "uDiffuseLight";
const std::string Light::uSpecularExponent = "uSpecularExponent";

void Light::init(glm::vec3 color, float ambientIntensity, glm::vec3 direction, float deffuseIntensity, float specularExponent) {
    _color = color;
    _ambientIntensity = ambientIntensity;
    _direction = direction;
    _diffuseIntensity = deffuseIntensity;
    _specularExponent = specularExponent;
}

void Light::useLight(const Shader& shader) const {
    if (shader.getUniformId(Light::uAmbientLight) != -1) {
        glUniform4f(shader.getUniformId(Light::uAmbientLight), _color.r, _color.g, _color.b, _ambientIntensity);
    }
    if (shader.getUniformId(Light::uDiffuseLight) != -1) {
        glUniform4f(shader.getUniformId(Light::uDiffuseLight), _direction.x, _direction.y, _direction.x, _diffuseIntensity);
    }
    if (shader.getUniformId(Light::uSpecularExponent) != -1) {
        glUniform1f(shader.getUniformId(Light::uSpecularExponent), _specularExponent);
    }
}
