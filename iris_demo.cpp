#include "network.h"
#include <chrono>

void printResults(matrix_h& inputs, matrix_h& outputs, matrix_h& predicted) {
    int errors = 0;
    for (int i = 0; i < inputs.rows; i++) {
        for (int j = 0; j < inputs.cols; j++) {
            printf("%.3f ", inputs(i, j));
        }
        bool counted = false;
        printf("(");
        for (int j = 0; j < outputs.cols; j++) {
            printf("%.0f", outputs(i, j));
            if (j < outputs.cols - 1) printf(", ");
        }
        printf(") (");
        for (int j = 0; j < predicted.cols; j++) {
            printf("%.0f", predicted(i, j));
            if (j < predicted.cols - 1) printf(", ");
        }
        printf(") <= (");
        for (int j = 0; j < predicted.cols; j++) {
            printf("%.5f", predicted(i, j));
            if (j < predicted.cols - 1) printf(", ");
        }
        printf(")");
        for (int j = 0; j < outputs.cols; j++) {
            if ((int)(outputs(i, j)) != round(predicted(i, j))) {
                errors++;
                printf(" error");
                break;
            }
        }
        printf("\n");
    }
    printf("Errors %d (%.2f%%) out of %d (100%%)\n", errors, errors * 100.0 / inputs.rows, (int)inputs.rows);
}

void main() {
    int rows = 150;
    matrix_h inputs(rows, 4), outputs(rows, 3), predicted;
    DataLoader::readCSV("resources/iris.csv", inputs, outputs);
    matrix_d dInputs(inputs), targets(outputs);

    Network net;
    net.add(new BatchNorm());
    net.add(new Layer(128, ActivationType::ReLU));
    net.add(new Layer(128, ActivationType::ReLU));
    net.add(new Layer(3));
    net.forward(dInputs);

    auto start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < 1000000; i++) {
        predicted = net.forward(dInputs);
        auto grads = net.loss(targets, predicted);

        printf("epoch: %d, cost: %f\n", (int)net.epoch, net.cost);
        if (net.cost < 0.02) {
            break;
        }

        net.backward(grads, 0.01);
    }
    auto stop = std::chrono::high_resolution_clock::now();

    predicted = net.forward(dInputs);
    printResults(inputs, outputs, predicted);

    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "Training time: " << duration.count() / 1000.0f << " s." << std::endl;
    system("pause");
}
