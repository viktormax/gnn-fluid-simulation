#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "core.h"
#include "network.h"
#include "sph.h"
#include "visualization.h"

void parseArgs(int argc, char** argv, int& scenario);
void noGravity12000(physics_s& physics, array_h<point>& particles, array_h<float3>& boundaryPoints, array_h<plane_s>& boundaries);
void regular12000(physics_s& physics, array_h<point>& particles, array_h<float3>& boundaryPoints, array_h<plane_s>& boundaries);
void regular2400(physics_s& physics, array_h<point>& particles, array_h<float3>& boundaryPoints, array_h<plane_s>& boundaries);
void regular28800(physics_s& physics, array_h<point>& particles, array_h<float3>& boundaryPoints, array_h<plane_s>& boundaries);

int main(int argc, char* argv[]) {
    int scenario = 2;

    parseArgs(argc, argv, scenario);

    Scene scene;
    scene.init();

    physics_s physics;
    array_h<point> particles;
    array_h<float3> boundaryPoints;
    array_h<plane_s> boundaries;

    if (scenario == 1) {
        noGravity12000(physics, particles, boundaryPoints, boundaries);
    } else if (scenario == 2) {
        regular12000(physics, particles, boundaryPoints, boundaries);
    } else if (scenario == 3) {
        regular2400(physics, particles, boundaryPoints, boundaries);
    } else if (scenario == 4) {
        regular28800(physics, particles, boundaryPoints, boundaries);
    } else if (scenario == 0) {
        physics = Dataset::getPhysics();
        auto [f, b] = Dataset::generareRandomFluid();
        boundaries = std::move(b);
        boundaryPoints = f.getStatics();
        particles = f.locs;
    }

    Fluid fluid;
    fluid.setParticles(particles, boundaryPoints);
    Solver solver;
    solver.setBoundaries(boundaries);
    Points points, staticPoints;
    points.create(particles.count);
    staticPoints.create(boundaryPoints.count);
    staticPoints.setPointSize(5);
    staticPoints.updateColors(color::rgb(52, 73, 94, 0.6));
    staticPoints.updatePositions(boundaryPoints);

    array_h<point> location = fluid.locs;
    array_h<point> velocity = fluid.vels;

    while (!scene.window.shouldClose()) {
        scene.createNewFrame();
        {
            if (scene.isResetSimulationRequested()) {
                fluid.setParticles(particles, boundaryPoints);
                solver.reset();
                location = fluid.locs;
                velocity = fluid.vels;
            }
            if (scene.playRolloutInProgress()) {
                auto next = scene.getRolloutStep();
                if (scene.getCurrentRoloutStep() == 1) {
                    velocity.resize(next.count);
                    velocity.init(make_point(0, 0, 0));
                } else if (next.count > 0){
                    velocity = (next - location) / 0.01;
                }
                location = std::move(next);
            }
            if (scene.isStartRolloutRequested()) {
                scene.startRollout(location, boundaryPoints, physics, boundaries);
            }
            if (scene.runSimulation()) {
                fluid = solver.run(fluid, physics);
                location = fluid.locs;
                velocity = fluid.vels;

                if (scene.rolloutRecordInProgress()) {
                    scene.addRolloutRecord(location);
                }
            }

            points.updatePositions(location);
            points.updateColors(mapVelocityToColor(velocity, scene.debug.getColors()));
            points.render(scene.camera, scene.light);

            if (scene.showBoundaryPoints()) {
                staticPoints.render(scene.camera, scene.light);
            }

            scene.renderDebugWindow(physics, solver.neighbours.count, solver.step);
        }
        scene.swapBuffers();

        memory_manager.runGarbitchCollector(5);
    }

    return 0;
}

void parseArgs(int argc, char** argv, int& scenario) {
    for (int i = 1; i < argc; i++) {
        std::string arg = argv[i];
        if (arg == "-s") {
            scenario = std::stoi(argv[++i]);
        }
    }
}

void noGravity12000(physics_s& physics, array_h<point>& particles, array_h<float3>& boundaryPoints, array_h<plane_s>& boundaries) {
    physics.gravity = { 0.0, 0.0, 0.0 };
    physics.radius = 0.1;
    physics.cohesion = 0.1;
    physics.surfaceTension = 20;
    physics.viscosity = 60;
    physics.restDistanceRatio = 0.55;
    physics.maxGrixDims = 128;
    physics.maxNeighbours = 128;
    physics.maxSpeed = 5.0;
    physics.relaxationFactor = 1.0;
    physics.damp = 1;

    particles = generateCubePoints(make_point(1, 1, 1), make_point(0.5, 0.5, 3), 0.05);
    particles.append(generateCubePoints(make_point(1, 2, 1), make_point(0.5, 0.5, 3), 0.05));
    boundaries = generateBoundariesStatic(make_point(0.9, 0, 0.9), make_point(4, 2.6, 3.2));
    boundaryPoints = generateBoundaryPoints(boundaries, physics.radius / 5);
}

void regular12000(physics_s & physics, array_h<point>&particles, array_h<float3>&boundaryPoints, array_h<plane_s>&boundaries) {
    physics.gravity = { 0.0, -9.8, 0.0 };
    physics.radius = 0.1;
    physics.cohesion = 0.025;
    physics.surfaceTension = 1;
    physics.viscosity = 5;
    physics.restDistanceRatio = 0.5;
    physics.maxGrixDims = 128;
    physics.maxNeighbours = 300;
    physics.maxSpeed = 5.0;
    physics.relaxationFactor = 1.0;
    physics.damp = 100;

    particles = generateCubePoints(make_point(1, 1, 1), make_point(0.5, 0.5, 3), 0.05);
    particles.append(generateCubePoints(make_point(1, 2, 1), make_point(0.5, 0.5, 3), 0.05));
    boundaries = generateBoundariesStatic(make_point(0.9, 0, 0.9), make_point(4, 2.6, 3.2));
    boundaryPoints = generateBoundaryPoints(boundaries, physics.radius / 5);
}

void regular2400(physics_s & physics, array_h<point>&particles, array_h<float3>&boundaryPoints, array_h<plane_s>&boundaries) {
    physics.gravity = { 0.0, -9.8, 0.0 };
    physics.radius = 0.1;
    physics.cohesion = 0.025;
    physics.surfaceTension = 1;
    physics.viscosity = 5;
    physics.restDistanceRatio = 0.55;
    physics.maxGrixDims = 128;
    physics.maxNeighbours = 300;
    physics.maxSpeed = 5.0;
    physics.relaxationFactor = 1.0;
    physics.damp = 30;

    particles = generateCubePoints(make_point(1, 1, 1), make_point(0.3, 1, 1), 0.05);
    boundaries = generateBoundariesStatic(make_point(0.9, 0.1, 0.9), make_point(2, 2, 2));
    boundaryPoints = generateBoundaryPoints(boundaries, physics.radius / 5);
}

void regular28800(physics_s & physics, array_h<point>&particles, array_h<float3>&boundaryPoints, array_h<plane_s>&boundaries) {
    physics.gravity = { 0.0, -9.8, 0.0 };
    physics.radius = 0.1;
    physics.cohesion = 0.025;
    physics.surfaceTension = 1;
    physics.viscosity = 5;
    physics.restDistanceRatio = 0.5;
    physics.maxGrixDims = 128;
    physics.maxNeighbours = 300;
    physics.maxSpeed = 5.0;
    physics.relaxationFactor = 1.0;
    physics.damp = 100;

    particles = generateCubePoints(make_point(1, 1, 1), make_point(1.2, 2.5, 1.2), 0.05);
    boundaries = generateBoundariesStatic(make_point(0.9, 0.1, 0.9), make_point(2, 4, 2));
    boundaryPoints = generateBoundaryPoints(boundaries, physics.radius / 5);
}
