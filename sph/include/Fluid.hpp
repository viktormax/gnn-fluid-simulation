#pragma once

#include "core.h"
#include "Neighbours.hpp"

class Fluid {
   public:
    const array_d<point>& allLocs;
    const array_d<float>& allMass;
    const array_d<point>& locs;
    const array_d<point>& vels;
    const array_d<float>& mass;
    const physics_s& physics;

    Fluid();

    Fluid(Fluid&& that);

    Fluid& operator=(const Fluid& that);

    Fluid& operator=(Fluid&& that);

    void setParticles(const array_h<point>& locs, const array_h<point>& statics);

    void setFluid(const array_d<point>& locs, const array_d<point>& vels);

    void setVelocity(point v);

    array_d<point> getStatics() const;

   private:
   // Lists of fluid particles properties, these are aligned between each other.
    array_d<point> _locs;        // Particle location
    array_d<point> _vels;        // Particle velocity
    array_d<float> _mass;

    // Lists of all particles properties, these are aligned between each other.
    array_d<point> _allLocs;
    array_d<float> _allMass;

    physics_s _physics;
};