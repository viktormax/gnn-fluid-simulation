#include "core.h"
#include "smoothingKernels.hpp"

void compute_grid_dims(
    array_d<point>& lower_bounds,
    array_d<point>& upper_bounds,
    array_d<point>& grid_dims,
    const physics_s& physics);

void compute_cell_ids(
    const array_d<point>& locs,
    const array_d<point>& lower_bounds,
    const array_d<point>& grid_dims,
    const physics_s& physics,
    array_d<uint32_t>& cell_ids,
    array_d<int>& indexes);

void fill_cells(
    const array_d<uint32_t>& cell_ids,
    array_d<uint32_t>& cell_starts,
    array_d<uint32_t>& cell_ends);

void compute_collisions(
    const array_d<point>& qlocs,
    const array_d<point>& locs,
    const array_d<uint32_t>& cell_starts,
    const array_d<uint32_t>& cell_ends,
    const array_d<int>& indexes,
    const array_d<point>& lower_bounds,
    const array_d<point>& grid_dims,
    const bool include_self,
    const bool use_unordered_index,
    const physics_s& physics,
    matrix_d& collisions,
    array_d<float>& collisions_count);

void cap_magnitude(
    const array_d<float3>& a,
    const float max,
    const array_d<float3>& out);

void conv_sp(
    const array_d<point>& locs,
    const float* data,
    const matrix_d& neighbors,
    const int data_count,
    const int data_dim,
    const bool dis_norm,
    const SmoothKernelType kernel,
    const physics_s physics,
    float* out);

void solve_boundaries(
    const array_d<plane_s>& planes,
    array_d<float3>& locs,
    array_d<float3>& vels);