#pragma once

#include "core.h"
#include "ConvSP.hpp"
#include "Fluid.hpp"

class Solver {
   public:
    const size_t& step;
    const array_d<plane_s>& boundaries;
    Neighbours neighbours;

    Solver();

    Fluid run(const Fluid& fluid, const physics_s& physics);

    void setBoundaries(const array_h<plane_s>& boundaries) {
        _boundaries = boundaries;
    }

    void reset() {
        _stepCount = 0;
    }

   private:
    ConvSP spiky;
    ConvSP dSpiky;
    ConvSP cohesion;
    ConvSP constant;
    ConvSP viscosity;

    array_d<plane_s> _boundaries;

    size_t _stepCount = 0;

    float _restDistanceRatio = -1;
    float _radius = -1;
    float densityRest = 0;
    float stiffness = 0;

    void updateStiffness(const physics_s& physics);
};
