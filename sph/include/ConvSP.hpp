#pragma once

#include "core.h"
#include "Neighbours.hpp"
#include "Fluid.hpp"
#include "smoothingKernels.hpp"

class ConvSP {
   public:
    void init(bool disNorm, SmoothKernelType kernel);

    void update(const Neighbours& neighbours, const Fluid& fluid, const physics_s& physics) {
        _neighbours = &neighbours;
        _physics = &physics;
        _fluid = &fluid;
    }

    array_d<point> operator()(const array_d<point>& data) const;

    array_d<float> operator()(const array_d<float>& data) const;

private:
    SmoothKernelType kernel;
    bool disNorm;
    const Fluid* _fluid;
    const Neighbours* _neighbours;
    const physics_s* _physics;
};
