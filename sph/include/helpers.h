#pragma once

#include <tuple>
#include <vector_types.h>

#include "core.h"

array_h<float3> generateTightPackedPoints(float radius, float restDistance);

std::tuple<float, float> calculateRestDensity(float radius, float restDistance);

array_h<float3> generateCubePoints(float3 start, float3 size, double radius);

array_h<float2> generateSquarePoints(float2 start, float2 size, float radius);

array_h<plane_s> generateBoundariesStatic(float3 s, float3 d);

array_h<float3> generateBoundaryPoints(const array_h<plane_s>& boundaries, double gap);

void translatePoints(array_h<float3>& points, float3 translate);

array_d<float3> capMagnitude(const array_d<float3>& a, const float max);

array_d<float2> capMagnitude(const array_d<float2>& a, const float max);

array_h<float4> mapVelocityToColor(const array_h<float3>& velocity, std::tuple<float3, float3> colors);

template <typename T>
void initArray(T* data, const long count,  const T val);
