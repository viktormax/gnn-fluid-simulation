#pragma once

#include "core.h"

class Neighbours {
   public:
    matrix_d data;
    array_d<float> count;

    Neighbours() {};

    Neighbours(Neighbours&& that);

    void compute(const array_d<point>& queries, const array_d<point>& locs, const physics_s& physics);

private:
    void computeGrid(const array_d<point>& locs, array_d<point>& lowerBound, array_d<point>& upperBound, array_d<point>& gridDims, const physics_s& physics);

    void computeIndices(const array_d<point>& locs, array_d<uint32_t>& cellIDs, array_d<int>& indices, const array_d<point>& lowerBound, const array_d<point>& gridDims, const physics_s& physics);
};