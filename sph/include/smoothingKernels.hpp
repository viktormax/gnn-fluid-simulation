#pragma once

#include <cuda_runtime_api.h>

#define M_PI 3.14159265358979323846

/**
 * Smoothing kernel types.
 */
enum SmoothKernelType {
    Constant,
    Cohesion, // From http://proceedings.mlr.press/v87/schenck18a/schenck18a.pdf
    DSpiky,   // Derivation of Spiky
    Spiky,    // Spiky for density, pressure computation in http://proceedings.mlr.press/v87/schenck18a/schenck18a.pdf
    Viscosity // From https://matthias-research.github.io/pages/publications/sca03.pdf
};

/**
 * Smoothing kernel function.
 *
 * @param d distance betwin points.
 * @param H cutoff value.
 * @param kernel type of kernel function.
 * @return computed value.
 */
inline __device__ __host__ float smoothing_kernel_w(float d, float H, SmoothKernelType kernel) {
    if (d > H) return 0.0f;

    switch (kernel) {
        case Constant:
            return 1;
        case Cohesion:
            return -6.0f * (d / H) * (d / H) * (d / H) + 7 * (d / H) * (d / H) - 1;
        case Spiky:
            return 15.0f / (M_PI * H * H * H) * (1.0f - d / H) * (1.0f - d / H);
        case DSpiky:
            return 30.0f / (M_PI * H * H * H * H * H) * (d - H);
        case Viscosity:
            return 15.0f / (2 * M_PI * H * H * H) * (-(d * d * d) / (2 * H * H * H) + (d * d) / (H * H) + H / (2 * d) - 1);
        default:
            return 0;
    }
}
