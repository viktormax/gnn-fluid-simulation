cmake_minimum_required(VERSION 3.18 FATAL_ERROR)
project(sph LANGUAGES CXX CUDA)

include(CheckLanguage)
check_language(CUDA)
enable_language(CUDA)

file(GLOB_RECURSE SOURCE_FILES_SUB "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cu" "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp")
file(GLOB_RECURSE HEADER_FILES_SUB "${CMAKE_CURRENT_SOURCE_DIR}/include/*.hpp" "${CMAKE_CURRENT_SOURCE_DIR}/include/*.h")

add_library(${PROJECT_NAME} STATIC ${SOURCE_FILES_SUB} ${HEADER_FILES_SUB})
 
target_link_libraries(${PROJECT_NAME} PRIVATE core)
target_include_directories(${PROJECT_NAME} PUBLIC include)
