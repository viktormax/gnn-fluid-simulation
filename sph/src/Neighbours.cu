#include <cub/cub.cuh>

#include "Neighbours.hpp"
#include "kernels.h"

struct FloatMin {
    __device__ __forceinline__ float3 operator()(const float3& a, const float3& b) const {
        return {a.x < b.x ? a.x : b.x, a.y < b.y ? a.y : b.y, a.z < b.z ? a.z : b.z};
    }

    __device__ __forceinline__ float2 operator()(const float2& a, const float2& b) const {
        return {a.x < b.x ? a.x : b.x, a.y < b.y ? a.y : b.y};
    }
};

struct FloatMax {
    __device__ __forceinline__ float3 operator()(const float3& a, const float3& b) const {
        return {a.x > b.x ? a.x : b.x, a.y > b.y ? a.y : b.y, a.z > b.z ? a.z : b.z};
    }

    __device__ __forceinline__ float2 operator()(const float2& a, const float2& b) const {
        return {a.x > b.x ? a.x : b.x, a.y > b.y ? a.y : b.y};
    }
};

Neighbours::Neighbours(Neighbours&& that) {
    this->data = std::move(that.data);
    this->count = std::move(that.count);
}

void Neighbours::compute(const array_d<point>& queries, const array_d<point>& locs, const physics_s& physics) {
    array_d<point> lowerBound(1), upperBound(1), gridDims(1);
    array_d<uint32_t> cellIDs(locs.count);
    array_d<int> indices(locs.count);

    computeGrid(locs, lowerBound, upperBound, gridDims, physics);
    computeIndices(locs, cellIDs, indices, lowerBound, gridDims, physics);

    auto ordered = locs.copy();
    ordered.reorder(indices);

    size_t cellsCount = pow(physics.maxGrixDims, 3);
    array_d<uint32_t> cellStarts(cellsCount);
    array_d<uint32_t> cellEnds(cellsCount);

    data.resize(queries.count, physics.maxNeighbours);
    count.resize(queries.count);

    cellStarts.init(0);
    cellEnds.init(0);
    data.init(-1);

    fill_cells(cellIDs, cellStarts, cellEnds);

    compute_collisions(
        queries, ordered, cellStarts, cellEnds, indices, lowerBound, gridDims,
        false, true, physics, data, count);
}

void Neighbours::computeGrid(const array_d<point>& locs, array_d<point>& lowerBound, array_d<point>& upperBound, array_d<point>& gridDims, const physics_s& physics) {
    size_t bufferSize = 0;

    FloatMin minOp;
    cub::DeviceReduce::Reduce(
        nullptr, bufferSize, locs(), lowerBound(), locs.count,
        minOp, make_point(FLT_MAX, FLT_MAX, FLT_MAX));
    array_d<char> buffer(bufferSize);
    cub::DeviceReduce::Reduce(
        buffer(), bufferSize, locs(), lowerBound(), locs.count,
        minOp, make_point(FLT_MAX, FLT_MAX, FLT_MAX));

    FloatMax maxOp;
    cub::DeviceReduce::Reduce(
        nullptr, bufferSize, locs(), upperBound(), locs.count,
        maxOp, make_point(FLT_MIN, FLT_MIN, FLT_MIN));
    buffer.resize(bufferSize);
    cub::DeviceReduce::Reduce(
        buffer(), bufferSize, locs(), upperBound(), locs.count,
        maxOp, make_point(FLT_MIN, FLT_MIN, FLT_MIN));

    compute_grid_dims(
        lowerBound, upperBound, gridDims, physics);
}

void Neighbours::computeIndices(const array_d<point>& locs, array_d<uint32_t>& cellIDs, array_d<int>& indices, const array_d<point>& lowerBound, const array_d<point>& gridDims, const physics_s& physics) {
    array_d<uint32_t> newCellIDs(locs.count);
    array_d<int> newIndices(locs.count);

    compute_cell_ids(locs, lowerBound, gridDims, physics, cellIDs, indices);

    cub::DoubleBuffer<uint32_t> keys(cellIDs(), newCellIDs());
    cub::DoubleBuffer<int> values(indices(), newIndices());

    // Sort indices by cell ID.
    size_t bufferSize = 0;
    cub::DeviceRadixSort::SortPairs(nullptr, bufferSize, keys, values, locs.count);
    array_d<char> buffer(bufferSize);
    cub::DeviceRadixSort::SortPairs(buffer(), bufferSize, keys, values, locs.count);

    cellIDs = std::move(newCellIDs);
    indices = std::move(newIndices);
}