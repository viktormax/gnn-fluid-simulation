#include "helpers.h"
#include "smoothingKernels.hpp"
#include "utils.h"
#include "constants.h"
#include "kernels.h"

array_h<float3> generateTightPackedPoints(float radius, float restDistance) {
    float separation = restDistance;
    int dim = ceil(radius / separation);
    array_h<float3> points(1000);
    points.resize(0);

    for (int z = -dim; z <= dim; z++) {
        for (int y = -dim; y <= dim; y++) {
            for (int x = -dim; x <= dim; x++) {
                float pX = x * separation * SQRT_0_75;
                float pY = y * separation * SQRT_0_75;
                float pZ = z * separation * SQRT_0_75;

                if (pX == 0 && pY == 0 && pZ == 0) {
                    continue;  // Skip center
                }

                if (sqrt(pX * pX + pY * pY + pZ * pZ) <= radius) {
                    points.push(make_float3(pX, pY, pZ));
                }
            }
        }
    }

    return points;
}

std::tuple<float, float> calculateRestDensity(float radius, float restDistance) {
    auto points = generateTightPackedPoints(radius, restDistance);
    float rho = 0;
    float rhoderiv = 0;
    for (int i = 0; i < points.count; i++) {
        float3 p = points[i];
        float distance = sqrt(p.x * p.x + p.y * p.y + p.z * p.z);
        rho += smoothing_kernel_w(distance, radius, SmoothKernelType::Spiky);
        rhoderiv += pow(smoothing_kernel_w(distance, radius, SmoothKernelType::DSpiky), 2);
    }
    float densityRest = rho;
    float stiffness = 1.0 / rhoderiv;
    return std::tuple(densityRest, stiffness);
}

array_h<float3> generateCubePoints(float3 start, float3 size, double radius) {
    float3 dim = size / radius;
    array_h<float3> points((dim.x + 1) * (dim.y + 1) * (dim.z + 1));
    points.resize(0);

    for (int z = 0; z < dim.z; z++) {
        for (int y = 0; y < dim.y; y++) {
            for (int x = 0; x < dim.x; x++) {
                points.push(start + (make_float3(x, y, z) * radius));
            }
        }
    }

    return points;
}

array_h<float2> generateSquarePoints(float2 start, float2 size, float radius) {
    float2 dim = size / radius;
    array_h<float2> points((dim.x + 1) * (dim.y + 1));
    points.resize(0);

    for (int y = 0; y < dim.y; y++) {
        for (int x = 0; x < dim.x; x++) {
            points.push(start + (make_float2(x, y) * radius));
        }
    }

    return points;
}

void translatePoints(array_h<float3>& points, float3 translate) {
    for (int i = 0; i < points.count; i++) {
        points[i] = points[i] + translate;
    }
}

array_d<point> capMagnitude(const array_d<point>& a, const float max) {
    array_d<point> out(a.count);
    cap_magnitude(a, max, out);
    return out;
}

static plane_s createPlaneStruct(float3 p1, float3 p2, float3 p3, float3 p4) {
    auto p12 = p2 - p1;
    auto p13 = p3 - p1;

    auto _normal = make_float3(
        p12.y * p13.z - p12.z * p13.y,
        p12.z * p13.x - p12.x * p13.z,
        p12.x * p13.y - p12.y * p13.x);

    auto _d = _normal.x * p1.x + _normal.y * p1.y + _normal.z * p1.z;

    // Normalize
    auto maxCoponent = max(abs(_normal));
    _normal = _normal / maxCoponent;
    _d = _d / maxCoponent;

    plane_s plane;
    plane.d = _d;
    plane.normal = _normal;
    plane.p[0] = p1;
    plane.p[1] = p2;
    plane.p[2] = p3;
    plane.p[3] = p4;
    return plane;
}

array_h<plane_s> generateBoundariesStatic(float3 s, float3 d) {
    array_h<plane_s> boundaries(6);

    boundaries[0] = createPlaneStruct(
        make_point(s.x + d.x, s.y, s.z + d.z),
        make_point(s.x + d.x, s.y, s.z),
        make_point(s.x, s.y, s.z),
        make_point(s.x, s.y, s.z + d.z));

    boundaries[1] = createPlaneStruct(
        make_point(s.x, s.y, s.z + d.z),
        make_point(s.x, s.y, s.z),
        make_point(s.x, s.y + d.y, s.z),
        make_point(s.x, s.y + d.y, s.z + d.z));

    boundaries[2] = createPlaneStruct(
        make_point(s.x, s.y, s.z),
        make_point(s.x + d.x, s.y, s.z),
        make_point(s.x + d.x, s.y + d.y, s.z),
        make_point(s.x, s.y + d.y, s.z));

    boundaries[3] = createPlaneStruct(
        make_point(s.x + d.x, s.y, s.z + d.z),
        make_point(s.x + d.x, s.y + d.y, s.z + d.z),
        make_point(s.x + d.x, s.y + d.y, s.z),
        make_point(s.x + d.x, s.y, s.z));

    boundaries[4] = createPlaneStruct(
        make_point(s.x + d.x, s.y, s.z + d.z),
        make_point(s.x, s.y, s.z + d.z),
        make_point(s.x, d.y, s.z + d.z),
        make_point(s.x + d.x, d.y, s.z + d.z));

    boundaries[5] = createPlaneStruct(
        make_point(s.x + d.x, s.y + d.y, s.z + d.z),
        make_point(s.x, s.y + d.y, s.z + d.z),
        make_point(s.x, s.y + d.y, s.z),
        make_point(s.x + d.x, s.y + d.y, s.z));


    return boundaries;
}

array_h<float3> generateBoundaryPoints(const array_h<plane_s>& boundaries, double gap) {
    array_h<float3> points(400000);
    points.resize(0);

    for (int i = 0; i < boundaries.count; i++) {
        // Plane points
        auto boundary = boundaries[i];
        auto start = boundary.p[0];
        auto end = boundary.p[2];
        auto diagonal = end - start;
        auto dir = make_float3(
            diagonal.x >= 0 ? (diagonal.x == 0 ? 0 : 1) : -1,
            diagonal.y >= 0 ? (diagonal.y == 0 ? 0 : 1) : -1,
            diagonal.z >= 0 ? (diagonal.z == 0 ? 0 : 1) : -1);
        auto length = abs(diagonal);
        auto gp = diagonal / ceil(diagonal / gap);

        for (float x = 0; x <= length.x; x += gp.x) {
            for (float y = 0; y <= length.y; y += gp.y) {
                for (float z = 0; z <= length.z; z += gp.z) {
                    points.push(start + make_float3(x, y, z) * dir);
                }
            }
        }

        // Outter plane points
        start = boundary.p[0] - dir * gap / 2;
        end = boundary.p[2] + dir * gap / 2;
        diagonal = end - start;
        length = abs(diagonal);
        start = start - boundary.normal * gap / 2;

        for (float x = 0; x <= length.x; x += gp.x) {
            for (float y = 0; y <= length.y; y += gp.y) {
                for (float z = 0; z <= length.z; z += gp.z) {
                    points.push(start + make_float3(x, y, z) * dir);
                }
            }
        }
    }

    return points;
}

array_h<float4> mapVelocityToColor(const array_h<float3>& velocity, std::tuple<float3, float3> colors) {
    auto& [slowColor, fastColor] = colors;
    array_h<float4> out(velocity.count);

    for (int i = 0; i < velocity.count; i++) {
        auto tmp = interpolate(slowColor, fastColor, length(velocity[i]), 0.8);
        out[i] = make_float4(tmp.x, tmp.y, tmp.z, 1);
    }
    return out;
}
