#pragma once

#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <math.h>

#include "ConvSP.hpp"
#include "Fluid.hpp"
#include "utils.h"
#include "kernels.h"

void ConvSP::init(bool disNorm, SmoothKernelType kernel) {
    this->kernel = kernel;
    this->disNorm = disNorm;
}

array_d<point> ConvSP::operator()(const array_d<point>& data) const {
    array_d<point> out(_neighbours->data.rows);

    conv_sp(
        _fluid->allLocs, (float*)data(), _neighbours->data,
        data.count, 3, disNorm, kernel, *_physics, (float*)out());

    return out;
}

array_d<float> ConvSP::operator()(const array_d<float>& data) const {
    array_d<float> out(_neighbours->data.rows);

    conv_sp(
        _fluid->allLocs, data(), _neighbours->data,
        data.count, 1, disNorm, kernel, *_physics, out());

    return out;
}
