#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>

#include "constants.h"
#include "kernels.h"
#include "smoothingKernels.hpp"

/**
 *  Calculate grid dimensions and adjust bounding box to the center of fluid.
 *
 * @param[in,out] lower_bounds lower bounding box point.
 * @param[in,out] upprt_bounds upper bounding box point.
 * @param[in,out] grid_dims grid dimensions.
 * TODO: update
 */
__global__ void kernel_compute_grid_dims(
    array_s<point> lower_bounds,
    array_s<point> upper_bounds,
    array_s<point> grid_dims,
    const physics_s p) {
    auto index = blockIdx.x * blockDim.x + threadIdx.x;

    if (index == 0) {
        grid_dims[0] = ceil(clamp((upper_bounds[0] - lower_bounds[0]) / p.radius, 1, p.maxGrixDims));
        auto center = (lower_bounds[0] + upper_bounds[0]) / 2.0f;
        lower_bounds[0] = center - grid_dims[0] * p.radius / 2.0f;
        upper_bounds[0] = center + grid_dims[0] * p.radius / 2.0f;
    }
}

/**
 * Calculates a unique cell hash from cell coordinates whre a particle is located.
 * TODO: update
 * @param locs particle locations.
 * @param lower_bounds lower bounding box point.
 * @param grid_dims grid dimensions.
 * @param count number of particles.
 * @param[out] cell_ids cell IDs of particles, aligned with indexes.
 * @param[out] indexes indexes of particles, aligned with cell_ids.
 */
__global__ void kernel_compute_cell_ids(
    const array_s<point> locs,
    const array_s<point> lower_bounds,
    const array_s<point> grid_dims,
    const physics_s p,
    array_s<uint32_t> cell_ids,
    array_s<int> indexes) {
    auto index = blockIdx.x * blockDim.x + threadIdx.x;

    if (index < locs.count) {
        auto cell = location_to_grid_cell(locs[index], lower_bounds[0], p.radius);
        auto hash = grid_cell_to_hash(cell, grid_dims[0]);

        cell_ids[index] = hash;
        indexes[index] = index;
    }
}

__global__ void kernel_fill_cells(
    const array_s<uint32_t> cell_ids,
    array_s<uint32_t> cell_starts,
    array_s<uint32_t> cell_ends) {
    auto index = blockIdx.x * blockDim.x + threadIdx.x;

    if (index < cell_ids.count) {
        auto curr = cell_ids[index];

        if (index == 0) {
            cell_starts[curr] = index;
        } else {
            auto prev = cell_ids[index - 1];

            if (curr != prev) {
                cell_starts[curr] = index;
                cell_ends[prev] = index;
            }
        }

        if (index == cell_ids.count - 1) {
            cell_ends[curr] = index + 1;
        }
    }
}

__global__ void kernel_compute_collisions(
    const array_s<point> qlocs,
    const array_s<point> locs,
    const array_s<uint32_t> cell_starts,
    const array_s<uint32_t> cell_ends,
    const array_s<int> indexes,
    const array_s<point> lower_bounds,
    const array_s<point> grid_dims,
    const bool include_self,
    const bool use_unordered_index,
    const physics_s p,
    matrix_s collisions,
    array_s<float> collisions_count) {
    auto qindex = blockIdx.x * blockDim.x + threadIdx.x;

    const float radius2 = pow(p.radius, 2);
    const int offset_length = pow(3, DATA_DIM);

    if (qindex < qlocs.count) {
        auto collisions_ptr = collisions.row(qindex);
        int collision_counter = 0;
        auto grid_coord = location_to_grid_cell(qlocs[qindex], lower_bounds[0], p.radius);

        for (int i = 0; i < offset_length; i++) {
            auto offset = index_to_offset(i);
            auto cell_coord = grid_coord + offset;

            if (!is_in_bounds(cell_coord, grid_dims[0])) {
                continue;
            }

            auto cell_id = grid_cell_to_hash(cell_coord, grid_dims[0]);
            auto lindex = cell_starts[cell_id];
            while (lindex< cell_ends[cell_id] && collision_counter < p.maxNeighbours) {
                float distance2 = squared_distance(qlocs[qindex], locs[lindex]);

                if (distance2 < radius2 && (distance2 > 0 || include_self)) {
                    collisions_ptr[collision_counter] = use_unordered_index ? indexes[lindex] : lindex;
                    collision_counter++;
                }

                lindex++;
            }
        }

        collisions_count[qindex] = collision_counter;
    }
}

__global__ void kernel_cap_magnitude(const array_s<float3> a, const float max, array_s<float3> out) {
    auto index = blockIdx.x * blockDim.x + threadIdx.x;

    if (index < a.count) {
        float len = length(a[index]);
        float correction = len == 0 ? 1 : fminf(len, max) / len;
        out[index] = a[index] * correction;
    }
}

__global__ void kernel_conv_sp(
    const array_s<point> locs,
    const float* data,
    const matrix_s neighbors,
    const int data_count,
    const int data_dim,
    const bool dis_norm,
    const SmoothKernelType kernel,
    const physics_s physics,
    float* out) {
    auto query_index = blockIdx.x * blockDim.x + threadIdx.x;

    if (query_index >= neighbors.rows) {
        return;
    }

    float* out_ptr = out + query_index * data_dim;
    float radius2 = pow(physics.radius, 2);

    for (int j = 0; j < data_dim; j++) {
        out_ptr[j] = 0;
    }

    const float* neighbors_ptr = neighbors.row(query_index);
    for (int i = 0; i < neighbors.cols && neighbors_ptr[i] >= 0; i++) {
        int neighbour_index = neighbors_ptr[i];
        if (neighbour_index >= data_count) {
            continue;
        }

        float3 neighbour = locs[neighbour_index];
        float distance2 = squared_distance(locs[query_index], neighbour);

        if (distance2 >= radius2) {
            continue;
        }

        float distance = sqrtf(distance2);

        float norm = 1;
        if (dis_norm && distance > 0) {
            norm = norm / distance;
        }

        float kw = smoothing_kernel_w(distance, physics.radius, kernel);
        const float* dataPtr = data + neighbour_index * data_dim;

        for (int j = 0; j < data_dim; j++) {
            out_ptr[j] += dataPtr[j] * kw * norm;
        }
    }
}

__global__ void kernel_solve_boundaries(
    const array_s<plane_s> planes,
    array_s<float3> locs,
    array_s<float3> vels) {
    auto index = blockIdx.x * blockDim.x + threadIdx.x;

    if (index < locs.count) {
        auto& loc = locs[index];
        auto& vel = vels[index];

        for (int i = 0; i < planes.count; i++) {
            auto normal = planes[i].normal;
            auto dist = normal.x * loc.x + normal.y * loc.y + normal.z * loc.z - planes[i].d;
            if (dist < 0) {
                auto newLoc = loc - normal * dist;
                vel = newLoc - loc;
                loc = newLoc;
            }
        }
    }
}

void compute_grid_dims(
    array_d<point>& lower_bounds,
    array_d<point>& upper_bounds,
    array_d<point>& grid_dims,
    const physics_s& physics) {
    kernel_compute_grid_dims<<<KERNEL_PROPS(1)>>>(
        lower_bounds.str(), upper_bounds.str(), grid_dims.str(), physics);
}

void compute_cell_ids(
    const array_d<point>& locs,
    const array_d<point>& lower_bounds,
    const array_d<point>& grid_dims,
    const physics_s& physics,
    array_d<uint32_t>& cell_ids,
    array_d<int>& indexes) {
    kernel_compute_cell_ids<<<KERNEL_PROPS(locs.count)>>>(
        locs.str(), lower_bounds.str(), grid_dims.str(), 
        physics, cell_ids.str(), indexes.str());
}

void fill_cells(
    const array_d<uint32_t>& cell_ids,
    array_d<uint32_t>& cell_starts,
    array_d<uint32_t>& cell_ends) {
    kernel_fill_cells<<<KERNEL_PROPS(cell_ids.count)>>>(
        cell_ids.str(), cell_starts.str(), cell_ends.str());
}

void compute_collisions(
    const array_d<point>& qlocs,
    const array_d<point>& locs,
    const array_d<uint32_t>& cell_starts,
    const array_d<uint32_t>& cell_ends,
    const array_d<int>& indexes,
    const array_d<point>& lower_bounds,
    const array_d<point>& grid_dims,
    const bool include_self,
    const bool use_unordered_index,
    const physics_s& physics,
    matrix_d& collisions,
    array_d<float>& collisions_count) {
    kernel_compute_collisions<<<KERNEL_PROPS(qlocs.count)>>>(
        qlocs.str(), locs.str(), cell_starts.str(), cell_ends.str(), indexes.str(),
        lower_bounds.str(), grid_dims.str(), include_self, use_unordered_index, 
        physics, collisions.str(), collisions_count.str());
}

void cap_magnitude(
    const array_d<float3>& a, 
    const float max, 
    const array_d<float3>& out) {
    kernel_cap_magnitude<<<KERNEL_PROPS(a.count)>>>(a.str(), max, out.str());
}

void conv_sp(
    const array_d<point>& locs,
    const float* data,
    const matrix_d& neighbors,
    const int data_count,
    const int data_dim,
    const bool dis_norm,
    const SmoothKernelType kernel,
    const physics_s physics,
    float* out) {
    kernel_conv_sp<<<KERNEL_PROPS(neighbors.rows)>>>(
        locs.str(), data, neighbors.str(), data_count, 
        data_dim, dis_norm, kernel, physics, out);
}

void solve_boundaries(
    const array_d<plane_s>& planes,
    array_d<float3>& locs,
    array_d<float3>& vels) {
    kernel_solve_boundaries<<<KERNEL_PROPS(locs.count)>>>(
        planes.str(), locs.str(), vels.str());
}