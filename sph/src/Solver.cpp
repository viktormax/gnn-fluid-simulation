#include "Solver.hpp"

#include "Fluid.hpp"
#include "helpers.h"
#include "utils.h"
#include "kernels.h"

Solver::Solver(): step(_stepCount), boundaries(_boundaries) {
    spiky.init(false, SmoothKernelType::Spiky);
    viscosity.init(false, SmoothKernelType::Viscosity);
    dSpiky.init(true, SmoothKernelType::DSpiky);
    cohesion.init(true, SmoothKernelType::Cohesion);
    constant.init(false, SmoothKernelType::Constant);
}

void Solver::updateStiffness(const physics_s& physics) {
    if (_radius == physics.radius && _restDistanceRatio == physics.restDistanceRatio) {
        return;
    }

    auto [densityRest, stiffness] = calculateRestDensity(physics.radius, physics.radius * physics.restDistanceRatio);
    this->densityRest = densityRest;
    this->stiffness = stiffness;
    _radius = physics.radius;
    _restDistanceRatio = physics.restDistanceRatio;
}

Fluid Solver::run(const Fluid& in, const physics_s& physics) {
    updateStiffness(physics);
    float dt = 0.01;
    Fluid fluid;
    fluid = in;

    auto locs = fluid.locs;
    auto vels = fluid.vels;

    // Gravity
    vels = vels + physics.gravity * dt;
    locs = locs + vels * dt;

    vels = capMagnitude(vels, physics.maxSpeed);
    fluid.setFluid(locs, vels);

    neighbours.compute(fluid.locs, fluid.allLocs, physics);
    spiky.update(neighbours, fluid, physics);
    dSpiky.update(neighbours, fluid, physics);
    viscosity.update(neighbours, fluid, physics);
    cohesion.update(neighbours, fluid, physics);
    constant.update(neighbours, fluid, physics);

    // DENSITY - computed including boundary (aka. ghost) particles.
    auto density = spiky(fluid.allMass);
    auto ni = dSpiky(fluid.allLocs * fluid.allMass);
    auto nj = locs * dSpiky(fluid.allMass);
    auto nij = nj - ni;

    // PRESSURE
    auto pressure = max(density - densityRest, 0) * stiffness;
    ni = dSpiky(locs * pressure);
    nj = locs * dSpiky(pressure);
    auto nijp = nj - ni;
    auto delta = -nij * pressure - nijp;

    // COHESION
    ni = cohesion(locs * fluid.mass);
    nj = locs * cohesion(fluid.mass);
    nij = nj - ni;
    delta = delta - nij * physics.radius * physics.cohesion;

    // SURFACE TENSION
    delta = delta - constant(nij) * (physics.surfaceTension / densityRest);

    // APPLAY RELAXATION
    auto scale = neighbours.count / (1 + physics.relaxationFactor);
    scale = max(scale, physics.damp);
    delta = delta / scale;

    // POSITION UPDATE
    auto& prevLocs = in.locs;
    locs = locs + delta;
    vels = (locs - prevLocs) / dt;  // Update velocity from the real locatin change

    // VISCOSITY
    auto vi = viscosity(vels * fluid.mass);
    auto vj = vels * viscosity(fluid.mass);
    auto vij = vj - vi;
    vels = vels - vij * (dt * physics.viscosity / densityRest);

    if (_boundaries.count > 0) {
        solve_boundaries(_boundaries, locs, vels);
    }

    _stepCount++;
    fluid.setFluid(locs, vels);
    return fluid;
}
