#include "Fluid.hpp"

Fluid::Fluid() : physics(_physics), locs(_locs), vels(_vels), mass(_mass), allLocs(_allLocs), allMass(_allMass) {
}

Fluid::Fluid(Fluid&& that) : Fluid() {
    _allLocs = std::move(that._allLocs);
    _allMass = std::move(that._allMass);
    _vels = std::move(that._vels);
    _locs = std::move(that._locs);
    _mass = std::move(that._mass);
}

Fluid& Fluid::operator=(const Fluid& that) {
    _allLocs = that._allLocs;
    _allMass = that._allMass;
    _vels = that._vels;
    _locs = that._locs;
    _mass = that._mass;
    return *this;
}

Fluid& Fluid::operator=(Fluid&& that) {
    _allLocs = std::move(that._allLocs);
    _allMass = std::move(that._allMass);
    _vels = std::move(that._vels);
    _locs = std::move(that._locs);
    _mass = std::move(that._mass);
    return *this;
}

void Fluid::setParticles(const array_h<point>& locs, const array_h<point>& statics) {
    _locs = locs;
    _vels.resize(locs.count).init(make_point(0, 0, 0));
    _mass.resize(locs.count).init(1);

    _allLocs = _locs;
    _allLocs.append(statics);
    _allMass = _mass;
    _allMass.append(array_d<float>(statics.count).init(1));
}

void Fluid::setFluid(const array_d<point>& locs, const array_d<point>& vels) {
    _allLocs.insert(locs);
    _locs = locs;
    _vels = vels;
}

void Fluid::setVelocity(point v) {
    _vels.init(v);
}

array_d<point> Fluid::getStatics() const {
    return allLocs.copy(locs.count);
}
