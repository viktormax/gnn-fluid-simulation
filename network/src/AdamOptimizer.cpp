#include "AdamOptimizer.hpp"

void AdamOptimizer::toStream(std::ofstream& wf) const {
    wf.write((char*)&_t, sizeof(_t));
    wf.write((char*)&_isArray, sizeof(_isArray));
    if (_isArray) {
        _ma.toStream(wf);
        _va.toStream(wf);
    } else {
        _mm.toStream(wf);
        _vm.toStream(wf);
    }
}

void AdamOptimizer::fromStream(std::ifstream& rf) {
    rf.read((char*)&_t, sizeof(_t));
    rf.read((char*)&_isArray, sizeof(_isArray));
    if (_isArray) {
        _ma.fromStream(rf);
        _va.fromStream(rf);
    } else {
        _mm.fromStream(rf);
        _vm.fromStream(rf);
    }
}

matrix_d AdamOptimizer::operator()(const matrix_d& param, const matrix_d& grads, float learningRate) {
    if (_mm.elements == 0) {
        _isArray = false;
        _mm.resize(grads.rows, grads.cols);
        _vm.resize(grads.rows, grads.cols);
        _mm.init(0);
        _vm.init(0);
    }

    _t++;
    _mm = _mm * _beta1 + grads * (1 - _beta1);
    _vm = _vm * _beta2 + grads.pow(2) * (1 - _beta2);

    auto m_hat = _mm / (1 - pow(_beta1, _t)) * learningRate;
    auto v_hat = _vm / (1 - pow(_beta2, _t));

    return param - m_hat / (v_hat.pow(0.5) + _epsilon);
}

array_d<float> AdamOptimizer::operator()(const array_d<float>& param, const array_d<float>& grads, float learningRate) {
    if(_ma.count == 0) {
        _isArray = true;
        _ma.resize(grads.count);
        _va.resize(grads.count);
        _ma.init(0);
        _va.init(0);
    }

    _t++;
    _ma = _ma * _beta1 +  grads * (1 - _beta1);
    _va = _va * _beta2 +  pow(grads, 2) * (1 - _beta2);

    auto m_hat = _ma / (1 - pow(_beta1, _t)) * learningRate;
    auto v_hat = _va / (1 - pow(_beta2, _t));

    return param - m_hat / (sqrt(v_hat) + _epsilon);
}

matrix_d AdamOptimizer::opt(const matrix_d& param, const matrix_d& grads, float learningRate) {
    if (_mm.elements == 0) {
        _isArray = false;
        _mm.resize(grads.rows, grads.cols);
        _vm.resize(grads.rows, grads.cols);
        _mm.init(0);
        _vm.init(0);
    }

    _t++;
    auto lr = learningRate * sqrt(1 - pow(_beta2, _t)) / pow(_beta1, _t);

    _mm = _mm * _beta1 + grads * (1 - _beta1);
    _vm = _vm * _beta2 + grads.pow(2) * (1 - _beta2);

    return param - _mm * lr / (_vm.pow(0.5) + _epsilon);
}
