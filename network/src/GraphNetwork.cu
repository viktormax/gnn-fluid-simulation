#include <vector>

#include "GraphNetwork.hpp"
#include "Layer.hpp"
#include "LayerNorm.hpp"
#include "BatchNorm.hpp"
#include "Network.hpp"

void GraphNetwork::init(int hiddenLayers, int hiddenSize, int outputCount, ActivationType activation, bool independent) {
    for (int i = 0; i < hiddenLayers; i++) {
        _nodeNetwork.add(new Layer(hiddenSize, activation));
        _edgeNetwork.add(new Layer(hiddenSize, activation));
    }

    // Output layer
    _nodeNetwork.add(new Layer(outputCount));
    _edgeNetwork.add(new Layer(outputCount));
    _nodeNetwork.add(new LayerNorm());
    _edgeNetwork.add(new LayerNorm());

    _independent = independent;
}

Graph GraphNetwork::predict(const Graph& input) const {
    Graph output = input;

    if (_independent) {
        output.edges = _edgeNetwork.predict(input.edges);
        output.nodes = _nodeNetwork.predict(input.nodes);
        return output;
    }

    output.edges = _edgeNetwork.predict(input.broadcastNodesToEdges());
    output.nodes = _nodeNetwork.predict(input.broadcastEdgesToNodes(output.edges));

    return output;
}


Graph GraphNetwork::forward(const Graph& input) {
    Graph output = input;
    _node_size = input.nodes.size();
    _edge_size = input.edges.size();

    if (_independent) {
        output.edges = _edgeNetwork.forward(input.edges);
        output.nodes = _nodeNetwork.forward(input.nodes);
        return output;
    }

    output.edges = _edgeNetwork.forward(input.broadcastNodesToEdges());
    output.nodes = _nodeNetwork.forward(input.broadcastEdgesToNodes(output.edges));

    return output;
}

GraphGrads GraphNetwork::backward(const Graph& graph, const GraphGrads& grads, float learningRate) {
    GraphGrads out;
    auto& nodeGrads = std::get<0>(grads);
    auto& edgeGrads = std::get<1>(grads);
    
    if (_independent) {
        std::get<0>(out) = nodeGrads ? std::optional(_nodeNetwork.backward(*nodeGrads, learningRate)) : std::nullopt;
        std::get<1>(out) = edgeGrads ? std::optional(_edgeNetwork.backward(*edgeGrads, learningRate)) : std::nullopt;
    } else {
        auto gradsTmp = _nodeNetwork.backward(*nodeGrads, learningRate);

        auto nodeTmpGrad = gradsTmp.copy(0, gradsTmp.cols - _node_size.cols);
        auto edgeTmpGrad = gradsTmp.copy(0, 0, gradsTmp.rows, gradsTmp.cols - _node_size.cols);

        edgeTmpGrad = graph.spredEdgeGrads(edgeTmpGrad);
        if (edgeGrads) {
            edgeTmpGrad = edgeTmpGrad + (*edgeGrads);
        }

        edgeTmpGrad = _edgeNetwork.backward(edgeTmpGrad, learningRate);
        nodeTmpGrad = graph.spredNodeGrads(nodeTmpGrad, edgeTmpGrad.copy(0, _edge_size.cols));
        edgeTmpGrad = edgeTmpGrad.copy(0, 0, _edge_size.rows, _edge_size.cols);

        std::get<0>(out) = std::optional(std::move(nodeTmpGrad));
        std::get<1>(out) = std::optional(std::move(edgeTmpGrad));
    }

    return out;
}

void GraphNetwork::toStream(std::ofstream& wf) const {
    wf.write((char*)&_independent, sizeof(_independent));
    _nodeNetwork.toStream(wf);
    _edgeNetwork.toStream(wf);
}

void GraphNetwork::fromStream(std::ifstream& rf) {
    rf.read((char*)&_independent, sizeof(_independent));
    _nodeNetwork.fromStream(rf);
    _edgeNetwork.fromStream(rf);
}
