#include <device_launch_parameters.h>
#include <cuda_runtime.h>
#include <cub/cub.cuh> 

#include "kernels.h"

namespace {
__global__ void kernel_activate_vector(const float* in, float* out, const size_t size, const ActivationType type) {
    auto index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < size) {
        out[index] = activate(in[index], type);
    }
}

__global__ void kernel_activate_deriv_vector(const float* in, float* out, const size_t size, const ActivationType type) {
    auto index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < size) {
        out[index] = activate_deriv(in[index], type);
    }
}

__global__ void kernel_construct_edge_features(matrix_s edges, array_s<uint32_t> senders, array_s<uint32_t> receivers, const array_s<float3> locs, const matrix_s neighbours, const array_s<uint32_t> indexes, float radius) {
    auto index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < neighbours.rows) {
        auto sample_neighbous = neighbours.row(index);

        for (size_t j = 0; j < neighbours.cols && sample_neighbous[j] >= 0; j++) {
            size_t neighbour_index = sample_neighbous[j];
            auto edge = edges.row(indexes[index] + j);
            float3 receiver = locs[index];
            float3 sender = locs[neighbour_index];
            float3 disp = (receiver - sender) / radius;

            edge[0] = disp.x;
            edge[1] = disp.y;
            edge[2] = disp.z;
            edge[3] = length(disp);

            senders[indexes[index] + j] = neighbour_index;
            receivers[indexes[index] + j] = index;
        }
    }
}

__global__ void kernel_construct_node_features(
    matrix_s nodes, 
    const array_s<float3> locs, 
    const float3* const* vels, 
    const size_t vels_count, 
    const array_s<plane_s> boundaries) {
    auto index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < nodes.rows) {
        auto node = nodes.row(index);
        auto loc = locs[index];

        int k = 0;
        node[k++] = loc.x;
        node[k++] = loc.y;
        node[k++] = loc.z;

        for (int i = 0; i < boundaries.count; i++) {
            auto normal = boundaries[i].normal;
            auto dist = normal.x * loc.x + normal.y * loc.y + normal.z * loc.z - boundaries[i].d;
            node[k++] = dist;
        }

        for (int i = 0; i < vels_count; i++) {
            auto v = vels[i][index];
            node[k++] = v.x;
            node[k++] = v.y;
            node[k++] = v.z;
        }

        for (int i = k; i < nodes.cols; i++) {
            node[i] = 0;
        }
    }
}

__global__ void kernel_get_total_size(const array_s<float> neighbours_count, array_s<uint32_t> indexes, array_s<uint32_t> size) {
    auto index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index == 0) {
        auto count = neighbours_count.count;
        indexes[count] = indexes[count - 1] + neighbours_count[count - 1];
        size[0] = indexes[count];
    }
}

__global__ void kernel_broadcast_nodes_to_edges(
    const matrix_s nodes,
    const matrix_s edges,
    const array_s<uint32_t> senders,
    const array_s<uint32_t> receivers,
    matrix_s outs) {
    auto index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < outs.elements) {
        auto out_row = index / outs.cols;
        auto out_col = index % outs.cols;

        auto col = out_col;
        if (col < edges.cols) {
            outs[index] = edges(out_row, col);
            return;
        }

        col -= edges.cols;
        if (col < nodes.cols) {
            outs[index] = nodes(receivers[out_row], col);
            return;
        }

        col -= nodes.cols;
        if (col < nodes.cols) {
            outs[index] = nodes(senders[out_row], col);
            return;
        }
    }
}

__global__ void kernel_unsorted_segment_sum(
    const matrix_s input, const array_s<uint32_t> indices, matrix_s out) {
    auto index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < input.elements) {
        auto segment_idx = indices[index / input.cols];
        atomicAdd(&out(segment_idx, index % input.cols), input[index]);
    }
}

__global__ void kernel_spred_edge_grads(
    const matrix_s grads, const array_s<uint32_t> receivers, matrix_s outs) {
    auto index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < outs.elements) {
        auto out_row = index / outs.cols;
        auto out_col = index % outs.cols;
        auto rec_row = receivers[out_row];

        outs[index] = grads(rec_row, out_col);
    }
}

__global__ void kernel_spred_node_grads(
    const matrix_s edgeGgrads,
    const array_s<uint32_t> receivers,
    const array_s<uint32_t> senders,
    matrix_s outs) {
    auto index = threadIdx.x + blockDim.x * blockIdx.x;

    if (index < edgeGgrads.elements) {
        auto edge_row = index / edgeGgrads.cols;
        auto edge_col = index % edgeGgrads.cols;
        
        if (edge_col < outs.cols) {
            auto out_row = receivers[edge_row];
            atomicAdd(&outs(out_row, edge_col), edgeGgrads[index]);
        } else {
            auto out_row = senders[edge_row];
            atomicAdd(&outs(out_row, edge_col - outs.cols), edgeGgrads[index]);
        }
    }
}
}  // namespace

matrix_d activate(const matrix_d& in, const ActivationType type) {
    matrix_d out(in.rows, in.cols);
    kernel_activate_vector<<<KERNEL_PROPS(in.elements)>>>(in(), out(), in.elements, type);
    return out;
}

matrix_d activate_deriv(const matrix_d& in, const ActivationType type) {
    matrix_d out(in.rows, in.cols);
    kernel_activate_deriv_vector<<<KERNEL_PROPS(in.elements)>>>(in(), out(), in.elements, type);
    return out;
}

void construct_edge_features(
    matrix_d& edges,
    array_d<uint32_t>& senders,
    array_d<uint32_t>& receivers,
    const array_d<float3>& locs,
    const matrix_d& neighbours,
    const array_d<uint32_t>& indexes,
    float radius) {
    kernel_construct_edge_features<<<KERNEL_PROPS(locs.count)>>>(
        edges.str(), senders.str(), receivers.str(), locs.str(), neighbours.str(), indexes.str(), radius);
}

void construct_node_features(
    matrix_d& nodes,
    const array_d<float3>& locs,
    const array_ptr<float3>& vels_list,
    const array_d<plane_s>& boundaries) {
    kernel_construct_node_features<<<KERNEL_PROPS(nodes.rows)>>>(
        nodes.str(), locs.str(), vels_list(), vels_list.count, boundaries.str());
}

void get_total_size(const array_d<float>& neighbours_count, array_d<uint32_t>& indexes, array_d<uint32_t>& size) {
    kernel_get_total_size<<<KERNEL_PROPS(1)>>>(neighbours_count.str(), indexes.str(), size.str());
}

matrix_d broadcast_nodes_to_edges(
    const matrix_d& nodes,
    const matrix_d& edges,
    const array_d<uint32_t>& senders,
    const array_d<uint32_t>& receivers) {
    matrix_d out(edges.rows, edges.cols + nodes.cols * 2);
    kernel_broadcast_nodes_to_edges<<<KERNEL_PROPS(out.elements)>>>(
        nodes.str(), edges.str(), senders.str(), receivers.str(), out.str());
    return out;
}

matrix_d broadcast_edges_to_nodes(
    const matrix_d& nodes,
    const matrix_d& edges,
    const array_d<uint32_t>& receivers) {
    matrix_d out(nodes.rows, nodes.cols + edges.cols);
    out.init(0);
    
    kernel_unsorted_segment_sum<<<KERNEL_PROPS(edges.elements)>>>(edges.str(), receivers.str(), out.str());
    out.insert(nodes, 0, edges.cols);

    return out;
}

matrix_d spred_edge_grads(const matrix_d& grads, const array_d<uint32_t>& receivers) {
    matrix_d out(receivers.count, grads.cols);

    kernel_spred_edge_grads<<<KERNEL_PROPS(out.elements)>>>(grads.str(), receivers.str(), out.str());

    return out;
}

matrix_d spred_node_grads(
    const matrix_d& nodeGrads, 
    const matrix_d& edgeGrads, 
    const array_d<uint32_t>& receivers,
    const array_d<uint32_t>& senders) {
    matrix_d out = nodeGrads;

    kernel_spred_node_grads<<<KERNEL_PROPS(edgeGrads.elements)>>>(edgeGrads.str(), receivers.str(), senders.str(), out.str());

    return out;
}

float max(const matrix_d& mat) {
    matrix_d max(1, 1);

    size_t storage_bytes;
    cub::DeviceReduce::Max(nullptr, storage_bytes, mat(), max(), mat.elements);
    array_d<char> tmp(storage_bytes);
    cub::DeviceReduce::Max(tmp(), storage_bytes, mat(), max(), mat.elements);

    return matrix_h(max)[0];
}
