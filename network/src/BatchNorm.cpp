#include "BatchNorm.hpp"

BatchNorm::BatchNorm(float momentum) : moving_mean(_moving_mean), moving_var(_moving_var), beta(_beta), gamma(_gamma) {
    _momentum = momentum;
}

matrix_d BatchNorm::predict(const matrix_d& batch) const {
    if (batch.cols != _moving_mean.count) {
        throw std::runtime_error("Input number of features does not match model.");
    }

    return (batch - _moving_mean) * pow(_moving_var + _eps, -0.5) * _gamma + _beta;
}

matrix_d BatchNorm::forward(const matrix_d& batch) {
    resize(batch.cols);

    auto batch_mean = batch.sumCols() / batch.rows;
    _xmu = batch - batch_mean;
    auto batch_var = _xmu.pow(2).sumCols() / batch.rows;

    _moving_mean = _moving_mean * _momentum + batch_mean * (1.0 - _momentum);
    _moving_var = _moving_var * _momentum + batch_var * (1.0 - _momentum);

    _ivar = pow(batch_var + _eps, -0.5);
    _xnorm = _xmu * _ivar;

    _cached = true;
    return _xnorm * _gamma + _beta;
}

// https://deepnotes.io/batchnorm
// https://www.adityaagrawal.net/blog/deep_learning/bprop_batch_norm
matrix_d BatchNorm::backward(const matrix_d& grads, float learningRate) {
    if (!_cached) {
        throw std::runtime_error("Run forward pass before backward.");
    }

    auto dx_norm = grads * _gamma;

    auto dvar = (dx_norm * _xmu).sumCols() * -0.5 * pow(_ivar, 3);
    auto dmu =  dvar * (_xmu).sumCols() / (grads.rows * -0.5) - (dx_norm * _ivar).sumCols();

    _beta = _beta - grads.sumCols() * learningRate;
    _gamma = _gamma - (grads * _xnorm).sumCols() * learningRate;
    auto out = dx_norm * _ivar + dmu / grads.rows + (_xmu * dvar * (2.0 / grads.rows));

    _xmu.free();
    _xnorm.free();
    _ivar.free();

    _cached = false;
    return out;
}

ModuleType BatchNorm::type() const {
    return ModuleType::BatchNorm;
}

void BatchNorm::toStream(std::ofstream& wf) const {
    _gamma.toStream(wf);
    _beta.toStream(wf);
    _moving_mean.toStream(wf);
    _moving_var.toStream(wf);
}

void BatchNorm::fromStream(std::ifstream& rf) {
    _gamma.fromStream(rf);
    _beta.fromStream(rf);
    _moving_mean.fromStream(rf);
    _moving_var.fromStream(rf);
}

void BatchNorm::resize(int cols) {
    if (_gamma.count != cols) {
        if (_gamma.count != 0) {
            throw std::runtime_error("Resize of initialized layer.");
        }

        _moving_mean.resize(cols, 0);
        _moving_var.resize(cols, 1);
        _gamma.resize(cols, 1);
        _beta.resize(cols, 0);
    }
}
