#include "NetworkModel.hpp"
#include "Layer.hpp"
#include <fstream>

void NetworkModel::init(int outputs, int hiddenCount, int neuronCount, ActivationType activation) {
    _encoder.init(2, neuronCount, neuronCount, activation, true);

    _hidden.resize(hiddenCount);
    for(auto& h : _hidden) {
        h.init(2, neuronCount, neuronCount, activation);
    }

    _decoder.add(new Layer(neuronCount, activation));
    _decoder.add(new Layer(neuronCount, activation));
    _decoder.add(new Layer(outputs));
}

void NetworkModel::save(const std::string& fileName) const {
    std::ofstream wf(fileName, std::ios::out | std::ios::binary);
    if (!wf) {
        printf("Cannot open file!\n");
        return;
    }

    _encoder.toStream(wf);

    size_t hSize = _hidden.size();
    wf.write((char*)&hSize, sizeof(hSize));
    for (int i = 0; i < hSize; i++) {
        _hidden[i].toStream(wf);
    }

    _decoder.toStream(wf);

    wf.close();
}

bool NetworkModel::load(const std::string& fileName) {
    std::ifstream rf(fileName, std::ios::in | std::ios::binary);
    if (!rf) {
        printf("Cannot open file!\n");
        return false;
    }

    fromStream(rf);

    rf.close();
    return true;
}

void NetworkModel::fromStream(std::ifstream& rf) {
    _encoder.fromStream(rf);

    size_t hSize = 0;
    rf.read((char*)&hSize, sizeof(hSize));
    _hidden.resize(hSize);
    for (int i = 0; i < hSize; i++) {
        _hidden[i].fromStream(rf);
    }

    _decoder.fromStream(rf);
}

matrix_d NetworkModel::predict(const Graph& input) const {
    Graph latent = _encoder.predict(input);

    for (int i = 0; i < _hidden.size(); i++) {
        latent = latent + _hidden[i].predict(latent);
    }

    return _decoder.predict(latent.nodes);
}

matrix_d NetworkModel::forward(const Graph& input) {
    Graph latent = _encoder.forward(input);

    for (int i = 0; i < _hidden.size(); i++) {
        latent = latent + _hidden[i].forward(latent);
    }

    return _decoder.forward(latent.nodes);
}

 GraphGrads NetworkModel::backward(const Graph& input, const matrix_d& predicted, const matrix_d& target, float learningRate) {
    _grads = _decoder.loss(target, predicted);
    GraphGrads grads(std::optional(_decoder.backward(_grads, learningRate)), std::nullopt);

    for (int i = _hidden.size() - 1; i >= 0; i--) {
        auto [n, e] = _hidden[i].backward(input, grads, learningRate);
        std::get<0>(grads) = std::get<0>(grads) ? *std::get<0>(grads) + *n : *n;
        std::get<1>(grads) = std::get<1>(grads) ? *std::get<1>(grads) + *e : *e;
    }
    
    return _encoder.backward(input, grads, learningRate);
}

matrix_d NetworkModel::train(const Graph& input, const matrix_d& target, float learningRate) {
    auto predicted = forward(input);

    backward(input, predicted, target, learningRate);

    return predicted;
}

double NetworkModel::cost() const {
    return _decoder.cost;
}

size_t NetworkModel::epoch() const {
    return _decoder.epoch;
}
