#include "GraphBuilder.hpp"
#include "GraphBuilder.hpp"
#include <cub/cub.cuh>

#include "GraphBuilder.hpp"
#include "Neighbours.hpp"
#include "kernels.h"

GraphBuilder::GraphBuilder() : locs(_locs), vels(_vels), boundaries(_boundaries) {
}

void GraphBuilder::setBoundaries(const array_d<plane_s>& boundaries) {
    _boundaries = boundaries;
}

void GraphBuilder::pushLocations(const array_d<float3>& locs) {
    if (_locs.count != 0) {
        _vels = locs - _locs;

        _vels_list.push_front(_vels);
        if (_vels_list.size() > velocitiesCount) {
            _vels_list.pop_back();
        }
    }
    _locs = locs;
}

size_t GraphBuilder::initialLoad(const Rollout& rollout) {
    _vels.free();
    _locs.free();
    _vels_list.empty();

    size_t step = 0;
    pushLocations(rollout.getStep(step++));
    for (int i = 0; i < velocitiesCount; i++) {
        pushLocations(rollout.getStep(step++));
    }
    return step;
}

matrix_d GraphBuilder::getAccelerations(const array_d<float3>& locs) {
    auto vels = locs - _locs;
    return toMatrix(vels - _vels);
}

Graph GraphBuilder::buildGraph(const physics_s& physics) {
    Graph graph;
    Neighbours neighbours;
    neighbours.compute(_locs, _locs, physics);

    graph.nodes.resize(locs.count, 3 * velocitiesCount + _boundaries.count + 3);
    construct_node_features(graph.nodes, locs, array_ptr(_vels_list), _boundaries);

    array_d<uint32_t> indexes(neighbours.count.count + 1);
    size_t bufferSize = 0;
    cub::DeviceScan::ExclusiveSum(nullptr, bufferSize, neighbours.count(), indexes(), neighbours.count.count);
    array_d<char> buffer(bufferSize);
    cub::DeviceScan::ExclusiveSum(buffer(), bufferSize, neighbours.count(), indexes(), neighbours.count.count);

    array_d<uint32_t> rows(1);
    get_total_size(neighbours.count, indexes, rows);
    auto edgeNumber = array_h(rows)[0];

    graph.edges.resize(edgeNumber, 3 + 1);
    graph.senders = std::make_shared<array_d<uint32_t>>(edgeNumber);
    graph.receivers = std::make_shared<array_d<uint32_t>>(edgeNumber);
    construct_edge_features(graph.edges, *graph.senders, *graph.receivers, locs, neighbours.data, indexes, physics.radius);

    return graph;
}