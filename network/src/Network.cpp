#include "Network.hpp"
#pragma once

#include "BatchNorm.hpp"
#include "Layer.hpp"
#include "LayerNorm.hpp"
#include "Network.hpp"
#include "kernels.h"

Network::Network() : cost(_cost), epoch(_epoch) {
}

Network::~Network() {
    for (auto layer : _layers) {
        delete layer;
    }
}

void Network::add(Module* layer) {
    this->_layers.push_back(layer);
}

matrix_d Network::predict(const matrix_d& inputs) const {
    auto data = inputs;

    for (auto layer : _layers) {
        data = layer->predict(data);
    }

    return data;
}

matrix_d Network::forward(const matrix_d& inputs) {
    auto data = inputs;

    for (auto layer : _layers) {
        data = layer->forward(data);
    }

    return data;
}

matrix_d Network::backward(const matrix_d& grads, const float learning_rate) {
    auto data = _layers.back()->backward(grads, learning_rate);

    for (int i = _layers.size() - 2; i >= 0; i--) {
        data = _layers[i]->backward(data, learning_rate);
    }
    
    if (learning_rate > 0) {
        _epoch++;
    }

    return data;
}

ModuleType Network::type() const {
    return ModuleType::Network;
}

matrix_d Network::loss(const matrix_d& target, const matrix_d& predicted) {
    auto [grads, cost] = MSE(target, predicted);
    _cost = cost;

    return grads;
}

std::tuple<matrix_d, double> Network::MSE(const matrix_d& target, const matrix_d& predicted) {
    auto deltas = predicted - target;
    array_h sample_delta = deltas.pow(2).sumRows() / deltas.cols;

    double cost = 0;
    for (int i = 0; i < sample_delta.count; i++) {
        cost += sample_delta[i];
    }
    cost = cost / predicted.rows;

    return {deltas * (2.0 / (predicted.cols * predicted.rows)), cost};
}

void Network::toStream(std::ofstream& wf) const {
    auto layers_count = _layers.size();

    wf.write((char*)&_cost, sizeof(_cost));
    wf.write((char*)&_epoch, sizeof(_epoch));
    wf.write((char*)&layers_count, sizeof(layers_count));

    for (auto layer : _layers) {
        ModuleType lType = layer->type();
        wf.write((char*)&lType, sizeof(lType));
        layer->toStream(wf);
    }
}

void Network::fromStream(std::ifstream& rf) {
    auto layers_count = _layers.size();

    rf.read((char*)&_cost, sizeof(_cost));
    rf.read((char*)&_epoch, sizeof(_epoch));
    rf.read((char*)&layers_count, sizeof(layers_count));

    _layers.resize(layers_count);
    for (auto& layer : _layers) {
        ModuleType lType;
        rf.read((char*)&lType, sizeof(lType));

        if (lType == ModuleType::Layer) {
            layer = new Layer();
        } else if (lType == ModuleType::BatchNorm) {
            layer = new BatchNorm();
        } else if (lType == ModuleType::LayerNorm) {
            layer = new LayerNorm();
        } else {
            throw "Unknown module.";
        }

        layer->fromStream(rf);
    }
}

std::ostream& operator<<(std::ostream& ss, const Network& n) {
    ss << "{\"cost\":" << n.cost << ",\"epoch\":" << n.epoch << ",\"layers\":[";
    for (int i = 0; i < n._layers.size(); i++) {
        ss << *(Layer*)n._layers[i];
        if (i < n._layers.size() - 1) {
            ss << ",";
        }
    }
    ss << "]}";
    return ss;
}
