#include "LayerNorm.hpp"

bool LayerNorm::useAdamOptimizer = true;

LayerNorm::LayerNorm() : beta(_beta), gamma(_gamma) {
}

matrix_d LayerNorm::predict(const matrix_d& batch) const {
    if (batch.cols != _gamma.count) {
        throw std::runtime_error("Input number of features does not match model.");
    }

    auto mean = batch.sumRows() / batch.cols;
    auto xmu = batch.transpose() - mean;
    auto var = xmu.pow(2).sumCols() * (1.0f / batch.cols);
    auto normalized = (xmu * pow(var + _eps, -0.5)).transpose();

    return normalized * _gamma + _beta;
}

matrix_d LayerNorm::forward(const matrix_d& batch) {
    resize(batch.cols);

    auto mean = batch.sumRows() / batch.cols;
    _xmu = batch.transpose() - mean;
    auto var = _xmu.pow(2).sumCols() * (1.0f / batch.cols);
    _ivar = pow(var + _eps, -0.5);
    _xnorm = (_xmu * _ivar).transpose();

    _cached = true;
    return _xnorm * _gamma + _beta;
}

matrix_d LayerNorm::backward(const matrix_d& grads, float learningRate) {
    if (!_cached) {
        throw std::runtime_error("Run forward pass before backward.");
    }

    auto dx_norm = (grads * _gamma).transpose();

    auto dvar = (dx_norm * _xmu).sumCols() * -0.5 * pow(_ivar, 3);
    auto dmu = dvar * (_xmu).sumCols() / (grads.rows * -0.5) - (dx_norm * _ivar).sumCols();
    auto out = dx_norm * _ivar + dmu / grads.cols + (_xmu * dvar * (2.0 / grads.cols));

    if (useAdamOptimizer) {
        _beta = _opb(_beta, grads.sumCols(), learningRate);
        _gamma = _opb(_gamma, (grads * _xnorm).sumCols(), learningRate);
    } else {
        _beta = _beta - grads.sumCols() * learningRate;
        _gamma = _gamma - (grads * _xnorm).sumCols() * learningRate;
    }

    _xmu.free();
    _xnorm.free();
    _ivar.free();

    _cached = false;
    return out.transpose();
}

ModuleType LayerNorm::type() const {
    return ModuleType::LayerNorm;
}

void LayerNorm::toStream(std::ofstream& wf) const {
    _gamma.toStream(wf);
    _beta.toStream(wf);
}

void LayerNorm::fromStream(std::ifstream& rf) {
    _gamma.fromStream(rf);
    _beta.fromStream(rf);
}

void LayerNorm::resize(int cols) {
    if (_gamma.count != cols) {
        if (_gamma.count != 0) {
            throw std::runtime_error("Resize of initialized layer.");
        }

        _gamma.resize(cols, 1);
        _beta.resize(cols, 0);
    }
}
