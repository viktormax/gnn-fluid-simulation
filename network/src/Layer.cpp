#include "Layer.hpp"
#include "Layer.hpp"
#include <curand.h>
#include <time.h>
#include "utils.h"
#include "activation.h"
#include "kernels.h"
#include <cuda.h>

bool Layer::useAdamOptimizer = true;

Layer::Layer(const size_t neuronCount, ActivationType activationType) :
        activationType(_activationType), neuronCount(_neuronCount), weights(_weights), biases(_biases)  {
    _neuronCount = neuronCount;
    _activationType = activationType;
}

Layer::Layer(const Layer& layer) : Layer(layer._neuronCount, layer._activationType) {
    _weights = layer._weights;
    _biases = layer._biases;
}

void Layer::checkInputSizes(int cols) {
    if (cols == _weights.rows) {
        return;
    }

    if (_weights.rows != 0) {
        printf("Resize of initialized layer!!!\n");
        exit(EXIT_FAILURE);
    }

    _biases.resize(_neuronCount);
    _biases.init(0);
    _weights.resize(cols, _neuronCount);
    randomize();
}

void Layer::randomize() {
    curandGenerator_t prng;
    curandCreateGenerator(&prng, CURAND_RNG_PSEUDO_DEFAULT);
    curandSetPseudoRandomGeneratorSeed(prng, clock());

    curandGenerateUniform(prng, _weights(), _weights.elements);

    _weights = (_weights - 0.5) * 0.25;

    curandDestroyGenerator(prng);
}

void Layer::setWeights(const matrix_d& weights, const array_d<float>& biases) {
    if (weights.cols != _neuronCount || biases.count != neuronCount) {
        printf("Invalid weights size!!!\n");
        exit(EXIT_FAILURE);
    }
    _weights = weights;
    _biases = biases;
}

matrix_d Layer::predict(const matrix_d& inputs) const {
    if (inputs.cols != _weights.rows) {
        throw std::runtime_error("Input number of features does not match model.");
    }

    matrix_d unactivated;
    dotMatrix(inputs, _weights, unactivated);

    return activate(unactivated + _biases, _activationType);
}

matrix_d Layer::forward(const matrix_d& inputs) {
    checkInputSizes(inputs.cols);
    this->_inputs = inputs;

    dotMatrix(_inputs, _weights, _unactivated);

    _unactivated = _unactivated + _biases;
    auto activated = activate(_unactivated, _activationType);

    _cahced = true;
    return activated;
}

matrix_d Layer::backward(const matrix_d& grads, float learningRate) {
    if (!_cahced) {
        throw std::runtime_error("Run forward pass before backward."); 
    }

    matrix_d outGrads;
    auto currGrads = grads * activate_deriv(_unactivated, _activationType);

    dotMatrixBT(currGrads, _weights, outGrads);

    auto gb = currGrads.sumCols();
    matrix_d gW;
    dotMatrixAT(_inputs, currGrads, gW);

    if (useAdamOptimizer) {
        _biases = _ob(_biases, gb, learningRate);
        _weights = _oW(_weights, gW, learningRate);
    } else {
        _biases = _biases - gb * learningRate;
        _weights = _weights - gW * learningRate;
    }

    _inputs.free();
    _unactivated.free();
    _cahced = false;
    return outGrads;
}

ModuleType Layer::type() const {
    return ModuleType::Layer;
}

void Layer::toStream(std::ofstream& wf) const {
    wf.write((char*)&_activationType, sizeof(_activationType));
    wf.write((char*)&_neuronCount, sizeof(_neuronCount));
    _weights.toStream(wf);
    _biases.toStream(wf);
    _ob.toStream(wf);
    _oW.toStream(wf);
}

void Layer::fromStream(std::ifstream& rf) {
    rf.read((char*)&_activationType, sizeof(_activationType));
    rf.read((char*)&_neuronCount, sizeof(_neuronCount));
    _weights.fromStream(rf);
    _biases.fromStream(rf);
    _ob.fromStream(rf);
    _oW.fromStream(rf);
}

std::ostream& operator<<(std::ostream& ss, const Layer& layer) {
    ss << "{\"activation\":" << (int)layer._activationType << ",\"neuronCount\":" << layer._neuronCount;
    ss << ",\"weights\":" << layer._weights;
    ss.flush();
    ss << ",\"biases\":" << layer._biases << "}";
    ss.flush();
    return ss;
}
