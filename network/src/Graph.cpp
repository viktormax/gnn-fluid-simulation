#include "Graph.hpp"

#include <stdexcept>

#include "kernels.h"

Graph::Graph() {
}

Graph::Graph(Graph&& that) {
    nodes = std::move(that.nodes);
    edges = std::move(that.edges);
    senders = std::move(that.senders);
    receivers = std::move(that.receivers);
}

matrix_d Graph::broadcastNodesToEdges() const {
    return broadcast_nodes_to_edges(nodes, edges, *senders, *receivers);
}

matrix_d Graph::broadcastEdgesToNodes(const matrix_d& edges) const {
    return broadcast_edges_to_nodes(nodes, edges, *receivers);
}

matrix_d Graph::spredEdgeGrads(const matrix_d& grads) const {
    return spred_edge_grads(grads, *receivers);
}

matrix_d Graph::spredNodeGrads(const matrix_d& nodeGrads, const matrix_d& edgeGrads) const {
    return spred_node_grads(nodeGrads, edgeGrads, *receivers, *senders);
}

Graph::Graph(const Graph& that) {
    senders = that.senders;
    receivers = that.receivers;
    nodes = that.nodes;
    edges = that.edges;
}

Graph& Graph::operator=(const Graph& that) {
    senders = that.senders;
    receivers = that.receivers;
    nodes = that.nodes;
    edges = that.edges;
    return *this;
}

Graph Graph::operator+(const Graph& that) {
    Graph graph;
    graph.senders = that.senders;
    graph.receivers = that.receivers;
    graph.nodes = this->nodes + that.nodes;
    graph.edges = this->edges + that.edges;
    return graph;
}

std::ostream& operator<<(std::ostream& ss, const Graph& graph) {
    ss << "{\"nodes\": " << graph.nodes << ",\n\"edges\": " << graph.edges;
    ss << ",\n\"receivers\": " << graph.receivers << ",\n\"senders\": " << graph.senders;
    return ss << "}";
}

void Graph::toStream(std::ofstream& wf) const {
    nodes.toStream(wf);
    edges.toStream(wf);
    senders->toStream(wf);
    receivers->toStream(wf);
}

void Graph::fromStream(std::ifstream& rf) {
    senders = std::make_shared<array_d<uint32_t>>();
    receivers = std::make_shared<array_d<uint32_t>>();

    nodes.fromStream(rf);
    edges.fromStream(rf);
    senders->fromStream(rf);
    receivers->fromStream(rf);
}