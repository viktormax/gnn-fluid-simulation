

#include "AdamOptimizer.hpp"

#include <gtest/gtest.h>

#include "utils.h"

class AdamOptimizerTest : public testing::Test {
   protected:
    static void SetUpTestSuite() {
        CREATE_TMP_DIR();
        RUN_PYTHON(AdamOptimizerTest);
    }
};

TEST_F(AdamOptimizerTest, RunTest) {
    AdamOptimizer op1;
    AdamOptimizer op2;
    matrix_h weights, weights_grads, exp_weights;
    array_h<float> biases, biases_grads, exp_biases;

    LOAD_DATA(AdamOptimizerTest, RunTest, weights);
    LOAD_DATA(AdamOptimizerTest, RunTest, weights_grads);
    LOAD_DATA(AdamOptimizerTest, RunTest, exp_weights);
    LOAD_DATA(AdamOptimizerTest, RunTest, biases);
    LOAD_DATA(AdamOptimizerTest, RunTest, biases_grads);
    LOAD_DATA(AdamOptimizerTest, RunTest, exp_biases);
   
    auto new_weights = op1(weights, weights_grads, 1e-4);
    auto new_biases = op2(biases, biases_grads, 1e-4);

    COMPARE_ARR(new_biases, exp_biases);
    COMPARE_MAT(new_weights, exp_weights);
}
