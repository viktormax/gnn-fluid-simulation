# %%
import ctypes
import tensorflow as tf
import graph_network
import keras
import utils as u
import numpy as np

# %%
def add_graphs(g1, g2):
  g = g1
  return g.replace(
    nodes=g.nodes+g2.nodes,
    edges=g.edges+g2.edges)

def save_layer(f, l, activation):
  f.write(ctypes.c_int(0))
  f.write(ctypes.c_int(activation))
  f.write(ctypes.c_ulonglong(l.weights[1].shape[0]))
  u.save_matrix(f, l.weights[0])
  u.save_array(f, l.weights[1])

def save_layer_norm(f, l):
  f.write(ctypes.c_int(1))
  u.save_array(f, l.gamma)
  u.save_array(f, l.beta)

def save_net(f, n, offset):
  f.write(ctypes.c_double(0))
  f.write(ctypes.c_ulonglong(0))
  f.write(ctypes.c_ulonglong(4))
  save_layer(f, n.submodules[offset], 1)
  save_layer(f, n.submodules[offset + 1], 1)
  save_layer(f, n.submodules[offset + 2], 0)
  if (len(n.submodules) > offset + 3):
    save_layer_norm(f, n.submodules[offset + 3])

def save_model(f, m):
    if isinstance(f, str):
        with open(f, 'wb') as f:
            save_model(f, m)
    else:
        sub = m.submodules
        # Encoder
        f.write(ctypes.c_bool(True))
        save_net(f, sub[1].submodules[7], 0)
        save_net(f, sub[1].submodules[2], 0)
        # Process
        f.write(ctypes.c_ulonglong(5))
        for i in range(5):
            f.write(ctypes.c_bool(False))
            save_net(f, sub[2 + i].submodules[1], 2)
            save_net(f, sub[2 + i].submodules[0], 1)
        # Decoder
        f.write(ctypes.c_double(0))
        f.write(ctypes.c_ulonglong(0))
        f.write(ctypes.c_ulonglong(3))
        save_layer(f, sub[7], 1)
        save_layer(f, sub[8], 1)
        save_layer(f, sub[9], 0)

# %%
SUITE = "NetworkModelTest"
SEQUENCE_LENGTH = 6

rollout = u.load_rollout(u.f(SUITE, "rollout"))
neighbours = []
with open(u.f(SUITE, "neighbours"), 'rb') as f:
    for i in range(rollout['steps']):
        neighbours.append([n[n != -1] for n in u.load_matrix(f)])

model_kwargs = dict(
    latent_size=128,
    mlp_hidden_size=128,
    mlp_num_hidden_layers=2,
    num_message_passing_steps=5,
)

# %%
TEST = "ForwardTest"
graph = u.build_graph(rollout, neighbours, SEQUENCE_LENGTH, 0)
net = graph_network.EncodeProcessDecode(output_size=3, **model_kwargs)
out = net(graph)
target = rollout['positions'][SEQUENCE_LENGTH] - rollout['positions'][SEQUENCE_LENGTH - 1]
loss = keras.losses.MeanSquaredError('sum_over_batch_size')(target, out)

u.save_graph(u.f(SUITE, TEST, "graph"), graph)
u.save_matrix(u.f(SUITE, TEST, "exp_out"), out)
save_model(u.f(SUITE, TEST, "model"), net)

# %%
TEST = "BackwardTest"
graph = u.build_graph(rollout, neighbours, SEQUENCE_LENGTH, 0)
net = graph_network.EncodeProcessDecode(output_size=3, **model_kwargs)
target = rollout['positions'][SEQUENCE_LENGTH] - rollout['positions'][SEQUENCE_LENGTH - 1]

with tf.GradientTape() as tape:
    tape.watch(graph.nodes)
    tape.watch(graph.edges)
    
    latent_graph_0 = net._encode(graph)
    latent_graph_1 = add_graphs(latent_graph_0, net._processor_networks[0](latent_graph_0))
    latent_graph_2 = add_graphs(latent_graph_1, net._processor_networks[1](latent_graph_1))
    latent_graph_3 = add_graphs(latent_graph_2, net._processor_networks[2](latent_graph_2))
    latent_graph_4 = add_graphs(latent_graph_3, net._processor_networks[3](latent_graph_3))
    out_4 = net._processor_networks[4](latent_graph_4)
    latent_graph_5 = add_graphs(latent_graph_4, out_4)
    out = net._decode(latent_graph_5)

    loss = keras.losses.MeanSquaredError('sum_over_batch_size')(target, out)

grads = tape.gradient(loss, [out, graph.nodes, graph.edges, 
                             out_4.nodes, latent_graph_5.nodes, 
                             latent_graph_4.nodes])

u.save_graph(u.f(SUITE, TEST, "graph"), graph)
u.save_matrix(u.f(SUITE, TEST, "exp_grads"), grads[0])
u.save_matrix(u.f(SUITE, TEST, "exp_grads_nodes"), grads[1])
u.save_matrix(u.f(SUITE, TEST, "exp_grads_edges"), grads[2])
u.save_matrix(u.f(SUITE, TEST, "target"), target)
u.save_array(u.f(SUITE, TEST, "exp_loss"), np.array([loss], dtype=np.float32))
save_model(u.f(SUITE, TEST, "model"), net)

# %%
TEST = "FullStepTest"
step = 47 - SEQUENCE_LENGTH
graph = u.build_graph(rollout, neighbours, SEQUENCE_LENGTH, step)
net = graph_network.EncodeProcessDecode(output_size=3, **model_kwargs)
target = rollout['positions'][step + SEQUENCE_LENGTH] - rollout['positions'][step + SEQUENCE_LENGTH - 1]

with tf.GradientTape() as tape:
    tape.watch(graph.nodes)
    tape.watch(graph.edges)
    latent_graph_0 = net._encode(graph)
    latent_graph_1 = add_graphs(latent_graph_0, net._processor_networks[0](latent_graph_0))
    latent_graph_2 = add_graphs(latent_graph_1, net._processor_networks[1](latent_graph_1))
    latent_graph_3 = add_graphs(latent_graph_2, net._processor_networks[2](latent_graph_2))
    latent_graph_4 = add_graphs(latent_graph_3, net._processor_networks[3](latent_graph_3))
    out_4 = net._processor_networks[4](latent_graph_4)
    latent_graph_5 = add_graphs(latent_graph_4, out_4)
    out = net._decode(latent_graph_5)

    loss = keras.losses.MeanSquaredError('sum_over_batch_size')(target, out)
grads = tape.gradient(loss, [net.trainable_variables, out, graph.nodes, graph.edges])
keras.optimizers.SGD(learning_rate=1e-6).apply_gradients(zip(grads[0], net.trainable_variables))
print(loss)

u.save_graph(u.f(SUITE, TEST, "exp_graph"), graph)
u.save_matrix(u.f(SUITE, TEST, "a"), latent_graph_0.nodes)
u.save_matrix(u.f(SUITE, TEST, "exp_grads"), grads[1])
u.save_matrix(u.f(SUITE, TEST, "exp_target"), target)
save_model(u.f(SUITE, TEST, "model"), net)
u.save_array(u.f(SUITE, TEST, "exp_loss"), np.array([loss], dtype=np.float32))
u.save_matrix(u.f(SUITE, TEST, "exp_grads_nodes"), grads[2])
u.save_matrix(u.f(SUITE, TEST, "exp_grads_edges"), grads[3])
