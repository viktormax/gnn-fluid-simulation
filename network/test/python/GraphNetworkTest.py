# %%
import graph_nets as gn
import tensorflow as tf
from graph_nets import modules
import sonnet as snt
import keras
import utils as u
import numpy as np

# %%
SUITE = "GraphNetworkTest"
ROLLOUT_SIZE = 5
MIN_VELS_COUNT = 4
RADIUS = 0.1

target = u.load_matrix(u.f(SUITE, "target"))
node_big_target = u.load_matrix(u.f(SUITE, "node_big_target"))
graph = u.load_graph(u.f(SUITE, "graph"))
network_independent = u.load_graph_network(u.f(SUITE, "network_independent"))
network1 = u.load_graph_network(u.f(SUITE, "network1"))
network2 = u.load_graph_network(u.f(SUITE, "network2"))

class MlpModel(snt.Module):
  def __init__(self, net):
    super(MlpModel, self).__init__()
    layers = net['layers']
    self.dnn1 = keras.layers.Dense(layers[0]['neuron_count'], 
      activation=keras.activations.relu, weights=[layers[0]['weights'], layers[0]['biases']])
    self.dnn2 = keras.layers.Dense(layers[1]['neuron_count'], 
      activation=keras.activations.relu, weights=[layers[1]['weights'], layers[1]['biases']])
    self.dnn3 = keras.layers.Dense(layers[2]['neuron_count'], 
      activation=keras.activations.linear, weights=[layers[2]['weights'], layers[2]['biases']])
    self.norm = keras.layers.LayerNormalization(epsilon=1e-5)

  def __call__(self, inputs):
    return self.norm(self.dnn3(self.dnn2(self.dnn1(inputs))))


class MLPGraphIndependent(snt.Module):
  def __init__(self, net, name="MLPGraphIndependent"):
    super(MLPGraphIndependent, self).__init__(name=name)
    def make_mlp_model_node():
      return MlpModel(net["node_net"])
    def make_mlp_model_edge():
      return MlpModel(net["edge_net"])

    self._network = modules.GraphIndependent(
        edge_model_fn=make_mlp_model_edge,
        node_model_fn=make_mlp_model_node)

  def __call__(self, inputs):
    return self._network(inputs)


class MLPGraphNetwork(snt.Module):
  def __init__(self, net, name="MLPGraphNetwork"):
    super(MLPGraphNetwork, self).__init__(name=name)
    def make_mlp_model_node():
      return MlpModel(net["node_net"])
    def make_mlp_model_edge():
      return MlpModel(net["edge_net"])
    def make_mlp_model_global():
      return lambda x: x

    self._network = modules.GraphNetwork(
        edge_block_opt={"use_globals": False},
        node_block_opt={"use_globals": False},
        edge_model_fn=make_mlp_model_edge,
        node_model_fn=make_mlp_model_node,
        global_model_fn=make_mlp_model_global)
  
  def __call__(self, inputs):
    return self._network(inputs)
  
input = gn.graphs.GraphsTuple(
    nodes=tf.concat(graph['nodes'], axis=-1),
    edges=tf.concat(graph['edges'], axis=-1),
    globals=tf.constant([[0.0]]),
    n_node=tf.constant([len(graph['nodes'])]),
    n_edge=tf.constant([len(graph['edges'])]),
    senders=np.array(graph['senders'], dtype=np.int32),
    receivers=np.array(graph['receivers'], dtype=np.int32),
)

# %%
TEST = "ForwardIndependentTest"

model = MLPGraphIndependent(network_independent)
out = model(input)

u.save_graph(u.f(SUITE, TEST, "exp_graph"), out)

# %%
TEST = "ForwardTest"

model = MLPGraphNetwork(network1)
out = model(input)

u.save_graph(u.f(SUITE, TEST, "exp_graph"), out)

# %%
TEST = "BackwardEndTest"
model = MLPGraphNetwork(network1)
with tf.GradientTape() as tape:
  tape.watch(input.nodes)
  tape.watch(input.edges)
  out = model(input)
  loss = keras.losses.MeanSquaredError('sum_over_batch_size')(node_big_target, out.nodes)

grad = tape.gradient(loss, [input.nodes, input.edges, out.nodes])
u.save_matrix(u.f(SUITE, TEST, "exp_node_grad"), grad[0])
u.save_matrix(u.f(SUITE, TEST, "exp_edge_grad"), grad[1])
u.save_matrix(u.f(SUITE, TEST, "node_grads"), grad[2])

# %%
TEST = "BackwardMidTest"
model1 = MLPGraphNetwork(network1)
model2 = MLPGraphNetwork(network2)
with tf.GradientTape() as tape:
  tape.watch(input.nodes)
  tape.watch(input.edges)
  out = model2(model1(input))
  loss = keras.losses.MeanSquaredError('sum_over_batch_size')(target, out.nodes)

grad = tape.gradient(loss, [input.nodes, input.edges, out.nodes])
u.save_matrix(u.f(SUITE, TEST, "exp_node_grad"), grad[0])
u.save_matrix(u.f(SUITE, TEST, "exp_edge_grad"), grad[1])
u.save_matrix(u.f(SUITE, TEST, "node_grads"), grad[2])
