# %%
from keras import layers
from keras import optimizers
from keras import losses
from tensorflow import GradientTape
import tensorflow as tf
import utils as u

# %%
SUITE = "BatchNormTest"
batch = tf.constant(u.load_matrix(u.f(SUITE, "batch")))
target = u.load_matrix(u.f(SUITE, "target"))

# %%
TEST = "ForwardTest"
layer = layers.BatchNormalization(epsilon=1e-5, momentum=0.9)
out = layer(batch, training=True) 

u.save_matrix(u.f(SUITE, TEST, "exp_out"), out)
u.save_array(u.f(SUITE, TEST, "exp_mean"), layer.moving_mean)
u.save_array(u.f(SUITE, TEST, "exp_var"), layer.moving_variance)

# %%
TEST = "BackwardTest"
layer = layers.BatchNormalization(epsilon=1e-5, momentum=0.9)

with GradientTape() as tape:
    tape.watch(batch)
    out = layer(batch, training=True)
    loss = losses.MeanSquaredError('sum_over_batch_size')(target, out)

grad = tape.gradient(loss, [layer.trainable_weights, batch])
optimizers.SGD(learning_rate=0.1).apply_gradients(zip(grad[0], layer.trainable_weights))

u.save_matrix(u.f(SUITE, TEST, "exp_grads"), grad[1])
u.save_array(u.f(SUITE, TEST, "exp_gamma"), layer.gamma)
u.save_array(u.f(SUITE, TEST, "exp_beta"), layer.beta)

# %%
TEST = "PredictTest"
layer = layers.BatchNormalization(epsilon=1e-5, momentum=0.9)

layer(batch, training=True)
layer(batch, training=True)
layer(batch, training=True)
layer(batch, training=True)
layer(batch, training=True)

out = layer(batch)

u.save_matrix(u.f(SUITE, TEST, "exp_out"), out)
u.save_array(u.f(SUITE, TEST, "exp_mean"), layer.moving_mean)
u.save_array(u.f(SUITE, TEST, "exp_var"), layer.moving_variance)
