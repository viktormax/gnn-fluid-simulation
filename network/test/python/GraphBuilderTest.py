# %%
import utils as u
import graph_nets as gn
import tensorflow as tf

#%%
SUITE = "GraphBuilderTest"
SEQUANCE_LENGTH = 6
RADIUS = 0.1

rollout = u.load_rollout(u.f(SUITE, "rollout"))
neighbours = []
with open(u.f(SUITE, "neighbours"), 'rb') as f:
    for i in range(rollout['steps']):
        neighbours.append([n[n != -1] for n in u.load_matrix(f)])

#%%
TEST = "BuildGraphInitialLoadTest"
graph = u.build_graph(rollout, neighbours, SEQUANCE_LENGTH, 0)
u.save_graph(u.f(SUITE, TEST, "exp_graph"), graph)

#%%
TEST = "BuildGraphTest"
graph = u.build_graph(rollout, neighbours, SEQUANCE_LENGTH, 4)
u.save_graph(u.f(SUITE, TEST, "exp_graph"), graph)

#%%
TEST = "BroadcastNodesToEdgesTest"

edges_to_collect = []
edges_to_collect.append(graph.edges)
edges_to_collect.append(gn.blocks.broadcast_receiver_nodes_to_edges(graph))
edges_to_collect.append(gn.blocks.broadcast_sender_nodes_to_edges(graph))
exp_edges = tf.concat(edges_to_collect, axis=-1)

u.save_matrix(u.f(SUITE, TEST, "exp_edges"), exp_edges)
u.save_graph(u.f(SUITE, TEST, "graph"), graph)

#%%
TEST = "BroadcastEdgesToNodesTest"

nodes_to_collect = []
received = gn.blocks.ReceivedEdgesToNodesAggregator(tf.math.unsorted_segment_sum)(graph)
nodes_to_collect.append(received)
nodes_to_collect.append(graph.nodes)
exp_nodes = tf.concat(nodes_to_collect, axis=-1)

u.save_matrix(u.f(SUITE, TEST, "exp_nodes"), exp_nodes)
u.save_graph(u.f(SUITE, TEST, "graph"), graph)

# %%
