#%%
from keras.optimizers import Adam
import numpy as np
import tensorflow as tf
import keras
import utils as u

# %%
SUITE = "AdamOptimizerTest"
op = Adam(learning_rate=1e-4, epsilon=1e-5)

# %%
TEST = "RunTest"
layer = keras.layers.Dense(128, activation='relu')
target = np.random.rand(30, 128)
input = np.random.rand(30, 128)

with tf.GradientTape() as tape:
    out = layer(input)
    loss = keras.losses.MeanSquaredError('sum_over_batch_size')(target, out)


grads = tape.gradient(loss, layer.trainable_weights)

u.save_matrix(u.f(SUITE, TEST, "weights"), layer.weights[0])
u.save_array(u.f(SUITE, TEST, "biases"), layer.weights[1])
u.save_matrix(u.f(SUITE, TEST, "weights_grads"), grads[0])
u.save_array(u.f(SUITE, TEST, "biases_grads"), grads[1])

print(op.get_slot(layer.weights[0], "m"))

op.apply_gradients(zip(grads, layer.trainable_weights))

u.save_matrix(u.f(SUITE, TEST, "exp_weights"), layer.weights[0])
u.save_array(u.f(SUITE, TEST, "exp_biases"), layer.weights[1])
