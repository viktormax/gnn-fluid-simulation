import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation
import utils as u

SUITE = "NetworkModelTest"
rollout = u.load_rollout("../tmp/NetworkModelTest-rollout.bin")

poss = rollout['positions']

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

def plot_positions(positions, ax):
    ax.scatter(positions[:, 0], positions[:, 2], positions[:, 1])

def animate(t):
    ax.clear()
    plot_positions(poss[t], ax)
    ax.set_xlim([1, 2.75])
    ax.set_ylim([1, 2.75])
    ax.set_zlim(0.25, 3)

ani = animation.FuncAnimation(fig, animate, frames=len(poss), interval=100)

# ani.save("animation.gif", writer=animation.PillowWriter(fps=30))

plt.show()