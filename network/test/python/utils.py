# %%
import ctypes
import struct
import numpy as np
from pathlib import Path
import tensorflow as tf
import os
import sys
from sklearn.neighbors import KDTree
import graph_nets as gn

# %%
def getDataPath():
    if (os.path.exists(sys.argv[1])):
        return sys.argv[1]
    else:
        return str(Path.cwd()) + "/../tmp/"

def f(*args):
    path = str(getDataPath()) + args[0]
    for p in args[1:]:
        path = path + "-" + p
    return path + ".bin"

# %%
def save_matrix(f, m):
    if isinstance(f, str):
        with open(f, 'wb') as f:
            save_matrix(f, m)
    else:
        f.write(ctypes.c_ulonglong(m.shape[0]))
        f.write(ctypes.c_ulonglong(m.shape[1]))
        f.write(m.numpy() if tf.is_tensor(m) else m)

def save_array(f, a):
    if isinstance(f, str):
        with open(f, 'wb') as f:
            save_array(f, a)
    else:    
        f.write(ctypes.c_ulonglong(a.shape[0]))
        f.write(a.numpy() if tf.is_tensor(a) else a)

def save_graph(f, graph):
    if isinstance(f, str):
        with open(f, 'wb') as f:
            save_graph(f, graph)
    else: 
        if isinstance(graph, dict):
            g = graph
        else:
            g = {}
            g["nodes"] = graph.nodes
            g["edges"] = graph.edges
            g["senders"] = graph.senders
            g["receivers"] = graph.receivers

        save_matrix(f, g["nodes"])
        save_matrix(f, g["edges"])
        save_array(f, g["senders"])
        save_array(f, g["receivers"])

# %%
def load_var(f, type):
    if isinstance(f, str):
        with open(f, 'rb') as f:
            return load_var(f, type)
    else:
        bytes = f.read(ctypes.sizeof(type))
        var = struct.unpack(type._type_, bytes)[0]
        return var

def load_array(f, type):
    if isinstance(f, str):
        with open(f, 'rb') as f:
            return load_array(f, type)
    else:
        bytes = f.read(ctypes.sizeof(ctypes.c_ulonglong))
        count = struct.unpack(ctypes.c_ulonglong._type_, bytes)[0]
        bytes = f.read(ctypes.sizeof(type) * count)
        arr = struct.unpack(str(count) + type._type_, bytes)
        return np.array(arr, dtype=np.dtype(type))
    
def load_bytes(f, count, type):
    bytes= f.read(ctypes.sizeof(type) * count)
    arr = struct.unpack(str(count) + type._type_, bytes)
    return np.array(arr, dtype=np.dtype(type))

def load_matrix(f):
    if isinstance(f, str):
        with open(f, 'rb') as f:
            return load_matrix(f)
    else:
        bytes = f.read(ctypes.sizeof(ctypes.c_ulonglong) * 2)
        shape = struct.unpack("2" + ctypes.c_ulonglong._type_, bytes)
        count = shape[0] * shape[1]
        bytes= f.read(ctypes.sizeof(ctypes.c_float) * count)
        arr = struct.unpack(str(count) + ctypes.c_float._type_, bytes)
        m = np.array(arr, dtype=np.dtype(ctypes.c_float))
        m.shape = shape
        return m

def load_plane(f):
    if isinstance(f, str):
        with open(f, 'rb') as f:
            return load_plane(f)
    else:
        p = {}
        p["normal"] = [load_var(f, ctypes.c_float), load_var(f, ctypes.c_float), load_var(f, ctypes.c_float)]
        p["d"] = load_var(f, ctypes.c_float)
        p["p1"] = [load_var(f, ctypes.c_float), load_var(f, ctypes.c_float), load_var(f, ctypes.c_float)]
        p["p2"] = [load_var(f, ctypes.c_float), load_var(f, ctypes.c_float), load_var(f, ctypes.c_float)]
        p["p3"] = [load_var(f, ctypes.c_float), load_var(f, ctypes.c_float), load_var(f, ctypes.c_float)]
        p["p4"] = [load_var(f, ctypes.c_float), load_var(f, ctypes.c_float), load_var(f, ctypes.c_float)]
        return p

def load_physics(f):
    if isinstance(f, str):
        with open(f, 'rb') as f:
            return load_physics(f)
    else:
        p = {}
        p["radius"] = load_var(f, ctypes.c_float)
        p["max_neighbors"] = load_var(f, ctypes.c_int)
        p["max_grid_dims"] = load_var(f, ctypes.c_int)
        p["gravity"] = {
            "x": load_var(f, ctypes.c_float),
            "y": load_var(f, ctypes.c_float),
            "z": load_var(f, ctypes.c_float)
        }
        p["rest_distance_ratio"] = load_var(f, ctypes.c_float)
        p["max_speed"] = load_var(f, ctypes.c_float)
        p["cohesion"] = load_var(f, ctypes.c_float)
        p["surface_tension"] = load_var(f, ctypes.c_float)
        p["relaxation_factor"] = load_var(f, ctypes.c_float)
        p["viscosity"] = load_var(f, ctypes.c_float)
        p["damp"] = load_var(f, ctypes.c_float)
        load_var(f, ctypes.c_float)  # Reomve structure padding
        return p

def load_rollout(path):
    rollout = []
    with open(path, 'rb') as f:
        rollout = {}
        rollout['particles'] = load_var(f, ctypes.c_ulonglong)
        rollout['statics'] = load_var(f, ctypes.c_ulonglong)
        rollout['steps'] = load_var(f, ctypes.c_ulonglong)
        rollout['boundaries'] = load_var(f, ctypes.c_ulonglong)
        rollout['physics'] = load_physics(f)
        rollout['planes'] = []
        for i in range(rollout['boundaries']):
            if i == 0:
                load_var(f, ctypes.c_ulonglong)
            rollout['planes'].append(load_plane(f))
        rollout['static_positions'] = []
        if rollout['statics'] > 0:
            count = load_var(f, ctypes.c_ulonglong)
            arr = load_bytes(f, count * 3,  ctypes.c_float)
            arr.shape = (count, 3)
            rollout['static_positions'] = arr
        rollout['positions'] = []
        for i in range(rollout['steps']):
            count = load_var(f, ctypes.c_ulonglong)
            arr = load_bytes(f, count * 3,  ctypes.c_float)
            arr.shape = (count, 3)
            rollout['positions'].append(arr)
    return rollout
 
def load_graph(f):
    if isinstance(f, str):
        with open(f, 'rb') as f:
            return load_graph(f)
    else:
        graph = {}
        graph['nodes'] = load_matrix(f)
        graph['edges'] = load_matrix(f)
        graph['senders'] = load_array(f, ctypes.c_uint)
        graph['receivers'] = load_array(f, ctypes.c_uint)
        return graph

def load_layer(f):
    if isinstance(f, str):
        with open(f, 'rb') as f:
            return load_layer(f)
    else:
        l = {}
        l["type"] = load_var(f, ctypes.c_int)
        if l["type"] == 0:
            l["activation"] = load_var(f, ctypes.c_int)
            l["neuron_count"] = load_var(f, ctypes.c_ulonglong)
            l["weights"] = load_matrix(f)
            l["biases"] = load_array(f, ctypes.c_float)
        if l["type"] == 1:
            l["gamma"] = load_array(f, ctypes.c_float)
            l["beta"] = load_array(f, ctypes.c_float)
        return l

def load_network(f):
    if isinstance(f, str):
        with open(f, 'rb') as f:
            return load_network(f)
    else:
        net = {}
        net["cost"] = load_var(f, ctypes.c_double)
        net["epoch"] = load_var(f, ctypes.c_ulonglong)
        net["layer_count"] = load_var(f, ctypes.c_ulonglong)
        net["layers"] = []
        for i in range(net["layer_count"]):
            net["layers"].append(load_layer(f))
        return net

def load_graph_network(f):
    if isinstance(f, str):
        with open(f, 'rb') as f:
            return load_graph_network(f)
    else:
        gn = {}
        gn["independent"] = load_var(f, ctypes.c_bool)
        gn["node_net"] = load_network(f)
        gn["edge_net"] = load_network(f)
        return gn
    
def build_graph(rollout, neighbours, sequance_lenght, offset):
    neighbours = neighbours[offset + sequance_lenght - 1] if neighbours != None else None
    vels = []
    locs = rollout['positions'][offset:offset + sequance_lenght]
    for i in reversed(range(len(locs) - 1)):
        vels.append(np.subtract(locs[i + 1], locs[i], dtype=np.float32))

    boundary_dist = []
    for p in locs[-1]:
        dis = []
        for i in range(rollout['boundaries']):
            plane = rollout['planes'][i]
            dis.append(np.dot(p, plane["normal"]) - plane["d"])
        boundary_dist.append(dis)

    nodes = np.concatenate((locs[-1], boundary_dist, *vels), axis=1, dtype=np.float32)
    sort_idx = np.array([np.argsort(x) for x in neighbours], dtype=object) if neighbours != None else None

    tree = KDTree(locs[-1])

    if neighbours == None:
        senders = tree.query_radius(locs[-1], r=rollout['physics']['radius'])
        # Remove self edges.
        for i in range(len(senders)):
            senders[i] = senders[i][senders[i] != i]

        for i in range(len(senders)):
            a = np.zeros(len(senders[i]), dtype=np.int32)
            for j in range(len(senders[i])):
                a[sort_idx[i][j]] = senders[i][j]
            senders[i] = a
    else:
        senders = [n.astype(np.int32) for n in neighbours]

    receivers = np.repeat(range(rollout['particles']), [len(a) for a in senders])
    senders = np.concatenate(senders, axis=0)

    sl = np.take(locs[-1], senders, axis=0)
    rl = np.take(locs[-1], receivers, axis=0)
    d = (rl - sl) / rollout['physics']['radius']
    r = np.linalg.norm(d, axis=1, keepdims=True)
    edges = np.concatenate((d, r), axis=1, dtype=np.float32)

    return gn.graphs.GraphsTuple(
        nodes=tf.concat(nodes, axis=-1),
        edges=tf.concat(edges, axis=-1),
        globals=tf.constant([[0.0]]),
        n_node=tf.constant([len(nodes)]),
        n_edge=tf.constant([len(edges)]),
        senders=np.array(senders, dtype=np.int32),
        receivers=np.array(receivers, dtype=np.int32),
    )