#include "GraphNetwork.hpp"

#include <gtest/gtest.h>

#include "DataLoader.hpp"
#include "NetworkModel.hpp"
#include "sph.h"
#include "utils.h"

class NetworkModelTest : public testing::Test {
   public:
    static physics_s p;

    static void rollout2400(physics_s& physics, array_h<point>& particles, array_h<float3>& boundaryPoints, array_h<plane_s>& boundaries) {
        physics.gravity = { 0.0, -9.8, 0.0 };
        physics.radius = 0.1;
        physics.cohesion = 0.025;
        physics.surfaceTension = 1;
        physics.viscosity = 5;
        physics.restDistanceRatio = 0.55;
        physics.maxGrixDims = 128;
        physics.maxNeighbours = 300;
        physics.maxSpeed = 5.0;
        physics.relaxationFactor = 1.0;
        physics.damp = 30;

        particles = generateCubePoints(make_point(1, 1, 1), make_point(0.3, 1, 1), 0.05);
        boundaries = generateBoundariesStatic(make_point(0.9, 0.1, 0.9), make_point(2, 2, 2));
        boundaryPoints = generateBoundaryPoints(boundaries, physics.radius / 5);
    }

    static void SetUpTestSuite() {
        Fluid fluid;
        Solver solver;
        GraphBuilder gb;
        array_h<point> particles, boundaryPoints;
        array_h<plane_s> boundaries;
        Rollout rollout;
        Neighbours neighbours;

        rollout2400(p, particles, boundaryPoints, boundaries);

        fluid.setParticles(particles, boundaryPoints);
        solver.setBoundaries(boundaries);
        rollout.setBoundaries(boundaries);

        CREATE_TMP_DIR();

        std::ofstream file(TMP_DATA_DIR "NetworkModelTest-neighbours.bin", std::ios::out | std::ios::binary);
        
        for (int i = 0; i < 110; i++) {
            rollout.pushLocs(fluid.locs);
            neighbours.compute(fluid.locs, fluid.locs, p);
            neighbours.data.toStream(file);
            fluid = solver.run(fluid, p);
        }

        file.close();

        SAVE_DATA(NetworkModelTest, rollout);

        RUN_PYTHON(NetworkModelTest);
    }
};

physics_s NetworkModelTest::p;

TEST_F(NetworkModelTest, ForwardTest) {
    NetworkModel model;
    Graph graph;
    matrix_h exp_out;

    LOAD_DATA(NetworkModelTest, ForwardTest, graph);
    LOAD_DATA(NetworkModelTest, ForwardTest, exp_out);
    LOAD_DATA(NetworkModelTest, ForwardTest, model);

    auto out = model.forward(graph);

    COMPARE_MAT(exp_out, out);
}

TEST_F(NetworkModelTest, BackwardTest) {
    NetworkModel model;
    Graph graph;
    matrix_h target, exp_grads, exp_grads_nodes, exp_grads_edges;
    array_h<float> exp_loss;

    LOAD_DATA(NetworkModelTest, BackwardTest, graph);
    LOAD_DATA(NetworkModelTest, BackwardTest, exp_grads);
    LOAD_DATA(NetworkModelTest, BackwardTest, exp_grads_nodes);
    LOAD_DATA(NetworkModelTest, BackwardTest, exp_grads_edges);
    LOAD_DATA(NetworkModelTest, BackwardTest, target);
    LOAD_DATA(NetworkModelTest, BackwardTest, exp_loss);
    LOAD_DATA(NetworkModelTest, BackwardTest, model);

    auto out = model.forward(graph);
    auto [grads, cost] = Network::MSE(target, out);
    auto [nodeGrads, edgeGrads] = model.backward(graph, out, target, 0.1);

    EXPECT_NEAR(cost / exp_loss[0], 1, 0.1);
    COMPARE_MAT(grads, exp_grads);
    COMPARE_MAT(*nodeGrads, exp_grads_nodes);
    COMPARE_MAT(*edgeGrads, exp_grads_edges);
}


TEST_F(NetworkModelTest, FullStepTest) {
    NetworkModel model;
    GraphBuilder gb;
    Graph exp_graph;
    matrix_h exp_target, exp_grads, exp_grads_nodes, exp_grads_edges;
    array_h<float> exp_loss;
    Rollout rollout;

    LOAD_DATA_S(NetworkModelTest, rollout);
    LOAD_DATA(NetworkModelTest, FullStepTest, model);
    LOAD_DATA(NetworkModelTest, FullStepTest, exp_target);
    LOAD_DATA(NetworkModelTest, FullStepTest, exp_grads);
    LOAD_DATA(NetworkModelTest, FullStepTest, exp_graph);
    LOAD_DATA(NetworkModelTest, FullStepTest, exp_loss);
    LOAD_DATA(NetworkModelTest, FullStepTest, exp_grads_nodes);
    LOAD_DATA(NetworkModelTest, FullStepTest, exp_grads_edges);

    int sequence_length = 6;
    int step = 47 - sequence_length;
    gb.setBoundaries(rollout.boundaries);
    for (int i = 0; i < sequence_length; i++) {
        gb.pushLocations(rollout.getStep(step++));
    }

    auto graph = gb.buildGraph(rollout.physics);
    auto target = toMatrix(rollout.getStep(step) - gb.locs);

    ASSERT_TRUE(target == exp_target);
    ASSERT_TRUE(*graph.receivers == *exp_graph.receivers);
    ASSERT_TRUE(*graph.senders == *exp_graph.senders);
    COMPARE_MAT(graph.nodes, exp_graph.nodes);
    COMPARE_MAT(graph.edges, exp_graph.edges);
    
    auto out = model.forward(graph);
    auto [grads, cost] = Network::MSE(target, out);
    auto [nodeGrads, edgeGrads] = model.backward(graph, out, target, 1e-6);

    exp_grads_nodes.print(10, 27);
    nodeGrads->print(10, 27);
    (nodeGrads->pow(-1) * exp_grads_nodes).print(10, 27);

    EXPECT_NEAR(cost / exp_loss[0], 1, 0.1);
    COMPARE_MAT(grads, exp_grads);
    COMPARE_MAT(*nodeGrads, exp_grads_nodes);
    COMPARE_MAT(*edgeGrads, exp_grads_edges);
}
