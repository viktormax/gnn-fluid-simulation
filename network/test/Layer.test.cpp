#include "Layer.hpp"

#include <gtest/gtest.h>

#include "Network.hpp"
#include "utils.h"

TEST(LayerTest, ConstructorTest) {
    // Test default constructor
    Layer layer1;
    EXPECT_EQ(layer1.type(), ModuleType::Layer);
    EXPECT_EQ(layer1.neuronCount, 0);
    EXPECT_EQ(layer1.activationType, ActivationType::Linear);

    // Test constructor with parameters
    Layer layer2(5, ActivationType::ReLU);
    EXPECT_EQ(layer2.type(), ModuleType::Layer);
    EXPECT_EQ(layer2.neuronCount, 5);
    EXPECT_EQ(layer2.activationType, ActivationType::ReLU);
}

TEST(LayerTest, CopyConstructorTest) {
    Layer layer1(5, ActivationType::ReLU);
    layer1.forward(matrix_d({{1, 2, 3}, {4, 5, 6}}));
    Layer layer2 = layer1;
    EXPECT_EQ(layer2.type(), ModuleType::Layer);
    EXPECT_EQ(layer2.neuronCount, 5);
    EXPECT_EQ(layer2.activationType, ActivationType::ReLU);
    EXPECT_TRUE(layer2.weights == layer1.weights);
    EXPECT_TRUE(layer2.biases == layer1.biases);
}

// From ipynbs/keras-layer.ipynb
TEST(LayerTest, ForwardSigmoidTest) {
    matrix_d inputs({{0.1, 0.2, 0.3, 0.4}, {0.5, 0.6, 0.7, 0.8}});
    matrix_d weights({{-0.1, -0.2, -0.3}, {-0.4, -0.5, -0.6}, {-0.7, 0.8, 0.9}, {-1.0, 1.1, 1.2}});
    array_d<float> biases({-0.1, 0.2, 0.3});
    Layer layer(3, ActivationType::Sigmoid);
    layer.setWeights(weights, biases);

    matrix_h expected({{0.3100255, 0.68135375, 0.71094954},
                       {0.15709546, 0.775564, 0.79899096}});

    matrix_h outputs = layer.forward(inputs);

    for (int i = 0; i < outputs.rows; i++) {
        for (int j = 0; j < outputs.cols; j++) {
            EXPECT_NEAR(outputs(i, j), expected(i, j), 1e-6);
        }
    }
}

// From ipynbs/keras-layer.ipynb
TEST(LayerTest, ForwardReLUTest) {
    matrix_d inputs({{0.1, 0.2, 0.3, 0.4}, {0.5, 0.6, 0.7, 0.8}});
    matrix_d weights({{-0.1, -0.2, -0.3}, {-0.4, -0.5, -0.6}, {-0.7, 0.8, 0.9}, {-1.0, 1.1, 1.2}});
    array_d<float> biases({-0.1, 0.2, 0.3});
    Layer layer(3, ActivationType::ReLU);
    layer.setWeights(weights, biases);

    matrix_h expected({{0., 0.76, 0.90000004},
                       {0., 1.2400001, 1.3799999}});

    matrix_h outputs = layer.forward(inputs);

    for (int i = 0; i < expected.rows; i++) {
        for (int j = 0; j < expected.cols; j++) {
            EXPECT_NEAR(outputs(i, j), expected(i, j), 1e-6);
        }
    }
}

// From ipynbs/keras-layer.ipynb
TEST(LayerTest, BackwardReLUTest) {
    matrix_d inputs({{0.1, 0.2, 0.3, 0.4}, {0.5, 0.6, 0.7, 0.8}});
    matrix_d weights({{-0.1, -0.2, -0.3, 0.1, -0.5},
                      {-0.4, -0.5, -0.6, 0.1, -0.5},
                      {-0.7, 0.8, 0.9, 0.1, -0.5},
                      {-1.0, 1.1, 1.2, 0.1, -0.5}});
    array_d<float> biases({-0.1, 0.2, 0.3, 0, 0.1});
    matrix_d targets({{-3.0, -2.0, -1.0, 1, 2}, {1.0, 2.0, 3.0, 3, 1}});
    Layer layer(5, ActivationType::ReLU);
    layer.setWeights(weights, biases);

    matrix_h expected_grads({{0.6, 0.552, 0.38000003, -0.17999999, -0.4},
                             {-0.2, -0.15199998, -0.32400003, -0.54800004, -0.2}});
    matrix_h expected_input_grads({{-0.24240002, -0.522, 0.7656, 1.0452001},
                                   {0.07280001, 0.21560001, -0.46800002, -0.6108001}});
    matrix_h expected_weights({{-0.1, -0.19792001, -0.2876, 0.12920001, -0.5},
                               {-0.4, -0.50192, -0.58816004, 0.13648, -0.5},
                               {-0.7, 0.79408, 0.91128, 0.14376, -0.5},
                               {-1., 1.09008, 1.2107201, 0.15104, -0.5}});
    array_h<float> expected_biases({-0.1, 0.16, 0.2944, 0.0728, 0.1});

    matrix_h outputs = layer.forward(inputs);
    auto [grads, cost] = Network::MSE(targets, outputs);

    EXPECT_NEAR(cost, 3.7747202, 1e-6);
    COMPARE_MAT(grads, expected_grads);

    Layer::useAdamOptimizer = false;
    auto input_grads = layer.backward(grads, 0.1);
    Layer::useAdamOptimizer = true;

    COMPARE_MAT(layer.weights, expected_weights);
    COMPARE_ARR(layer.biases, expected_biases);
    COMPARE_MAT(input_grads, expected_input_grads);
}

// From ipynbs/keras-layer.ipynb
TEST(LayerTest, BackwardSigmoidTest) {
    matrix_d inputs({{0.1, 0.2, 0.3, 0.4}, {0.5, 0.6, 0.7, 0.8}});
    matrix_d weights({{-0.1, -0.2, -0.3, 0.1, -0.5},
                      {-0.4, -0.5, -0.6, 0.1, -0.5},
                      {-0.7, 0.8, 0.9, 0.1, -0.5},
                      {-1.0, 1.1, 1.2, 0.1, -0.5}});
    array_d<float> biases({-0.1, 0.2, 0.3, 0, 0.1});
    matrix_d targets({{-3.0, -2.0, -1.0, 1, 2}, {1.0, 2.0, 3.0, 3, 1}});
    Layer layer(5, ActivationType::Sigmoid);
    layer.setWeights(weights, biases);

    matrix_h expected_grads({{0.6620051, 0.5362708, 0.3421899, -0.09500416, -0.31973752},
                             {-0.16858092, -0.24488722, -0.44020182, -0.48707277, -0.15370496}});
    matrix_h expected_input_grads({{-0.02250199, -0.12100989, 0.09334675, 0.10688906},
                                   {0.03366533, 0.07435955, -0.08040512, -0.10770562}});
    matrix_h expected_weights({{-0.10003, -0.19990331, -0.29971683, 0.10062236, -0.49978647},
                               {-0.4001493, -0.4999771, -0.5997165, 0.10076579, -0.4996823},
                               {-0.70026857, 0.7999491, 0.90028393, 0.10090921, -0.49957815},
                               {-1.0003879, 1.0998753, 1.2002844, 0.10105263, -0.49947396}});
    array_h<float> expected_biases({-0.10119287, 0.19926196, 0.3000038, 0.00143425, 0.10104164});

    matrix_h outputs = layer.forward(inputs);
    auto [grads, cost] = Network::MSE(targets, outputs);

    matrix_h h = grads;
    EXPECT_NEAR(cost, 3.743052, 1e-6);
    COMPARE_MAT(grads, expected_grads);

    Layer::useAdamOptimizer = false;
    auto input_grads = layer.backward(grads, 0.01);
    Layer::useAdamOptimizer = true;

    COMPARE_MAT(layer.weights, expected_weights);
    COMPARE_ARR(layer.biases, expected_biases);
    COMPARE_MAT(input_grads, expected_input_grads);
}

TEST(LayerTest, PredictTest) {
    Layer layer(5, ActivationType::Sigmoid);
    matrix_d inputs({{0.1, 0.2, 0.3, 0.4}, {0.5, 0.6, 0.7, 0.8}});

    matrix_d outputs1 = layer.forward(inputs);
    matrix_d outputs2 = layer.predict(inputs);

    EXPECT_TRUE(outputs1 == outputs2);
}
