#include "GraphBuilder.hpp"

#include <gtest/gtest.h>

#include "DataLoader.hpp"
#include "sph.h"
#include "utils.h"

class GraphBuilderTest : public testing::Test {
   public:
    static physics_s p;

    static void SetUpTestSuite() {
        Fluid fluid;
        Solver solver;

        p.cohesion = 0.025;
        p.damp = 20.0;
        p.gravity = {0.0, 0.0, 0.0};
        p.maxGrixDims = 128;
        p.maxNeighbours = 50;
        p.maxSpeed = 5.0;
        p.radius = 0.1;
        p.relaxationFactor = 1.0;
        p.restDistanceRatio = 0.55;
        p.surfaceTension = 1.0;
        p.viscosity = 5.0;

        auto locs = generateCubePoints(make_point(1, 1, 1), make_point(0.2, 0.2, 0.2), 0.05);
        auto boundaries = generateBoundariesStatic(make_point(0, 0, 0), make_point(4, 2.6, 3.2));

        fluid.setParticles(locs, array_d<float3>());
        solver.setBoundaries(boundaries);

        CREATE_TMP_DIR();

        std::ofstream file(TMP_DATA_DIR "GraphBuilderTest-neighbours.bin", std::ios::out | std::ios::binary);
        
        Rollout rollout;
        Neighbours neighbours;
        rollout.setBoundaries(boundaries);
        rollout.pushLocs(locs);
        neighbours.compute(locs, locs, p);
        neighbours.data.toStream(file);
        for (int i = 0; i < 10; i++) {
            fluid = solver.run(fluid, p);
            rollout.pushLocs(fluid.locs);
            neighbours.compute(fluid.locs, fluid.locs, p);
            neighbours.data.toStream(file);
        }

        file.close();

        SAVE_DATA(GraphBuilderTest, rollout);

        RUN_PYTHON(GraphBuilderTest);
    }
};

physics_s GraphBuilderTest::p;

TEST_F(GraphBuilderTest, BuildGraphInitialLoadTest) {
    GraphBuilder gb;
    matrix_h locs;
    Graph exp_graph;

    LOAD_DATA(GraphBuilderTest, BuildGraphInitialLoadTest, exp_graph);

    Rollout rollout;
    LOAD_DATA_S(GraphBuilderTest, rollout);
    gb.initialLoad(rollout);

    gb.setBoundaries(rollout.boundaries);
    Graph graph = gb.buildGraph(p);

    EXPECT_TRUE(*graph.receivers == *exp_graph.receivers);
    EXPECT_TRUE(*graph.senders == *exp_graph.senders);
    EXPECT_TRUE(graph.nodes == exp_graph.nodes);
    COMPARE_MAT(graph.edges, exp_graph.edges);
}

TEST_F(GraphBuilderTest, BuildGraphTest) {
    GraphBuilder gb;
    matrix_h locs;
    Graph exp_graph;

    LOAD_DATA(GraphBuilderTest, BuildGraphTest, exp_graph);

    Rollout rollout;
    LOAD_DATA_S(GraphBuilderTest, rollout);
    for (int i = 4; i < 4 + 6; i++) {
        gb.pushLocations(rollout.getStep(i));
    }

    gb.setBoundaries(rollout.boundaries);
    Graph graph = gb.buildGraph(p);

    EXPECT_TRUE(*graph.receivers == *exp_graph.receivers);
    EXPECT_TRUE(*graph.senders == *exp_graph.senders);
    COMPARE_MAT(graph.nodes, exp_graph.nodes);
    COMPARE_MAT(graph.edges, exp_graph.edges);
}

TEST_F(GraphBuilderTest, BroadcastNodesToEdgesTest) {
    Graph graph;
    matrix_d exp_edges;

    LOAD_DATA(GraphBuilderTest, BroadcastNodesToEdgesTest, graph);
    LOAD_DATA(GraphBuilderTest, BroadcastNodesToEdgesTest, exp_edges);

    auto edges = graph.broadcastNodesToEdges();

    EXPECT_TRUE(edges == exp_edges);
}

TEST_F(GraphBuilderTest, BroadcastEdgesToNodesTest) {
    Graph graph;
    matrix_d exp_nodes;

    LOAD_DATA(GraphBuilderTest, BroadcastNodesToEdgesTest, graph);
    LOAD_DATA(GraphBuilderTest, BroadcastEdgesToNodesTest, exp_nodes);

    auto nodes = graph.broadcastEdgesToNodes(graph.edges);

    COMPARE_MAT(nodes, exp_nodes);
}
