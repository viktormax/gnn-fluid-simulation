#include "GraphNetwork.hpp"

#include <gtest/gtest.h>

#include "DataLoader.hpp"
#include "sph.h"
#include "utils.h"
#include "Layer.hpp"

class GraphNetworkTest : public testing::Test {
   public:
    static physics_s p;

    static void SetUpTestSuite() {
        Fluid fluid;
        Solver solver;
        GraphBuilder gb;
        GraphNetwork network_independent, network1, network2;

        p.cohesion = 0.025;
        p.damp = 20.0;
        p.gravity = {0.0, 0.0, 0.0};
        p.maxGrixDims = 128;
        p.maxNeighbours = 50;
        p.maxSpeed = 5.0;
        p.radius = 0.1;
        p.relaxationFactor = 1.0;
        p.restDistanceRatio = 0.55;
        p.surfaceTension = 1.0;
        p.viscosity = 5.0;

        auto locs = generateCubePoints(make_point(1, 1, 1), make_point(0.2, 0.2, 0.2), 0.05);
        fluid.setParticles(locs, array_d<float3>());

        gb.pushLocations(fluid.locs);
        for (int i = 0; i < 4; i++) {
            fluid = solver.run(fluid, p);
            gb.pushLocations(fluid.locs);
        }
        auto target = toMatrix(fluid.vels);

        Graph graph = gb.buildGraph(p);

        network_independent.init(2, 128, 128, ActivationType::ReLU, true);
        network_independent.forward(graph);

        network1.init(2, 128, 128, ActivationType::ReLU);
        network2.init(2, 128, 3, ActivationType::ReLU);
        network2.forward(network1.forward(graph));

        RANDOM_MAT(64, 128, node_big_target);

        CREATE_TMP_DIR();

        SAVE_DATA(GraphNetworkTest, node_big_target);
        SAVE_DATA(GraphNetworkTest, target);
        SAVE_DATA(GraphNetworkTest, graph);
        SAVE_DATA(GraphNetworkTest, network1);
        SAVE_DATA(GraphNetworkTest, network2);
        SAVE_DATA(GraphNetworkTest, network_independent);

        RUN_PYTHON(GraphNetworkTest);
    }
};

physics_s GraphNetworkTest::p;

TEST_F(GraphNetworkTest, ForwardIndependentTest) {
    Graph graph, exp_graph;
    GraphNetwork network_independent;

    LOAD_DATA_S(GraphNetworkTest, network_independent);
    LOAD_DATA_S(GraphNetworkTest, graph);
    LOAD_DATA(GraphNetworkTest, ForwardIndependentTest, exp_graph);

    Graph out = network_independent.forward(graph);

    COMPARE_MAT(out.nodes, exp_graph.nodes);
    COMPARE_MAT(out.edges, exp_graph.edges);
    EXPECT_TRUE(*out.senders == *exp_graph.senders);
    EXPECT_TRUE(*out.receivers == *exp_graph.receivers);
}

TEST_F(GraphNetworkTest, ForwardTest) {
    Graph graph, exp_graph;
    GraphNetwork network1;

    LOAD_DATA_S(GraphNetworkTest, network1);
    LOAD_DATA_S(GraphNetworkTest, graph);
    LOAD_DATA(GraphNetworkTest, ForwardTest, exp_graph);

    Graph out = network1.forward(graph);

    COMPARE_MAT(out.nodes, exp_graph.nodes);
    COMPARE_MAT(out.edges, exp_graph.edges);
    EXPECT_TRUE(*out.senders == *exp_graph.senders);
    EXPECT_TRUE(*out.receivers == *exp_graph.receivers);
}

TEST_F(GraphNetworkTest, BackwardEndTest) {
    Graph graph;
    GraphNetwork network1;
    matrix_d node_grads, exp_node_grad, exp_edge_grad;

    LOAD_DATA_S(GraphNetworkTest, network1);
    LOAD_DATA_S(GraphNetworkTest, graph);
    LOAD_DATA(GraphNetworkTest, BackwardEndTest, node_grads);
    LOAD_DATA(GraphNetworkTest, BackwardEndTest, exp_node_grad);
    LOAD_DATA(GraphNetworkTest, BackwardEndTest, exp_edge_grad);

    GraphGrads gradsTuple(std::optional(std::move(node_grads)), std::nullopt);
    network1.forward(graph);
    auto [nodeGrad, edgeGrad] = network1.backward(graph, gradsTuple, 0.1);

    COMPARE_MAT(*nodeGrad, exp_node_grad);
    COMPARE_MAT(*edgeGrad, exp_edge_grad);
}

TEST_F(GraphNetworkTest, BackwardMidTest) {
    Graph graph;
    GraphNetwork network1, network2;
    matrix_d node_grads, exp_node_grad, exp_edge_grad;

    LOAD_DATA_S(GraphNetworkTest, network1);
    LOAD_DATA_S(GraphNetworkTest, network2);
    LOAD_DATA_S(GraphNetworkTest, graph);
    LOAD_DATA(GraphNetworkTest, BackwardMidTest, node_grads);
    LOAD_DATA(GraphNetworkTest, BackwardMidTest, exp_node_grad);
    LOAD_DATA(GraphNetworkTest, BackwardMidTest, exp_edge_grad);

    network2.forward(network1.forward(graph));
    GraphGrads gradsTuple(std::optional(std::move(node_grads)), std::nullopt);
    auto [nodeGrad, edgeGrad] = network1.backward(graph, network2.backward(graph, gradsTuple, 0.1), 0.1);

    COMPARE_MAT(*nodeGrad, exp_node_grad);
    COMPARE_MAT(*edgeGrad, exp_edge_grad);
}
