#pragma once

#include <curand.h>

#include <filesystem>
#include <fstream>

#include "core.h"

#define RANDOM_ARR(count, var)                                   \
    array_d<float> var(count);                                   \
    do {                                                         \
        curandGenerator_t prng;                                  \
        curandCreateGenerator(&prng, CURAND_RNG_PSEUDO_DEFAULT); \
        curandSetPseudoRandomGeneratorSeed(prng, clock());       \
        curandGenerateUniform(prng, var(), var.elements);        \
        curandDestroyGenerator(prng);                            \
        var = var - 0.5;                                         \
    } while (0)

#define RANDOM_MAT(rows, cols, var)                              \
    matrix_d var(rows, cols);                                    \
    do {                                                         \
        curandGenerator_t prng;                                  \
        curandCreateGenerator(&prng, CURAND_RNG_PSEUDO_DEFAULT); \
        curandSetPseudoRandomGeneratorSeed(prng, clock());       \
        curandGenerateUniform(prng, var(), var.elements);        \
        curandDestroyGenerator(prng);                            \
        var = var - 0.5;                                         \
    } while (0)

#define SAVE_DATA(suite, var)                                                                      \
    do {                                                                                           \
        std::ofstream file(TMP_DATA_DIR #suite "-" #var ".bin", std::ios::out | std::ios::binary); \
        var.toStream(file);                                                                        \
    } while (0)

#define RUN_PYTHON(suite)                                                 \
    do {                                                                  \
        std::system("python " PYTHON_SRC_DIR #suite ".py " TMP_DATA_DIR); \
    } while (0)

#define CREATE_TMP_DIR()                                   \
    do {                                                   \
        std::filesystem::create_directories(TMP_DATA_DIR); \
    } while (0)

#define LOAD_DATA(suite, test, var)                                                                       \
    do {                                                                                                  \
        std::ifstream ss(TMP_DATA_DIR #suite "-" #test "-" #var ".bin", std::ios::in | std::ios::binary); \
        var.fromStream(ss);                                                                               \
    } while (0)

#define LOAD_DATA_S(suite, var)                                                                 \
    do {                                                                                        \
        std::ifstream ss(TMP_DATA_DIR #suite "-" #var ".bin", std::ios::in | std::ios::binary); \
        var.fromStream(ss);                                                                     \
    } while (0)

#define COMPARE_MAT(act0, exp0)                                          \
    do {                                                                 \
        matrix_h act = act0;                                             \
        matrix_h exp = exp0;                                             \
        ASSERT_EQ(act.rows, exp.rows);                                   \
        ASSERT_EQ(act.cols, exp.cols);                                   \
        for (int i = 0; i < exp.rows; i++) {                             \
            for (int j = 0; j < exp.cols; j++) {                         \
                if (act(i, j) == exp(i, j) || exp(i, j) < 1e-7) continue;\
                ASSERT_NEAR(act(i, j) / exp(i, j), 1, 0.1)               \
                    << "i = " << i << " j = " << j                       \
                    << " act = " << act(i, j) << " exp = " << exp(i, j); \
            }                                                            \
        }                                                                \
    } while (0)

#define COMPARE_ARR(act0, exp0)                   \
    do {                                          \
        array_h act = act0;                       \
        array_h exp = exp0;                       \
        ASSERT_EQ(act.count, exp.count);          \
        for (int i = 0; i < exp.count; i++) {     \
            if (act[i] == exp[i]) continue;       \
            ASSERT_NEAR(act[i] / exp[i], 1, 0.1); \
        }                                         \
    } while (0)