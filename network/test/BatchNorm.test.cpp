#include "BatchNorm.hpp"

#include <gtest/gtest.h>

#include "Network.hpp"
#include "utils.h"

class BatchNormTest : public testing::Test {
   public:
    static void SetUpTestSuite() {
        RANDOM_MAT(6, 4, batch);
        RANDOM_MAT(6, 4, target);

        CREATE_TMP_DIR();
        SAVE_DATA(BatchNormTest, batch);
        SAVE_DATA(BatchNormTest, target);
        RUN_PYTHON(BatchNormTest);
    }
};

TEST_F(BatchNormTest, ConstructorTest) {
    BatchNorm layer;
    EXPECT_EQ(layer.type(), ModuleType::BatchNorm);
}

TEST_F(BatchNormTest, ForwardTest) {
    BatchNorm layer(0.90);
    matrix_h exp_out, batch;
    array_h<float> exp_mean, exp_var;

    LOAD_DATA_S(BatchNormTest, batch);
    LOAD_DATA(BatchNormTest, ForwardTest, exp_out);
    LOAD_DATA(BatchNormTest, ForwardTest, exp_mean);
    LOAD_DATA(BatchNormTest, ForwardTest, exp_var);

    auto out = layer.forward(batch);

    COMPARE_MAT(out, exp_out);
    COMPARE_ARR(layer.moving_mean, exp_mean);
    COMPARE_ARR(layer.moving_var, exp_var);

    EXPECT_EQ(layer.gamma.count, layer.beta.count);
    array_h g(layer.gamma), b(layer.beta);
    for (int i = 0; i < g.count; i++) {
        EXPECT_EQ(g[i], 1);
        EXPECT_EQ(b[i], 0);
    }
}

TEST_F(BatchNormTest, BackwardTest) {
    BatchNorm layer(0.90);
    matrix_h exp_grads, batch, target;
    array_h<float> exp_beta, exp_gamma;

    LOAD_DATA_S(BatchNormTest, batch);
    LOAD_DATA_S(BatchNormTest, target);
    LOAD_DATA(BatchNormTest, BackwardTest, exp_grads);
    LOAD_DATA(BatchNormTest, BackwardTest, exp_beta);
    LOAD_DATA(BatchNormTest, BackwardTest, exp_gamma);

    matrix_d out = layer.forward(batch);
    auto [grads, cost] = Network::MSE(target, out);
    auto newGrads = layer.backward(grads, 0.1);

    COMPARE_MAT(newGrads, exp_grads);
    COMPARE_ARR(layer.gamma, exp_gamma);
    COMPARE_ARR(layer.beta, exp_beta);
}

TEST_F(BatchNormTest, PredictTest) {
    BatchNorm layer(0.90);
    matrix_h exp_out, batch;
    array_h<float> exp_mean, exp_var;

    LOAD_DATA_S(BatchNormTest, batch);
    LOAD_DATA(BatchNormTest, PredictTest, exp_out);
    LOAD_DATA(BatchNormTest, PredictTest, exp_mean);
    LOAD_DATA(BatchNormTest, PredictTest, exp_var);

    layer.forward(batch);
    layer.forward(batch);
    layer.forward(batch);
    layer.forward(batch);
    layer.forward(batch);

    auto out = layer.predict(batch);
    
    COMPARE_MAT(out, exp_out);
    COMPARE_ARR(layer.moving_mean, exp_mean);
    COMPARE_ARR(layer.moving_var, exp_var);
}
