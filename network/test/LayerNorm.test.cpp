#include "LayerNorm.hpp"

#include <gtest/gtest.h>

#include "Network.hpp"
#include "utils.h"

class LayerNormTest : public testing::Test {
protected:
    static void SetUpTestSuite() {
        RANDOM_MAT(6, 4, batch);
        RANDOM_MAT(6, 4, target);
        
        CREATE_TMP_DIR();
        SAVE_DATA(LayerNormTest, batch);
        SAVE_DATA(LayerNormTest, target);
        RUN_PYTHON(LayerNormTest);
    }
};

TEST_F(LayerNormTest, ConstructorTest) {
    LayerNorm layer;
    EXPECT_EQ(layer.type(), ModuleType::LayerNorm);
}

TEST_F(LayerNormTest, ForwardTest) {
    LayerNorm layer;
    matrix_h exp_out, batch;

    LOAD_DATA_S(LayerNormTest, batch);
    LOAD_DATA(LayerNormTest, ForwardTest, exp_out);

    auto out = layer.forward(batch);

    COMPARE_MAT(out, exp_out);

    EXPECT_EQ(layer.gamma.count, layer.beta.count);
    array_h g(layer.gamma), b(layer.beta);
    for (int i = 0; i < g.count; i++) {
        EXPECT_EQ(g[i], 1);
        EXPECT_EQ(b[i], 0);
    }
}

TEST_F(LayerNormTest, PredictTest) {
    LayerNorm layer;
    matrix_h batch;

    LOAD_DATA_S(LayerNormTest, batch);

    auto out1 = layer.forward(batch);
    auto out2 = layer.predict(batch);

    EXPECT_TRUE(out1 == out2);
}

TEST_F(LayerNormTest, BackwardTest) {
    LayerNorm layer;
    matrix_h exp_grads, batch, target;
    array_h<float> exp_beta, exp_gamma;

    LOAD_DATA_S(LayerNormTest, batch);
    LOAD_DATA_S(LayerNormTest, target);
    LOAD_DATA(LayerNormTest, BackwardTest, exp_grads);
    LOAD_DATA(LayerNormTest, BackwardTest, exp_beta);
    LOAD_DATA(LayerNormTest, BackwardTest, exp_gamma);

    matrix_d out = layer.forward(batch);
    auto [grads, cost] = Network::MSE(target, out);
    auto newGrads = layer.backward(grads, 0.1);

    COMPARE_MAT(newGrads, exp_grads);
    COMPARE_ARR(layer.gamma, exp_gamma);
    COMPARE_ARR(layer.beta, exp_beta);
}
