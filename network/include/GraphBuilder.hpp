#pragma once

#include "core.h"
#include "Graph.hpp"
#include "sph.h"

struct GraphSizes {
    int2 nodes;
    int2 edges;
};

class GraphBuilder {
public:
    const array_d<float3>& locs;
    const array_d<float3>& vels;
    const array_d<plane_s>& boundaries;
    const int velocitiesCount = 5;

    GraphBuilder();

    GraphBuilder(GraphBuilder const&) = delete;

    GraphBuilder& operator=(GraphBuilder const&) = delete;

    void setBoundaries(const array_d<plane_s>& boundaries);

    void pushLocations(const array_d<float3>& locs);

    size_t initialLoad(const Rollout& rollout);

    matrix_d getAccelerations(const array_d<float3>& locs);

    Graph buildGraph(const physics_s &physics);

private:
    array_d<plane_s> _boundaries;
    array_d<float3> _locs;
    array_d<float3> _vels;
    std::deque<array_d<float3>> _vels_list;
};
