#pragma once

#include <algorithm>
#include <random>
#include "sph.h"

class Dataset {
   public:
    std::vector<Graph> graphs;
    std::vector<matrix_d> targets;
    std::vector<int> indices;

    void shuffle() {
        auto rng = std::default_random_engine{};
        std::shuffle(std::begin(indices), std::end(indices), rng);
    }

    const Graph& getGraph(int index) {
        return graphs[indices[index]];
    }

    const matrix_d& getTarget(int index) {
        return targets[indices[index]];
    }

    void init(int count, int steps, float scale) {
        graphs.clear();
        indices.clear();
        targets.clear();

        auto physics = getPhysics();
        int index = 0;
        float step = (0.07 - 0.042) / (count - 1);

        for (int i = 0; i < count; i++) {
            auto [fluid, boundaries] = generareRandomFluid(1000, 0.042 + step * i);
            Solver solver;
            GraphBuilder gb;
            gb.setBoundaries(boundaries);
            solver.setBoundaries(boundaries);
            
            for (int j = 0; j < steps; j++) {
                Fluid next = solver.run(fluid, getPhysics());
                gb.pushLocations(fluid.locs);
                graphs.push_back(gb.buildGraph(physics));
                indices.push_back(index++);
                targets.push_back(toMatrix(next.locs - fluid.locs) * scale);
                fluid = std::move(next);
            }
        }

        shuffle();
    }

    static physics_s getPhysics() {
        physics_s physics;
        physics.gravity = {0.0, -9.8, 0.0};
        physics.radius = 0.1;
        physics.cohesion = 0.025;
        physics.surfaceTension = 1;
        physics.viscosity = 5;
        physics.restDistanceRatio = 0.55;
        physics.maxGrixDims = 128;
        physics.maxNeighbours = 300;
        physics.maxSpeed = 5.0;
        physics.relaxationFactor = 1.0;
        physics.damp = 30;
        return physics;
    }

    static std::tuple<Fluid, array_h<plane_s>> generareRandomFluid(int particlesCount = 1000, double dist = rnd(0.042, 0.07)) {
        srand(time(NULL));

        double volume = particlesCount * dist * dist * dist;
        double maxDim = pow(volume, 1.0 / 3.0);
        double shiftX = maxDim * rnd(-0.2, 0.2);
        double x = maxDim - shiftX;
        double shiftY = x * rnd(-0.2, 0.2);
        double y = maxDim + shiftX - shiftY;
        double z = maxDim + shiftY;

        double oX = rnd(1, 2.7 - x);
        double oY = rnd(1, 1.7 - y);
        double oZ = rnd(1, 2.7 - z);

        auto particles = generateCubePoints(make_point(oX, oY, oZ), make_point(x, y, z), dist);
        auto boundaries = generateBoundariesStatic(make_point(0.9, -0.1, 0.9), make_point(2, 2, 2));
        auto statics = generateBoundaryPoints(boundaries, getPhysics().radius / 5.0);

        Fluid fluid;
        fluid.setParticles(particles, statics);
        fluid.setVelocity(make_point(rnd(-1, 1), rnd(-1, 1), rnd(-1, 1)));

        return { std::move(fluid), std::move(boundaries) };
    }

    static Rollout generateRollout(int steps) {
        auto [fluid, boundaries] = generareRandomFluid();
        physics_s physics = getPhysics();
        Rollout roll;
        Solver solver;

        solver.setBoundaries(boundaries);
        roll.setPhysics(physics);
        roll.setBoundaries(boundaries);
        roll.setStatics(fluid.allLocs.copy(fluid.locs.count));

        roll.pushLocs(fluid.locs);
        for (int i = 0; i < steps; i++) {
            fluid = solver.run(fluid, physics);
            roll.pushLocs(fluid.locs);
        }

        return roll;
    }

   private:
    static float rnd(float lower_bound, float upper_bound) {
        return lower_bound + rand() / (RAND_MAX / (upper_bound - lower_bound));
    }
};
