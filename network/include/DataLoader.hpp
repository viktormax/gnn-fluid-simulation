#pragma once

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "core.h"

class DataLoader {
   public:
    static void readCSV(std::string filename, matrix_h& inputs, matrix_h& outputs) {
        std::ifstream file(filename);
        std::string line, word;
        if (!file) {
            printf("Cannot open file!\n");
            exit(EXIT_FAILURE);
        }

        for (int i = 0; i < inputs.rows; i++) {
            std::getline(file, line, '\n');
            std::stringstream ss(line);
            for (int j = 0; j < inputs.cols; j++) {
                std::getline(ss, word, ',');
                inputs(i, j) = std::stof(&word[0]);
            }
            for (int j = 0; j < outputs.cols; j++) {
                std::getline(ss, word, ',');
                outputs(i, j) = std::stof(&word[0]);
            }
        }
    }
};
