#pragma once

#include <nlohmann/json.hpp>

using json = nlohmann::json;

class TrainingSession {
   public:
    float learningRate;
    int currentVersion;
    int versionIteration;
    int saveIteration;
    float minMeanLoss;
    float prevMeanLoss;

    void load(const std::string& modelDir) {
        std::ifstream file(modelDir + "/trainingSession.json");
        if (file) {
            json j = json::parse(file);
            j["learningRate"].get_to(learningRate);
            j["currentVersion"].get_to(currentVersion);
            j["versionIteration"].get_to(versionIteration);
            j["saveIteration"].get_to(saveIteration);
            j["minMeanLoss"].get_to(minMeanLoss);
            j["prevMeanLoss"].get_to(prevMeanLoss);
        } else {
            learningRate = 1e-4;
            currentVersion = 0;
            versionIteration = 0;
            saveIteration = 25;
            minMeanLoss = FLT_MAX;
            prevMeanLoss = 0;
            save(modelDir);
        }
    }

    void save(const std::string& modelDir) {
        json j;
        j["learningRate"] = learningRate;
        j["currentVersion"] = currentVersion;
        j["versionIteration"] = versionIteration;
        j["saveIteration"] = saveIteration;
        j["minMeanLoss"] = minMeanLoss;
        j["prevMeanLoss"] = prevMeanLoss;
        std::ofstream ff(modelDir + "/trainingSession.json");
        ff << std::setw(4) << j << std::endl;
    }
};