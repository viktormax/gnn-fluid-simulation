#pragma once

#include <cuda_runtime_api.h>

enum class ActivationType : int {
    Linear,
    ReLU,
    LReLU,
    Sigmoid,
    Tanh
};

inline std::string to_string(ActivationType type) {
    switch (type) {
    case ActivationType::Linear:
        return "linear";
    case ActivationType::ReLU:
        return "relu";
    case ActivationType::LReLU:
        return "lrelu";
    case ActivationType::Sigmoid:
        return "sigmoid";
    case ActivationType::Tanh:
        return "tanh";
    default:
        return "undefined";
    }
}

inline ActivationType from_string(std::string type) {
    if (type == "linear") {
        return ActivationType::Linear;
    } else if (type == "relu") {
        return ActivationType::ReLU;
    } else if (type == "lrelu") {
        return ActivationType::LReLU;
    } else if (type == "sigmoid") {
        return ActivationType::Sigmoid;
    } else if (type == "tanh") {
        return ActivationType::Tanh;
    } else {
        return ActivationType::Linear;
    }
}

inline __forceinline__ __device__ __host__ float activate(const float x, const ActivationType type) {
    switch (type) {
        case ActivationType::ReLU:
            return x < 0 ? 0 : x;
        case ActivationType::Sigmoid:
            return 1 / (1 + exp(-x));
        case ActivationType::LReLU:
            return x < 0 ? 0.2 * x : x;
        case ActivationType::Linear:
            return x;
        case ActivationType::Tanh:
            return tanh(x);
        default:
            return 0;
    }
}

inline __forceinline__ __device__ __host__ float activate_deriv(const float x, const ActivationType type) {
    switch (type) {
        case ActivationType::ReLU:
            return x < 0 ? 0 : 1;
        case ActivationType::Sigmoid: {
            auto fx = activate(x, ActivationType::Sigmoid);
            return fx * (1 - fx);
        }
        case ActivationType::LReLU:
            return x < 0 ? 0.2 : 1;
        case ActivationType::Linear:
            return 1;
        case ActivationType::Tanh:
            return 1 - pow(tanh(x), 2);
        default:
            return 0;
    }
}

