#pragma once

#include <vector>

#include "Graph.hpp"
#include "GraphNetwork.hpp"
#include "Network.hpp"
#include "core.h"

class NetworkModel {
public:
    void init(int outputs, int hiddenCount, int neuronCount, ActivationType activation);

    void save(const std::string& fileName) const;

    bool load(const std::string& fileName);

    void fromStream(std::ifstream& rf);

    matrix_d predict(const Graph& input) const;

    matrix_d forward(const Graph& input);

    GraphGrads backward(const Graph& input, const matrix_d& predicted, const matrix_d& target, float learningRate);

    matrix_d train(const Graph& input, const matrix_d& target, float learningRate);
    
    double cost() const;

    size_t epoch() const;

    const matrix_d* getGrads() {
        return &_grads;
    }

private:
    GraphNetwork _encoder;
    std::vector<GraphNetwork> _hidden;
    Network _decoder;
    matrix_d _grads;
};
