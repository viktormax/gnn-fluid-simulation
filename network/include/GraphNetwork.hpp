#pragma once

#include <tuple>

#include "GraphBuilder.hpp"
#include "Network.hpp"
#include "core.h"
#include "activation.h"

typedef std::tuple<std::optional<matrix_d>, std::optional<matrix_d>> GraphGrads;

class GraphNetwork {
   public:
    void init(int hiddenLayers, int hiddenSize, int outputCount, ActivationType activation, bool independent = false);

    Graph GraphNetwork::predict(const Graph& input) const;

    Graph GraphNetwork::forward(const Graph& input);

    GraphGrads backward(const Graph& graph, const GraphGrads& grads, float learningRate);

    void toStream(std::ofstream& wf) const;

    void fromStream(std::ifstream& rf);

   private:
    Network _nodeNetwork;
    Network _edgeNetwork;

    bool _independent;

    mat_size _node_size;
    mat_size _edge_size;
};
