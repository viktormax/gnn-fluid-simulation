#pragma once

#include <fstream>
#include <vector>
#include "Module.hpp"

class Network : public Module {
   private:
    std::vector<Module*> _layers;
    double _cost = DBL_MAX;
    size_t _epoch = 0;

   public:
    const double& cost;
    const size_t& epoch;

    Network();

    ~Network();

    void add(Module* layer);

    matrix_d predict(const matrix_d& inputs) const;

    matrix_d forward(const matrix_d& inputs);

    matrix_d backward(const matrix_d& grads, const float learning_rate);

    ModuleType type() const;

    matrix_d loss(const matrix_d& target, const matrix_d& predicted);

    static std::tuple<matrix_d, double> MSE(const matrix_d& target, const matrix_d& predicted);

    void toStream(std::ofstream& wf) const;

    void fromStream(std::ifstream& rf);

    friend std::ostream& operator<<(std::ostream& ss, const Network& n);
};

