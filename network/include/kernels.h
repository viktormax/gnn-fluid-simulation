#pragma once

#include "activation.h"
#include "core.h"

matrix_d activate(const matrix_d& in, const ActivationType type);

matrix_d activate_deriv(const matrix_d& in, const ActivationType type);

void construct_edge_features(
    matrix_d& edges,
    array_d<uint32_t>& senders,
    array_d<uint32_t>& receivers,
    const array_d<float3>& locs,
    const matrix_d& neighbours,
    const array_d<uint32_t>& indexes,
    float radius);

void construct_node_features(
    matrix_d& nodes,
    const array_d<float3>& locs,
    const array_ptr<float3>& vels_list,
    const array_d<plane_s>& boundaries);

void get_total_size(const array_d<float>& neighbours_count, array_d<uint32_t>& indexes, array_d<uint32_t>& size);

matrix_d broadcast_nodes_to_edges(
    const matrix_d& nodes,
    const matrix_d& edges,
    const array_d<uint32_t>& senders,
    const array_d<uint32_t>& receivers);

matrix_d broadcast_edges_to_nodes(
    const matrix_d& nodes,
    const matrix_d& edges,
    const array_d<uint32_t>& receivers);

matrix_d spred_edge_grads(
    const matrix_d& grads,
    const array_d<uint32_t>& receivers);

matrix_d spred_node_grads(
    const matrix_d& nodeGrads,
    const matrix_d& edgeGrads,
    const array_d<uint32_t>& receivers,
    const array_d<uint32_t>& senders);

float max(const matrix_d& mat);