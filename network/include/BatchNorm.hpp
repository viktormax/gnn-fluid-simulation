#pragma once

#include <fstream>

#include "Module.hpp"
#include "core.h"

class BatchNorm : public Module {
   public:
    const array_d<float>& moving_mean;
    const array_d<float>& moving_var;
    const array_d<float>& gamma;
    const array_d<float>& beta;

    BatchNorm(float momentum = 0.99);

    matrix_d predict(const matrix_d& batch) const;

    matrix_d forward(const matrix_d& batch);

    matrix_d backward(const matrix_d& grads, float learningRate);

    ModuleType type() const;

    void toStream(std::ofstream& wf) const;

    void fromStream(std::ifstream& rf);

   private:
    array_d<float> _moving_mean;
    array_d<float> _moving_var;
    array_d<float> _gamma;
    array_d<float> _beta;

    // Cache for backward
    bool _cached = false; 
    matrix_d _xmu;
    matrix_d _xnorm;
    array_d<float> _ivar;

    double _eps = 1e-5;
    double _momentum;

    void resize(int cols);
};
