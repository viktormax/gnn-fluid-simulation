#pragma once

#include <memory>

#include "core.h"

class Graph {
   public:
    matrix_d edges;
    matrix_d nodes;
    std::shared_ptr<array_d<uint32_t>> senders;
    std::shared_ptr<array_d<uint32_t>> receivers;

    Graph();

    Graph(const Graph& that);

    Graph& operator=(const Graph& that);

    Graph operator+(const Graph& that);

    Graph(Graph&& that);

    matrix_d broadcastNodesToEdges() const;

    matrix_d broadcastEdgesToNodes(const matrix_d& edges) const;

    matrix_d spredEdgeGrads(const matrix_d& grads) const;

    matrix_d spredNodeGrads(const matrix_d& nodeGrads, const matrix_d& edgeGrads) const;

    friend std::ostream& operator<<(std::ostream& ss, const Graph& g);

    void toStream(std::ofstream& wf) const;

    void fromStream(std::ifstream& rf);
};