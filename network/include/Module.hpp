#pragma once

#include "core.h"

enum class ModuleType : int {
    Layer,
    LayerNorm,
    BatchNorm,
    Network
};

class Module {
   public:
    virtual matrix_d predict(const matrix_d& inputs) const = 0;

    virtual matrix_d forward(const matrix_d& inputs) = 0;

    virtual matrix_d backward(const matrix_d& deltas, float learningRate) = 0;

    virtual ModuleType type() const = 0;

    virtual void toStream(std::ofstream& wf) const = 0;

    virtual void fromStream(std::ifstream& rf) = 0;
};
