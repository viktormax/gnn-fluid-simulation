#pragma once

#include "Module.hpp"
#include "activation.h"
#include "core.h"
#include "AdamOptimizer.hpp"

class Layer : public Module {
   public:
    const ActivationType& activationType;
    const size_t& neuronCount;
    const matrix_d& weights;
    const array_d<float>& biases;
    static bool useAdamOptimizer;

    Layer(const Layer& layer);

    Layer(const size_t neuronCount = 0, const ActivationType activationType = ActivationType::Linear);

    void setWeights(const matrix_d& weights, const array_d<float>& biases);

    matrix_d predict(const matrix_d& inputs) const;

    matrix_d forward(const matrix_d& inputs);

    matrix_d backward(const matrix_d& grads, float learningRate);

    ModuleType type() const;

    void toStream(std::ofstream& wf) const;

    void fromStream(std::ifstream& rf);

    friend std::ostream& operator<<(std::ostream& ss, const Layer& layer);

   private:
    ActivationType _activationType;
    size_t _neuronCount;

    matrix_d _weights;
    array_d<float> _biases;

    bool _useAdamOptimizer;
    AdamOptimizer _ob;
    AdamOptimizer _oW;

    // Cache for backword
    bool _cahced = false;
    matrix_d _inputs;
    matrix_d _unactivated;

    void randomize();

    void checkInputSizes(int cols);
};

