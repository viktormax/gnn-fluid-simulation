#pragma once

#include "core.h"

class AdamOptimizer {
   public:
    void toStream(std::ofstream& wf) const;

    void fromStream(std::ifstream& rf);

    matrix_d operator()(const matrix_d& param, const matrix_d& grads, float learningRate);

    array_d<float> operator()(const array_d<float>& param, const array_d<float>& grads, float learningRate);

    matrix_d opt(const matrix_d& param, const matrix_d& grads, float learningRate);

    matrix_d _mm;
    matrix_d _vm;

    array_d<float> _ma;
    array_d<float> _va;

    float _isArray = false;

    float _beta1 = 0.9;
    float _beta2 = 0.999;
    float _epsilon = 1e-05;
    int _t = 0;
};
