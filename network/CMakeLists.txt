cmake_minimum_required(VERSION 3.18 FATAL_ERROR)
project(network LANGUAGES CXX CUDA)

include(CheckLanguage)
check_language(CUDA)
enable_language(CUDA)

file(GLOB_RECURSE SOURCE_FILES_SUB "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cu" "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp")
file(GLOB_RECURSE HEADER_FILES_SUB "${CMAKE_CURRENT_SOURCE_DIR}/include/*.hpp" "${CMAKE_CURRENT_SOURCE_DIR}/include/*.h")

add_library(${PROJECT_NAME} STATIC ${SOURCE_FILES_SUB} ${HEADER_FILES_SUB})

target_link_libraries(${PROJECT_NAME} PRIVATE cublas curand core sph)
target_include_directories(${PROJECT_NAME} PUBLIC include)

# TESTS
find_package(GTest REQUIRED)

file(GLOB_RECURSE TEST_FILES_SUB "${CMAKE_CURRENT_SOURCE_DIR}/test/*.cpp")

add_executable(${PROJECT_NAME}_test ${TEST_FILES_SUB})

target_link_libraries(${PROJECT_NAME}_test PRIVATE ${PROJECT_NAME} GTest::gtest GTest::gtest_main network core sph)

add_test(${PROJECT_NAME}_test ${PROJECT_NAME}_test)

target_compile_definitions(${PROJECT_NAME}_test PRIVATE TMP_DATA_DIR="${CMAKE_CURRENT_SOURCE_DIR}/test/tmp/" PYTHON_SRC_DIR="${CMAKE_CURRENT_SOURCE_DIR}/test/python/")
