@REM REQUIREMENTS: cmake, git, Visual Studio, CUDA Toolkit 11

git clone https://github.com/Microsoft/vcpkg.git

call %cd%\vcpkg\bootstrap-vcpkg.bat
%cd%\vcpkg\vcpkg integrate install

%cd%\vcpkg\vcpkg install imgui:x64-windows
%cd%\vcpkg\vcpkg install glm:x64-windows
%cd%\vcpkg\vcpkg install glfw3:x64-windows
%cd%\vcpkg\vcpkg install glew:x64-windows
%cd%\vcpkg\vcpkg install gtest:x64-windows
%cd%\vcpkg\vcpkg install nlohmann-json:x64-windows

cmake -B build -S . -DCMAKE_TOOLCHAIN_FILE=%cd%\vcpkg\scripts\buildsystems\vcpkg.cmake

xcopy /s trained-models build
