#include <chrono>

#include "core.h"
#include "network.h"
#include "sph.h"
#include "visualization.h"

void parseArgs(int argc, char** argv, int& stepsCount, std::string& modelDir, std::string& version, float& scale, bool& generateStatistics, int& framePause);
void generateStats(NetworkModel& model, int generateSteps, float scale);

int main(int argc, char** argv) {
    int generateSteps = 400;
    float scale = 10000;
    std::string modelDir = "2mps-128nc-10000sc-lrelu";
    std::string version = "";
    bool generateStatistics = false;
    int framePause = 0;

    parseArgs(argc, argv, generateSteps, modelDir, version, scale, generateStatistics, framePause);

    NetworkModel model;
    auto name = version.length() > 0 ? version + "-model.bin" : "model.bin";
    if (!model.load(modelDir + "/" + name)) {
        std::cout << "Model \"" << modelDir + "/" + name << "\" not found.\n";
        system("pause");
        return 0;
    }

    if (generateStatistics) {
        std::cout << "Generating statistics...\n";
        generateStats(model, generateSteps, scale);
        return 0;
    }

    float dt = 0.01;
    auto physics = Dataset::getPhysics();
    auto [fluid, boundaries] = Dataset::generareRandomFluid(3000);

    std::cout << "Generating ground truth trajectories...\n";
    auto start = std::chrono::high_resolution_clock::now();
    Rollout rollGTruth;
    {
        Solver solver;
        Fluid fluid2;
        fluid2 = fluid;
        rollGTruth.setPhysics(physics);
        rollGTruth.setStatics(fluid.allLocs.copy(fluid.locs.count));
        rollGTruth.setBoundaries(boundaries);
        solver.setBoundaries(boundaries);

        for (int i = 0; i < generateSteps; i++) {
            fluid2 = solver.run(fluid2, physics);
            rollGTruth.pushLocs(fluid2.locs);
        }
    }
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    std::cout << "Ground truth generation took " << duration.count() << "ms.\n";

    std::cout << "Generating predicted trajectories...\n";
    start = std::chrono::high_resolution_clock::now();
    Rollout rollPred;
    {
        GraphBuilder gb;
        // Initial load
        for (int i = 0; i < 6; i++) {
            gb.pushLocations(rollGTruth.getStep(i));
            rollPred.pushLocs(rollGTruth.getStep(i));
        }
        gb.setBoundaries(boundaries);

        rollPred.setPhysics(physics);
        rollPred.setStatics(fluid.allLocs.copy(fluid.locs.count));
        rollPred.setBoundaries(boundaries);

        for (int i = 0; i < generateSteps - 6; i++) {
            auto graph = gb.buildGraph(rollPred.physics);
            auto locs = gb.locs + toArray(model.predict(graph)) / scale;

            gb.pushLocations(locs);
            rollPred.pushLocs(locs);

            memory_manager.runGarbitchCollector(2);
        }
    }
    end = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    std::cout << "Prediction generation took " << duration.count() << "ms.\n";

    Scene scene;
    scene.init();
    auto offset = make_point(2, 0, 0);

    Points pointsPred, pointsGTruth, pointsStatic;
    pointsPred.create(fluid.locs.count);
    pointsGTruth.create(fluid.locs.count);
    auto prevLocsPred = rollPred.getStep(0), locsPred = rollPred.getStep(0);
    auto prevLocsGTruth = rollGTruth.getStep(0), locsGTruth = rollGTruth.getStep(0);

    // auto statics = fluid.allLocs.copy(fluid.locs.count);
    // statics.append(statics + offset);
    // pointsStatic.create(statics.count);
    // pointsStatic.setPointSize(5);
    // pointsStatic.updateColors(color::rgb(52, 73, 94, 0.6));
    // pointsStatic.updatePositions(statics);

    std::cout << "Press space to run...\n";
    int step = 1;
    while (!scene.window.shouldClose()) {
        scene.createNewFrame();

        if (scene.runSimulation()) {
            prevLocsPred = rollPred.getStep(step - 1);
            locsPred = rollPred.getStep(step);
            prevLocsGTruth = rollGTruth.getStep(step - 1);
            locsGTruth = rollGTruth.getStep(step);
            step++;
        }

        pointsPred.updatePositions(locsPred + offset);
        pointsPred.updateColors(mapVelocityToColor((locsPred - prevLocsPred) / dt, scene.debug.getColors()));
        pointsPred.render(scene.camera, scene.light);

        pointsGTruth.updatePositions(locsGTruth);
        pointsGTruth.updateColors(mapVelocityToColor((locsGTruth - prevLocsGTruth) / dt, scene.debug.getColors()));
        pointsGTruth.render(scene.camera, scene.light);

        // pointsStatic.render(scene.camera, scene.light);

        scene.swapBuffers();

        if (framePause && step % framePause == 0) {
            scene.stopSimulation();
        }

        if (step == generateSteps) {
            step = 1;
        }
    }
}

void parseArgs(int argc, char** argv, int& stepsCount, std::string& modelDir, std::string& version, float& scale, bool& generateStatistics, int& framePause) {
    for (int i = 1; i < argc; i++) {
        std::string arg = argv[i];
        if (arg == "-st") {
            stepsCount = std::stoi(argv[++i]);
        } else if (arg == "-m") {
            modelDir = argv[++i];
        } else if (arg == "-v") {
            version = argv[++i];
        } else if (arg == "-sc") {
            scale = std::stoi(argv[++i]);
        } else if (arg == "-gs") {
            generateStatistics = true;
        } else if (arg == "-fp") {
            framePause = std::stoi(argv[++i]);
        }
    }
}

void generateStats(NetworkModel& model, int generateSteps, float scale) {
    float dt = 0.01;
    auto physics = Dataset::getPhysics();
    auto [fluid, boundaries] = Dataset::generareRandomFluid(2000);

    Solver solver;
    solver.setBoundaries(boundaries);

    GraphBuilder gb;
    // Initial load
    gb.pushLocations(fluid.locs);
    for (int i = 0; i < 5; i++) {
        fluid = solver.run(fluid, physics);
        gb.pushLocations(fluid.locs);
    }
    gb.setBoundaries(boundaries);

    std::ofstream wf("statistics.csv", std::ios::out);

    wf << "Step,MSE,ModelTime,SolverTime,Neighbours\n";

    auto start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < generateSteps; i++) {
        auto start = std::chrono::high_resolution_clock::now();
        auto graph = gb.buildGraph(physics);
        auto vels = model.predict(graph);
        auto end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);

        start = std::chrono::high_resolution_clock::now();
        auto next = solver.run(fluid, physics);
        end = std::chrono::high_resolution_clock::now();
        auto duration2 = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);

        auto neighboursStats = getStats(solver.neighbours.count);

        auto expected = toMatrix(next.locs - fluid.locs) * scale;
        auto [g, mse] = Network::MSE(vels, expected);

        gb.pushLocations(next.locs);
        
        wf << i << "," << mse << "," << duration.count() << "," << duration2.count() << "," << neighboursStats.mean << "\n";

        fluid = std::move(next);

        memory_manager.runGarbitchCollector(2);
    }
}
